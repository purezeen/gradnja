<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gulp-wordpress, use a find and replace
	 * to change 'gulp-wordpress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'gulp-wordpress' ),
		'secondary' => esc_html__( 'Footer', 'gulp-wordpress' ),
		'top-header' => esc_html__( 'Top-header', 'gulp-wordpress' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 782 );
}
// add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
		'before_widget' => '<section><div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div></section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {
	// Bootstrap css
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap.css' , array(), '1.33', 'all'  );

	// Lightcase css
	wp_enqueue_style( 'lightcase-css', get_template_directory_uri() . '/css/lightcase.css' , array(), '1.33', 'all'  );

	// Mmenu css
	wp_enqueue_style( 'Mmenu-css', get_template_directory_uri() . '/css/mmenu.css' , array(), '1.33', 'all'  );

	// Plyr css
	// wp_enqueue_style( 'plyr-css', get_template_directory_uri() . '/css/plyr.css' , array(), '1.33', 'all'  );

	// Select2 css
	wp_enqueue_style( 'select2-css', get_template_directory_uri() . '/css/select2.min.css' , array(), '1.33', 'all'  );

	wp_enqueue_style( 'gulp-wordpress-style', get_stylesheet_uri() , array(), '1.13', 'all'  );

	// Resize Senzor js
	wp_enqueue_script( 'ResizeSenzor', get_template_directory_uri() . '/js/ResizeSensor.min.js', array(), '2', true );

	// Theia sticky sidebar js
	wp_enqueue_script( 'theia-sticky-sidebar-js', get_template_directory_uri() . '/js/theia-sticky-sidebar.min.js', array(), '2', true );

	// Masonry js
	 wp_enqueue_script( 'masonry-js', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), '2', true );
	
	// MMenu 
	wp_enqueue_script( 'mmenu-js', get_template_directory_uri() . '/js/mmenu.js', array(), '3', true ); 

	 // jquery.mmenu.wordpress
	 wp_enqueue_script( 'jquery.mmenu.wordpress', get_template_directory_uri() . '/js/jquery.mmenu.wordpress.js', array(), '2', true );

	 // Plyr js
	// wp_enqueue_script( 'plyr-js', get_template_directory_uri() . '/js/plyr.min.js', array(), '2', true );

	// Select2 js
	wp_enqueue_script( 'select2-js', get_template_directory_uri() . '/js/select2.full.min.js', array(), '2', true );

	// Lightcase addon - mobile swipe
	wp_enqueue_script( 'lightcase-mobile-swipe', get_template_directory_uri() . '/js/jquery.events.touch.js', array(), '1', true );

	// Lightcase js
	wp_enqueue_script( 'lightcase-js', get_template_directory_uri() . '/js/lightcase.js', array(), '2', true );

	// Flexslider js
	wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array(), '2', true );

	// Main js
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '3.1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Register Custom Post Type
function cpt_CustomPost() {

	$labels = array(
		'name'                  => _x( 'Uradi sam', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Uradi sam', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Uradi sam', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Uradi sam', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Custom post', 'gulp_wordpress' ),
		'description'           => __( 'Custom post Description', 'gulp_wordpress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'main titile' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'   => 'dashicons-hammer',
		'taxonomies'   => array(
      'post_tag',
      'category'
    ) // Add Category and Post Tags support
	);
	register_post_type( 'uradi-sam', $args );

}

add_action( 'init', 'cpt_CustomPost', 0 );









//* ======== EXCERTP ======== *//
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Banners',
		'menu_title'	=> 'Baneri',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Snippet',
		'menu_title'	=> 'Snippet',
		'parent_slug'	=> 'theme-general-settings',
	));
}


add_action( 'after_setup_theme', function() {
	add_theme_support( 'responsive-embeds' );
} );

/**
 * Estimates the reading time for a given piece of $content.
 *
 * @param string $content Content to calculate read time for.
 * @param int $wpm Estimated words per minute of reader.
 *
 * @returns	int $time Esimated reading time.
 */
function prefix_estimated_reading_time( $content = '', $wpm = 300 ) {
	$clean_content = strip_shortcodes( $content );
	$clean_content = strip_tags( $clean_content );
	$word_count = str_word_count( $clean_content );
	$time = ceil( $word_count / $wpm );
	return $time;
}

/**
 * Show how many coments is in specific post.
 */
function wpb_comment_count() { 
$comments_count = wp_count_comments();
$message =  'There are <strong>'.  $comments_count->approved . '</strong> comments posted by our users.';
 
return $message; 
} 
 
add_shortcode('wpb_total_comments','wpb_comment_count'); 

add_action( 'init', 'gradnja_pagination' ); // Add our HTML5 Pagination
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function gradnja_pagination() {
		if( get_option('permalink_structure') ) {
	      $format = 'page/%#%/';
	  } else {
	      $format = '&paged=%#%';
	  }
    global $wp_query;
    $big = 999999999;
    echo paginate_links( array(
        'base'    	=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'  	=> $format,
        'current' 	=> max( 1, get_query_var( 'paged' ) ),
        'total'   	=> $wp_query->max_num_pages,
        'mid_size'  => 3,
        'type'      => 'list',
        'prev_text' => esc_html__( 'Prethodno', 'gulp_wordpress' ),
        'next_text' => esc_html__( 'Sledeće', 'gulp_wordpress' ),
    ) );
}


function my_pre_get_posts( $query ) {
    
    // do not modify queries in the admin
    if( is_admin() ) {
        
        return $query;
        
    }
    
    
    // only modify queries for 'event' post type
    if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'product' ) {
        
        // allow the url to alter the query
        if( isset($_GET['category']) ) {
            
            $query->set('cat', $_GET['category']);
            
        } 
        
    }
    
    
    // return
    return $query;

}

add_action('pre_get_posts', 'my_pre_get_posts');


/**
 * Breadcrumb
 */

function get_breadcrumb() {
	echo 'Home';
	if (is_category() || is_single()) {

		/* The stuff below happens when it’s a category or a post*/

		echo " &nbsp; / &nbsp;";
		echo "<strong>";
		the_category(' • ');
		echo "</strong>";
		if (is_single()) {
			echo " &nbsp; / &nbsp;";
			echo "<strong>";
			the_title();
			echo "</strong>";
		}
	} elseif (is_page()) {

		/* The stuff below happens when it’s a page */

		echo " &nbsp; / &nbsp; ";
		echo "<strong>";
		echo the_title();
		echo "</strong>";
	} elseif (is_search()) {

		/* The stuff below happens when it’s a search */

		echo "  /  Rezultati tražene reči… ";
		echo "<strong>";
		echo '"';
		echo the_search_query();
		echo '"';
		echo "</strong>";
	}
}

/**
 * Widget API: Gradnja_Commets class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Recent Comments widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class Gradnja_Commets extends WP_Widget {

	/**
	 * Sets up a new Recent Comments widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'widget_recent_comments',
			'description'                 => __( 'Your site&#8217;s most recent comments.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'recent-comments', __( 'Najnoviji komentari' ), $widget_ops );
		$this->alt_option_name = 'widget_recent_comments';

		if ( is_active_widget( false, false, $this->id_base ) || is_customize_preview() ) {
			add_action( 'wp_head', array( $this, 'recent_comments_style' ) );
		}
	}

	/**
	 * Outputs the default styles for the Recent Comments widget.
	 *
	 * @since 2.8.0
	 */
	public function recent_comments_style() {
		/**
		 * Filters the Recent Comments default widget styles.
		 *
		 * @since 3.1.0
		 *
		 * @param bool   $active  Whether the widget is active. Default true.
		 * @param string $id_base The widget ID.
		 */
		if ( ! current_theme_supports( 'widgets' ) // Temp hack #14876
			|| ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) ) {
			return;
		}
		?>
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<?php
	}

	/**
	 * Outputs the content for the current Recent Comments widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Recent Comments widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$output = '';

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number ) {
			$number = 5;
		}

		/**
		 * Filters the arguments for the Recent Comments widget.
		 *
		 * @since 3.4.0
		 * @since 4.9.0 Added the `$instance` parameter.
		 *
		 * @see WP_Comment_Query::query() for information on accepted arguments.
		 *
		 * @param array $comment_args An array of arguments used to retrieve the recent comments.
		 * @param array $instance     Array of settings for the current widget.
		 */
		$comments = get_comments(
			apply_filters(
				'widget_comments_args',
				array(
					'number'      => $number,
					'status'      => 'approve',
					'post_status' => 'publish',
				),
				$instance
			)
		);

		$output .= $args['before_widget'];
		$output .= '<div class="widget-posts sidebar-block">';
		if ( $title ) {
			$output .= '<h3 class="title-line title-sidebar">' . $title . '<h3>';
		}

		$output .= '<ul id="recentcomments" class="unstyle-list">';
		$output .= '<li class="recentcomments">';
		if ( is_array( $comments ) && $comments ) {
			// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
			$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
			_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

			foreach ( (array) $comments as $comment ) {
				$output .= '<span href="#" class="widget-post clearfix">';
				/* translators: comments widget: 1: comment author, 2: post link */
				$output .= sprintf(
					_x( '%1$s %2$s', 'widgets' ),
					'<div>',
					'<div class="meta-data align-center"><span class="devider align-center"><span class="iconify" data-icon="fa-comment-o" data-inline="false"></span>' . get_comment_author( $comment ) . '</span><span>' . get_comment_date( 'd.m.Y.', $comment->comment_ID ) . '</span></div><a href="' . esc_url( get_comment_link( $comment ) ) . '"><h6 class="widget-post-title">' . get_the_title( $comment->comment_post_ID ) . '</h6></a>'
				);
				$output .= '</div></span>';
			}
		}
		$output .= '</li>';
		$output .= '</ul>';
		$output .= '</div>';
		$output .= $args['after_widget'];
		echo $output;
	}

	/**
	 * Handles updating settings for the current Recent Comments widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Recent Comments widget.
	 *
	 * @since 2.8.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? $instance['title'] : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of comments to show:' ); ?></label>
		<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>
		<?php
	}

	/**
	 * Flushes the Recent Comments widget cache.
	 *
	 * @since 2.8.0
	 *
	 * @deprecated 4.4.0 Fragment caching was removed in favor of split queries.
	 */
	public function flush_widget_cache() {
		_deprecated_function( __METHOD__, '4.4.0' );
	}
}
add_action( 'widgets_init', 'register_my_widget' );
function register_my_widget() {
     register_widget ( 'Gradnja_Commets' );
}

function isMobileDevice() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

// function isMobileDevice(){
//    if(defined(isMobileDevice))return isMobileDevice;
//    @define(isMobileDevice,(!($HUA=@trim(@$_SERVER['HTTP_USER_AGENT']))?0:
//    (
//       preg_match('/(android|bb\d+|meego).+mobile|silk|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i'
//       ,$HUA)
//    ||
//       preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i'
//       ,$HUA)
//    )
//    ));
// }

	//Exclude pages from WordPress Search
if (!is_admin()) {
function wpb_search_filter($query) {
if ($query->is_search) {
$query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','wpb_search_filter');
}



/**
 * Post Gallery Layout
 */
add_filter( 'post_gallery', 'gillion_post_gallery', 10, 2 );
function gillion_post_gallery( $string, $attr ){

    $columns = ( isset( $attr['columns'] ) && $attr['columns'] >= 1 && $attr['columns'] <= 4 ) ? $attr['columns'] : '3';
    // $size = $attr['size'];
    $size = 'large';
    $gallery_id = 'gallery-'.gillion_rand(6);
    
    $data_justify = '';

    $output = '<div class="post-content-gallery gallery-columns-'.esc_attr( $columns ).'">';
    $posts = get_posts( array( 'include' => $attr['ids'], 'post_type' => 'attachment', 'orderby' => $attr['orderby'] ) );


        foreach( $posts as $post ) :
            $post_link = '';
            if( isset( $post->ID ) && is_array( wp_get_attachment_image_src( $post->ID, 'full' ) ) ) :
                $post_link_var = wp_get_attachment_image_src( $post->ID, 'full' );
                if( isset( $post_link_var[0] ) ) :
                    $post_link = $post_link_var[0];
                endif;
            endif;

            $post_image = '';
            if( isset( $post->ID ) && is_array( wp_get_attachment_image_src( $post->ID, $size ) ) ) :
                $post_image_var = wp_get_attachment_image_src( $post->ID, $size );
                if( isset( $post_image_var[0] ) ) :
                    $post_image = $post_image_var[0];
                endif;
            endif;

            $output .= '
            <figure class="post-content-gallery-item">
                <a href="'.esc_url( $post_link ).'" data-rel="lightcase:'.$gallery_id.'">
                    <img src="'.esc_url( $post_image ).'" alt="'.$post->post_excerpt.'" />
                    <div class="post-content-gallery-item-caption">'.$post->post_excerpt.'</div>
                </a>
            </figure>';
        endforeach;

    $output .= '</div>';
    return $output;
}


/**
 * Create Custom Random Function
 */
function gillion_rand($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
  if( is_category() && $query->is_main_query() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'uradi-sam'); // don't forget nav_menu_item to allow menus to work!
    $query->set('post_type',$post_type);
    return $query;
    }
}

/**
 * Remove the slug from published post permalinks. Only affect our CPT though.
 */
function wpex_remove_cpt_slug( $post_link, $post, $leavename ) {
    if ( 'uradi-sam' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    return $post_link;
}
add_filter( 'post_type_link', 'wpex_remove_cpt_slug', 10, 3 );