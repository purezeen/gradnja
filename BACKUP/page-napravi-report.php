<?php  
//@phpcs:ignoreFile
include 'dbcalls/db.php';

$connect = mysqli_connect('localhost', $username, $password, $database);   
$query = "SELECT * FROM wp_maps_markers WHERE createdOn >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)";  
$result = mysqli_query($connect, $query);
mysqli_close($connect);

get_header();
if(current_user_can('administrator') || current_user_can('editor') || current_user_can('placeni_korisnik'))
{
?>
<body>
     <script>
          $("#export-button").click(function(e){
               e.preventDefault();
          })
     </script>
     <div class="container create-report">
          <div class="row">
               <div class="col-16">

                    <br /><br />
                    <div class="create-report__wrapper">
                         <h2 align="left">Export u Excel</h2>
                         <h5 class="report-h5" align="left">Odaberite kriterijum, sortirajte gradilišta i izvezite
                              podatke u tablici</h5>
                         <br />
                         <form method="post" class="export-form" id="export-form"
                              action="<?php echo get_template_directory_uri() ?>/dbcalls/export.php" align="center">
                              <div class="addPageDiv">
                                   <h6>Period: </h6>
                                   <select form="export-form" style="width:250px" name="Interval" id="export-interval">
                                        <option value="1">Prethodni dan</option>
                                        <option value="7">Prethodnih 7 dana</option>
                                        <option value="15">Prethodnih 15 dana</option>
                                        <option value="30">Prethodnih 30 dana</option>
                                        <option value="365">Prethodnih godinu dana</option>
                                   </select>
                              </div>
                          
                              <div class="addPageDiv">
                                   <h6>Sortiraj po: </h6>
                                   <select form="export-form" style="width:250px" name="Order" id="export-order">
                                        <option value="investitor">Investitor</option>
                                        <option value="pocetak_gradnje">Pocetak gradnje</option>
                                        <option value="zavrsetak_gradnje">Zavrsetak gradnje</option>
                                        <option value="izvodjac">Izvodjac</option>
                                        <option value="objekat">Ime objekta</option>
                                        <option value="Projektant">Projektant</option>
                                   </select>
                              </div>
    
                              <button type="submit" name="export" value="CSV Export" class="report-button" id="export-button">
                                   CSV EXPORT
                              </button>
                         </form>
                         <br />
                    </div>
               </div>


          </div>
     </div>
     <div style="margin-bottom:50px" class="table-responsive" id="employee_table">
          <table class="table table-bordered">
               <tr>
                    <th class="new-th">Objekat</th>
                    <th class="new-th">Investitor</th>
                    <th class="new-th">Pocetak</th>
                    <th class="new-th">Kraj</th>
                    <th class="new-th">Izvodjac</th>
                    <th class="new-th">Adresa</th>
                    <th class="new-th">Projektant</th>
                    <th class="new-th">Spratnost</th>
                    <th class="new-th">Opis</th>
               </tr>
               <?php  
                     while($row = mysqli_fetch_array($result))  
                     {  
                     ?>
               <tr>
                    <td><?php echo $row["objekat"]; ?></td>
                    <td><?php echo $row["investitor"]; ?></td>
                    <td><?php echo $row["pocetak_gradnje"]; ?></td>
                    <td><?php echo $row["zavrsetak_gradnje"]; ?></td>
                    <td><?php echo $row["izvodjac"]; ?></td>
                    <td><?php echo $row["grad"] . " " . $row["ulica"] . " " . $row["broj"]; ?></td>
                    <td><?php echo $row["projektant"]; ?></td>
                    <td><?php echo $row["spratnost"]; ?></td>
                    <td><?php echo implode(' ', array_slice(explode(' ', $row["opis"]), 0, 8)); ?></td>
               </tr>
               <?php       
                     }  
                     ?>
          </table>
     </div>

     <?php
}
else{
    echo '<script>window.location.href = "http://gradnja.local/mapa-gradilista-u-excel-formatu/";</script>';
}
    get_footer();
?>