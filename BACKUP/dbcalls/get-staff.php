<?php
/**
 * Get Investors, architects and performers.
 */
require 'db.php';
require 'wp-load.php';

global $wpdb;

$investors_query = 'SELECT investitor FROM wp_maps_markers';
$architect_query = 'SELECT projektant FROM wp_maps_markers';
$performer_query = 'SELECT izvodjac FROM wp_maps_markers';

$obj_investors = $wpdb->get_results( $investors_query );
$obj_architect = $wpdb->get_results( $architect_query );
$obj_performer = $wpdb->get_results( $performer_query );

foreach ( $obj_investors as $investor ) {
	$investor = $investor->investitor;

	if ( $investor ) {
		$investors[] = $investor;
	}
}
$investors = array_unique( $investors );

foreach ( $obj_architect as $architect ) {
	$architect = $architect->projektant;

	if ( $architect ) {
		$architects[] = $architect;
	}
}
$architects = array_unique( $architects );

foreach ( $obj_performer as $performer ) {
	$performer = $performer->izvodjac;

	if ( $performer ) {
		$performers[] = $performer;
	}
}
$performers = array_unique( $performers );

echo wp_json_encode( $investors );

echo wp_json_encode( $architects );

echo wp_json_encode( $performers );
