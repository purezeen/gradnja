<?php
//phpcs:ignoreFile
require 'db.php';
require '../../../../wp-load.php';

global $wpdb;

$connection     = mysqli_connect( $db, $username, $password );

if ( ! $connection ) {
	die( 'Not connected : ' . mysqli_error( $connection ) );
}

$db_selected = mysqli_select_db( $connection, $database );

if ( ! $db_selected ) {
	die( 'Can\'t use db : ' . mysqli_error( $connection ) );
}

$query = "SELECT DISTINCT tag FROM wp_maps_markers WHERE tag IS NOT NULL";

$get_data = mysqli_query( $connection, $query );
if ( ! $get_data ) {
	die( 'Couldnt get ' . mysqli_error( $connection ) );
}

if ( $get_data->num_rows === 0 ) {
	echo 'No data';
	return;
}

$obj_tags = $wpdb->get_results( $query );

foreach ( $obj_tags as $tag ) {
	if ( $tag ) {
		$tags[] = $tag;
	}
}
$tags = (array) $tags;

echo wp_json_encode($tags, JSON_NUMERIC_CHECK );

mysqli_close( $connection );
