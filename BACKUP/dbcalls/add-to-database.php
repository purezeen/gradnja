<?php
require 'db.php';
require_once '../../../../wp-load.php';

$objekat          = $_POST['Objekat'];
$type             = $_POST['Tip'];
$tipZgrade        = $_POST['TipZgrade'];
$grad             = $_POST['Grad'];
$ulica            = $_POST['Ulica'];
$broj             = intval( $_POST['Broj'] );
$lat              = floatval( $_POST['Lat'] );
$lon              = floatval( $_POST['Lon'] );
$cena             = $_POST['Cena'];
$broj_stanova     = floatval( $_POST['BrojStanova'] );
$investitor       = $_POST['Investitor'];
$projektant       = $_POST['Projektant'];
$spratnost        = $_POST['Spratnost'];
$opis             = $_POST['Opis'];
$linkovi          = $_POST['Linkovi'];
$pocetak          = $_POST['Pocetak'];
$zavrsetak        = $_POST['Zavrsetak'];
$izvodjac         = $_POST['Izvodjac'];
$povrsina_objekta = $_POST['Povrsina'];
$tag              = $_POST['Tag'];
$slike            = '';

if ( $type == 'U izgradnji' ) {
	$type = 'Blue';
} elseif ( $type == 'U planu' ) {
	$type = 'Yellow';
} elseif ( $type == 'Obustavljeno' ) {
	$type = 'Red';
} else {
	$type = 'Green';
}

if ( $pocetak == '' ) {
	$pocetak = '0000-0-0';
}
if ( $zavrsetak == '' ) {
	$zavrsetak = '0000-0-0';
}
require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';

$files = $_FILES['image'];
foreach ( $files['name'] as $key => $value ) {
	if ( $files['name'][ $key ] ) {
		$file          = array(
			'name'     => $files['name'][ $key ],
			'type'     => $files['type'][ $key ],
			'tmp_name' => $files['tmp_name'][ $key ],
			'error'    => $files['error'][ $key ],
			'size'     => $files['size'][ $key ],
		);
		$_FILES        = array( 'upload_file' => $file );
		$attachment_id = media_handle_upload( 'upload_file', 0 );

		if ( is_wp_error( $attachment_id ) ) {
			// There was an error uploading the image.
			echo 'Error adding file';
		} else {
			// The image was uploaded successfully!
			echo 'File added successfully with ID: ' . $attachment_id . '<br>';
			$slike = $slike . wp_get_attachment_image_src( $attachment_id )[0] . '\n';
		}
	}
}
$slike      = trim( $slike, '\n' );
$connection = mysqli_connect( $db, $username, $password );
if ( ! $connection ) {
	die( 'Not connected : ' . mysqli_error() );
}

$db_selected = mysqli_select_db( $connection, $database );
if ( ! $db_selected ) {
	die( 'Can\'t use db : ' . mysqli_error() );
}

$query = 'INSERT INTO wp_maps_markers(type, lat, lon, objekat, investitor, opis, grad, ulica, broj, projektant, spratnost, linkovi,pocetak_gradnje,zavrsetak_gradnje, izvodjac, type_of_building, slike, povrsina_objekta, cena, broj_stanova, tag)
VALUES("' . $type . '","' . $lat . '","' . $lon . '","' . $objekat . '","' . $investitor . '","' . $opis . '","' . $grad . '","' . $ulica . '","' . $broj . '","' . $projektant . '","' . $spratnost . '","' . $linkovi . '","' . $pocetak . '","' . $zavrsetak . '","' . $izvodjac . '","' . $tipZgrade . '","' . $slike . '","' . $povrsina_objekta . '","' . $cena . '","' . $broj_stanova . '","' . $tag . '") ';

$post = mysqli_query( $connection, $query );
if ( ! $post ) {
	die( 'Couldnt insert' . mysqli_error( $connection ) );
}

echo 'Uspesno prosledjeno u bazu!';
mysqli_close( $connection );
