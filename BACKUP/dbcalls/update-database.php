<?php
require 'db.php';
require_once '../../../../wp-load.php';

global $wpdb;

$nonce     = wp_verify_nonce( $_POST['gradnja_nonce'], 'izmeni-gradiliste' );
$obj_id    = intval( $_POST['id'] );
$obj_type  = $_POST['type'];
$pocetak   = $_POST['pocetak_gradnje'];
$zavrsetak = $_POST['zavrsetak_gradnje'];

$obj_data = $_POST;

// Unset nonce and tags from db.
// Tags are only used as a setting mechanism in the frontend and,
// nonce is not needed in the database.
unset( $obj_data['gradnja_nonce'] );
unset( $obj_data['tags'] );

if ( 'U izgradnji' === $obj_type ) {
	$obj_type = 'Blue';
} elseif ( 'U planu' === $obj_type ) {
	$obj_type = 'Yellow';
} elseif ( 'Obustavljeno' === $obj_type ) {
	$obj_type = 'Red';
} else {
	$obj_type = 'Green';
}

if ( '' === $pocetak ) {
	$pocetak = '0000-0-0';
}

if ( '' === $zavrsetak ) {
	$zavrsetak = '0000-0-0';
}

require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';

$files = $_FILES['image'];
foreach ( $files['name'] as $key => $value ) {
	if ( $files['name'][ $key ] ) {
		$file          = array(
			'name'     => $files['name'][ $key ],
			'type'     => $files['type'][ $key ],
			'tmp_name' => $files['tmp_name'][ $key ],
			'error'    => $files['error'][ $key ],
			'size'     => $files['size'][ $key ],
		);
		$_FILES        = array( 'upload_file' => $file );
		$attachment_id = media_handle_upload( 'upload_file', 0 );

		if ( is_wp_error( $attachment_id ) ) {
			// There was an error uploading the image.
			echo 'Error adding file';
		} else {
			// The image was uploaded successfully!
			echo 'File added successfully with ID: ' . esc_attr( $attachment_id ) . '<br>';
			$slike = $slike . wp_get_attachment_image_src( $attachment_id )[0] . '\n';
		}
	}
}

$obj_data['type']              = $obj_type;
$obj_data['pocetak_gradnje']   = $pocetak;
$obj_data['zavrsetak_gradnje'] = $zavrsetak;
$obj_data['slike']             = trim( $slike, '\n' );

$condition = array( 'ID' => $obj_id );
$updated   = $wpdb->update( $wpdb->map_markers, $obj_data, $condition );

if ( ! $updated ) {
	echo $wpdb->last_error . ' FOR ' . $wpdb->last_query; // phpcs:ignore
	return;
}

echo 'Updated database successfuly.';
