<?php
//@phpcs:ignoreFile
require 'db.php';

//export.php
if ( isset( $_POST['export'] ) ) {
	$interval = $_POST['Interval'];

	$is_by_year = explode('-', $interval)[0] === 'year';

	$connect = mysqli_connect( $db, $username, $password, $database );
	header( 'Content-Type: text/csv; charset=utf-8' );
	header( 'Content-Disposition: attachment; filename=data.csv' );
	$output = fopen( 'php://output', 'w' );
	fputcsv( $output, array( 'Objekat', 'Investitor', 'Pocetak gradnje', 'Kraj gradnje', 'Izvodjac', 'Grad', 'Ulica', 'Broj', 'Projektant', 'Spratnost', 'Opis', 'Tip', 'Status' ) );

	if($is_by_year) {
		$interval = explode('year-', $interval)[1];
		$query = 'SELECT objekat, investitor, pocetak_gradnje, zavrsetak_gradnje, izvodjac, grad, ulica, broj, projektant, spratnost, opis, type_of_building, type from wp_maps_markers WHERE tag REGEXP ' . $interval . ' ORDER BY ' . $_POST['Order'] . ' DESC';
	} else {
		$query  = 'SELECT objekat, investitor, pocetak_gradnje, zavrsetak_gradnje, izvodjac, grad, ulica, broj, projektant, spratnost, opis, type_of_building, type from wp_maps_markers WHERE createdOn >= DATE_SUB(CURDATE(), INTERVAL ' . $interval . ' DAY) ORDER BY ' . $_POST['Order'] . ' DESC';
	}

	$result = mysqli_query( $connect, $query );
	mysqli_close( $connect );

	while ( $row = mysqli_fetch_assoc( $result ) ) {
		$status = $row['type'];
		$date_start = $row['pocetak_gradnje'];
		$date_end = $row['zavrsetak_gradnje'];

		if($date_start === '0000-0-0') {
			$row['pocetak_gradnje'] = '';
		}

		if($date_end === '0000-0-0') {
			$row['zavrsetak_gradnje'] = '';
		}

		if($status === 'Blue') {
			$row['type'] = 'U Izgradnji';
		} else if($status === 'Red') {
			$row['type'] = 'Obustavljeno';
		} else if($status === 'Green') {
			$row['type'] = 'Završeno';
		} else if($status === 'Yellow') {
			$row['type'] = 'U Planu';
		}

		 fputcsv( $output, $row );
	}
	 fclose( $output );
}
