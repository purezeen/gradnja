<?php
require 'db.php';
$building_id = $_GET['deleteSelect'];

$connection = mysqli_connect( $db, $username, $password );
if ( ! $connection ) {
	die( 'Not connected : ' . mysqli_error( $connection ) );
}

$db_selected = mysqli_select_db( $connection, $database );
if ( ! $db_selected ) {
	die( 'Can\'t use db : ' . mysqli_error( $connection ) );
}

$query = 'DELETE FROM wp_maps_markers WHERE id = "' . $building_id . '"';

$get_data = mysqli_query( $connection, $query );
if ( ! $get_data ) {
	die( 'Couldnt get ' . mysqli_error( $connection ) );
}
mysqli_close( $connection );
