<?php
/**
 * Theme enqueue scripts and styles.
 */

/**
 * Lib styles enqueues.
 */
function gradnja_lib_style_enqueue() {
	// Bootstrap css
	wp_enqueue_style(
		'bootstrap',
		get_template_directory_uri() . '/dist/css/bootstrap.css',
		array(),
		get_file_version( '/dist/css/bootstrap.css' ),
		'all'
	);

	// Lightcase css
	wp_enqueue_style(
		'lightcase',
		get_template_directory_uri() . '/css/lightcase.css',
		array(),
		get_file_version( '/css/lightcase.css' ),
		'all'
	);

	// Mmenu css
	wp_enqueue_style(
		'mmenu',
		get_template_directory_uri() . '/dist/css/mmenu.css',
		array(),
		get_file_version( '/dist/css/mmenu.css' ),
		'all'
	);

	// Select2 css
	wp_enqueue_style(
		'select2',
		get_template_directory_uri() . '/dist/css/select2.css',
		array(),
		get_file_version( '/dist/css/select2.css' ),
		'all'
	);
}
add_action( 'wp_enqueue_scripts', 'gradnja_lib_style_enqueue' );

/**
 * Lib scripts enqueues.
 */
function gradnja_lib_scripts_enqueue() {
	// Resize Senzor js
	wp_enqueue_script(
		'ResizeSensor',
		get_template_directory_uri() . '/dist/js/vendor/ResizeSensor.js',
		array(),
		get_file_version( '/dist/js/vendor/ResizeSensor.js' ),
		true
	);

	// Theia sticky sidebar js
	wp_enqueue_script(
		'theia-sticky-sidebar',
		get_template_directory_uri() . '/dist/js/vendor/theia-sticky-sidebar.js',
		array(),
		get_file_version( '/dist/js/vendor/theia-sticky-sidebar.js' ),
		true
	);

	// Masonry js
	wp_enqueue_script(
		'masonry',
		get_template_directory_uri() . '/dist/js/vendor/masonry.pkgd.js',
		array(),
		get_file_version( '/dist/js/vendor/masonry.pkgd.js' ),
		true
	);

	// MMenu
	wp_enqueue_script(
		'mmenu',
		get_template_directory_uri() . '/dist/js/vendor/mmenu.js',
		array(),
		get_file_version( '/dist/js/vendor/mmenu.js' ),
		true
	);

	// Select2 js
	wp_enqueue_script(
		'select2',
		get_template_directory_uri() . '/dist/js/vendor/select2.full.js',
		array(),
		get_file_version( '/dist/js/vendor/select2.full.js' ),
		true
	);

	// Lightcase addon - mobile swipe
	wp_enqueue_script(
		'lightcase-mobile-swipe',
		get_template_directory_uri() . '/dist/js/vendor/jquery.events.touch.js',
		array(),
		get_file_version( '/dist/js/vendor/jquery.events.touch.js' ),
		true
	);

	// Lightcase js
	wp_enqueue_script(
		'lightcase',
		get_template_directory_uri() . '/dist/js/vendor/lightcase.js',
		array(),
		get_file_version( '/dist/js/vendor/lightcase.js' ),
		true
	);

	// Flexslider js
	wp_enqueue_script(
		'flexslider',
		get_template_directory_uri() . '/dist/js/vendor/jquery.flexslider.js',
		array(),
		get_file_version( '/dist/js/vendor/jquery.flexslider.js' ),
		true
	);
}
add_action( 'wp_enqueue_scripts', 'gradnja_lib_scripts_enqueue' );

/**
 * Main scripts and styles.
 */
function gradnja_main_enqueue() {

	// jquery.mmenu.WordPress
	wp_enqueue_script(
		'jquery.mmenu.WordPress',
		get_template_directory_uri() . '/dist/js/custom/jquery.mmenu.wordpress.js',
		array(),
		get_file_version( '/js/custom/jquery.mmenu.wordpress.js' ),
		true
	);

	// Theme details stylesheet
	wp_enqueue_style(
		'theme-details',
		get_template_directory_uri() . '/style.css',
		array(),
		get_file_version( '/style.css' ),
		'all'
	);

	// Main stylesheet
	wp_enqueue_style(
		'theme',
		get_template_directory_uri() . '/dist/css/style.css',
		array(),
		get_file_version( '/dist/css/style.css' ),
		'all'
	);

	// Main js
	wp_enqueue_script(
		'main',
		get_template_directory_uri() . '/dist/js/custom/main.js',
		array(),
		get_file_version( '/dist/js/custom/main.js' ),
		true
	);

	// Maps script
	wp_enqueue_script(
		'maps',
		get_template_directory_uri() . '/dist/js/custom/maps.js',
		array(),
		get_file_version( '/dist/js/custom/maps.js' ),
		false
	);

	// Slide script
	wp_enqueue_script(
		'slide',
		get_template_directory_uri() . '/dist/js/custom/slide.js',
		array(),
		get_file_version( '/dist/js/custom/slide.js' ),
		false
	);

	// Validate fields script
	if ( is_page( array( 'dodaj-gradiliste', 'izmeni-gradiliste' ) ) ) {
		wp_enqueue_script(
			'validate',
			get_template_directory_uri() . '/dist/js/custom/validate.js',
			array(),
			get_file_version( '/dist/js/custom/validate.js' ),
			true
		);
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gradnja_main_enqueue' );
