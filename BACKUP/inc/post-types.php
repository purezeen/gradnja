<?php
/**
 * Theme custom post types.
 *
 * @package gradnja
 * @since 0.0.1
 */

/**
 * CPT Uradi sam.
 */
function cpt_uradi_sam() {

	$labels = array(
		'name'                  => _x( 'Uradi sam', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Uradi sam', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Uradi sam', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Uradi sam', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args   = array(
		'label'               => __( 'Custom post', 'gulp_wordpress' ),
		'description'         => __( 'Custom post Description', 'gulp_wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'main titile' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon'           => 'dashicons-hammer',
		'taxonomies'          => array(
			'post_tag',
			'category',
		),
	);
	register_post_type( 'uradi-sam', $args );

}

add_action( 'init', 'cpt_uradi_sam', 0 );
