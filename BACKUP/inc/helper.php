<?php
/**
 * Gradnja theme useful functions.
 *
 * @package gradnja
 * @since 0.0.1
 */

/**
 * Checks if file has been changed.
 *
 * @param string $file_path - file path relative to theme dir.
 * @return int
 */
function get_file_version( $file_path ) {
	return filemtime( get_stylesheet_directory() . $file_path );
}
