import { initCreatePolyline } from './routemap.js';

function initMain() {
    const $ = jQuery;
    let map, poly;

    const buildingPages =
        $('.page-izmeni-gradiliste').length ||
        $('.page-dodaj-gradiliste').length;
    let tags = buildingPages && [...$('#tag').val().split(' ')];

    const customLog =
        (logger = console.log) =>
        (message) =>
        logger(message),
        alertLog = customLog(window.alert),
        consoleLog = customLog();

    const clearTextField = (field) => setTimeout(() => field.val(''), 100);

    const createTag = (text) => {
        if (!text) {
            return;
        }

        const $tag = $(`
         <span class="label-tag" id="tag-${text}">
            <span class="label-tag__text">${text}</span>
            <span id="remove-tag" class="label-tag__remove"></span>
         </span>
        `);

        $tag.on('click', function() {
            $(this).fadeOut(300, function() {
                tags = tags.filter(
                    (tag) => $tag.find('.label-tag__text').text() !== tag
                );
                $('#tag').val(tags.join(' '));
                $(this).remove();
            });
        });

        return $tag;
    };

    function createTagField(field) {
        const $field = field instanceof jQuery ? field : $(field);

        $field.on('keypress', function(event) {
            const spaceIsPressed = event.keyCode === 32;

            let tag;

            if (spaceIsPressed) {
                tag = $field.val().split(' ')[0];

                const tagAdded = !!$field.parent().find(`#tag-${tag}`).length;

                if (tagAdded) {
                    alertLog('Tag već postoji.');
                    clearTextField($field);
                    return;
                }

                tags = [...tags, tag];

                $field
                    .parent()
                    .find('.label-tag-wrapper')
                    .append(createTag(tag));

                if (!tags[0]) {
                    tags = tags.slice(1);
                }

                $('#tag').val(tags.join(' '));
                clearTextField($field);
            }
        });

        return $field;
    }

    $(window).ready(function() {
        if (buildingPages) {
            const $investorSelect = $('#investitor-select').select2({
                tags: true,
            });
            const $architectSelect = $('#projektant-select').select2({
                tags: true,
            });
            const $performerSelect = $('#izvodjac-select').select2({
                tags: true,
            });

            [$investorSelect, $architectSelect, $performerSelect].forEach(
                (select) => {
                    select.on('select2:select', function() {
                        const hiddenInputId = $(this)
                            .attr('id')
                            .split('-select')
                            .join('');
                        const value = $(this).find('option:selected').val();

                        const hiddenInput = $(`#${hiddenInputId}`);
                        hiddenInput.val(value);
                    });
                }
            );

            $('#delete-image').on('click', function() {
                $('#image-wrapper').remove();
                $('#image-url').text(' ');
            });

            // toggle lat & lon fields on checkbox click
            $('#putanja').on('click', function() {
                $('#lat').toggleClass('novalidate');
                $('#lon').toggleClass('novalidate');
            });
        }

        if ($('.page-izmeni-gradiliste').length) {
            var search = $('#change-select'),
                rootPath = '/wp-content/themes/gradnja';

            search.select2();
            $('#delete-select').select2();

            search.on('select2:select', function() {
                const selectedOption = $('#change-select option:selected');
                const data = `changeSelect=${selectedOption
                    .data('id')
                    .split('objekat-')
                    .join('')}`;

                const clearMap = $('#clear');
                clearMap.trigger('click');

                $('#image-wrapper').remove();
                const imageWrapper = $(
                    `<div id="image-wrapper" class="image-wrapper"></div>`
                );
                $('#btns').parent().append(imageWrapper);

                $.ajax({
                        method: 'get',
                        url: rootPath + '/dbcalls/get-by-id.php',
                        data,
                    })
                    .done(function(data) {
                        data = JSON.parse(data);
                        let result = data[0];

                        $('#id').val(result.id);
                        $('#objekat').val(result.objekat);
                        $('#grad').val(result.grad);
                        $('#ulica').val(result.ulica);
                        $('#broj').val(result.broj);
                        $('#opis').val(result.opis);
                        $('#investitor').val(result.investitor);
                        $('#projektant').val(result.projektant);
                        $('#spratnost').val(result.spratnost);
                        $('#izvodjac').val(result.izvodjac);
                        $('#pocetak').val(result.pocetak_gradnje);
                        $('#zavrsetak').val(result.zavrsetak_gradnje);
                        $('#lat').val(result.lat);
                        $('#lon').val(result.lon);
                        $('#cena').val(result.cena);
                        $('#image-url').text(result.slike);
                        $('#povrsina').val(result.povrsina_objekta);
                        $('#linkovi').val(result.linkovi);
                        $('#brojStanova').val(result.broj_stanova);
                        $('#tags').val(result.tag);
                        $('#tag').val(result.tag);

                        $('#lat').removeClass('novalidate');
                        $('#lon').removeClass('novalidate');

                        if (result.putanje) {
                            const id = result.id;
                            const putanje = JSON.parse(
                                result.putanje.replace(/\\/g, '')
                            ); // Necessary to 'unescape' this string ( '\' character ) escaped by wordpress since this is retreived from the WordPress database.

                            var mapOptions = {
                                center: {
                                    lat: putanje[id][0].lat,
                                    lng: putanje[id][0].lng,
                                },
                                zoom: 12,
                                zoomControl: true,
                            };
                            map = new google.maps.Map(
                                document.getElementById('map'),
                                mapOptions
                            );

                            const latLng = new google.maps.LatLng(
                                parseFloat(mapOptions.center.lat),
                                parseFloat(mapOptions.center.lng)
                            );

                            $('#lat').addClass('novalidate');
                            $('#lon').addClass('novalidate');
                            $('#putanja').prop('checked', true);
                            $('#putanje').text(JSON.stringify(putanje));
                            $('#lat').val(JSON.stringify(latLng.lat()));
                            $('#lon').val(JSON.stringify(latLng.lng()));

                            map.setZoom(12);
                            map.setCenter({
                                lat: putanje[id][0].lat,
                                lng: putanje[id][0].lng,
                            });

                            const roadPath = new google.maps.Polyline({
                                path: putanje[id],
                                geodesic: true,
                                strokeColor: '#FF0000',
                                strokeOpacity: 1.0,
                                strokeWeight: 3,
                            });

                            roadPath.setMap(map);
                            $('#clear').on('click', function() {
                                roadPath.setMap(null);
                                initCreatePolyline(map);
                            });
                        }

                        $('[data-name=investitor]')
                            .val(result.investitor)
                            .change();
                        $('[data-name=projektant]')
                            .val(result.projektant)
                            .change();
                        $('[data-name=izvodjac]').val(result.izvodjac).change();

                        tags = [...result.tag.toString().split(' ')];

                        const images = $('#image-url').text().split('\n');

                        images.forEach((image) => {
                            const imageComponent = $(
                                `<div class="image"><img src="${image}" width="150" height="150" /></div>`
                            );
                            if (!image || image === '\n') return;

                            imageWrapper.append(imageComponent);
                        });

                        if (result.type === 'Blue')
                            $('#tip').val('U izgradnji').change();
                        if (result.type === 'Yellow')
                            $('#tip').val('U planu').change();
                        if (result.type === 'Green')
                            $('#tip').val('Zavrseno').change();
                        if (result.type === 'Red')
                            $('#tip').val('Obustavljeno').change();
                        $('#tipZgrade').val(result.type_of_building).change();

                        const $tagWrapper = $('.label-tag-wrapper');

                        $tagWrapper.children().remove();
                        const $tags = $('#tags')
                            .val()
                            .split(' ')
                            .map((tag) => createTag(tag));

                        $tagWrapper.append($tags);

                        clearTextField($('#tags'));
                    })
                    .fail(function(req, e) {
                        alert(
                            'Desila se greška pri pretraživanju. Molimo pokušajte ponovo'
                        );
                    })
                    .always(function() {});
            });

            createTagField($('#tags'));

            $('#izmeni').click(function(e) {
                e.preventDefault();
                if (!$('#objekat').val()) {
                    alert('Pogrešno formatirani podaci!');
                    $('#objekat').css('color', 'red');
                    return;
                }
                if (isNaN(parseFloat($('#lon').val()))) {
                    alert('Pogrešno formatirani podaci!');
                    $('#lon').css('color', 'red');
                    return;
                }
                if (isNaN(parseFloat($('#lat').val()))) {
                    alert('Pogrešno formatirani podaci!');
                    $('#lat').css('color', 'red');
                    return;
                }

                let formData = new FormData($('#formid').get(0));
                $.ajax({
                        method: 'post',
                        url: rootPath + '/dbcalls/update-database.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        cache: false,
                    })
                    .done(function(data) {
                        if (!JSON.parse(data).updated) {
                            alert('Desila se greška, podaci nisu ažurirani');
                        } else {
                            alert('Podaci ažurirani');
                        }
                    })
                    .fail(function(req, e) {
                        console.log('error', req);
                        alert('Desila se greška, podaci nisu ažurirani');
                    });
            });

            $('#obrisi').click(function(e) {
                const selectedOption = $('#delete-select option:selected');
                const data = `deleteSelect=${selectedOption
                    .data('id')
                    .split('objekat-')
                    .join('')}`;

                e.preventDefault();
                $.ajax({
                        method: 'get',
                        url: rootPath + '/dbcalls/remove-from-database.php',
                        data,
                    })
                    .done(function(req) {
                        console.log(req);
                        alert('Podaci ažurirani');
                        selectedOption.remove();
                    })
                    .fail(function(req, e) {
                        console.log(req);
                        alert('Desila se greška, podaci nisu ažurirani');
                    });
            });
        }

        if ($('.page-dodaj-gradiliste').length) {
            const $tagWrapper = $('.label-tag-wrapper'),
                $tags = $('#tags');

            $tagWrapper.children().remove();

            if ($tagWrapper.children().length) {
                $tags
                    .val()
                    .split(' ')
                    .map((tag) => createTag(tag));

                $tagWrapper.append($tags);
            }

            clearTextField($tags);
            createTagField($tags);
        }

        if (buildingPages) {
            const hybridSearch = $('.select2.field');
            hybridSearch.each((i, search) =>
                $(search).on('select2:select', function() {
                    const name = $(this).data('name');

                    $(`#${name}`).val($(this).val());
                })
            );
        }

        /*========= FLEXSLIDER ======= */

        $('.blog-slider').flexslider({
            animation: 'fade',
            animationLoop: true,
            initDelay: 1000,
            animationSpeed: 1000,
            controlNav: false,
            controlsContainer: $('.custom-controls-container-blog'),
            customDirectionNav: $('.custom-navigation-blog a'),
            start: function(slider) {
                var timer;
                $('.blog-slider-mini-list .widget-post').hover(
                    function() {
                        clearTimeout(timer);
                        slider.pause();
                        var $this = $(this);
                        timer = setTimeout(function() {
                            $('.blog-slider').flexslider($this.index());
                            $('.blog-slider .blog-slider-content').addClass(
                                'animate'
                            );
                        }, 500);
                    },
                    function() {
                        $('.blog-slider .blog-slider-content').removeClass(
                            'animate'
                        );
                        clearTimeout(timer);
                        timer = setTimeout(function() {
                            $('.blog-slider').flexslider('play');
                            $('.blog-slider .blog-slider-content').removeClass(
                                'animate'
                            );
                        }, 500);
                    }
                );
            },
        });

        $('.flexslider-latest-post').flexslider({
            animation: 'slide',
            animationLoop: false,
            controlsContainer: $('.custom-controls-container'),
            customDirectionNav: $('.custom-navigation a'),
            controlNav: false,
        });

        $('.flexslider-jobs').flexslider({
            animation: "slide",
            animationLoop: false,
            controlsContainer: $(".custom-controls-container-jobs"),
            customDirectionNav: $(".custom-navigation-jobs a"),
            controlNav: false
        });


        // Flexslider carousel
        var $window = $(window),
            flexslider = {
                vars: {},
            };

        // tiny helper function to add breakpoints
        function getGridSize() {
            return window.innerWidth < 400 ?
                1 :
                window.innerWidth < 600 ?
                2 :
                window.innerWidth < 900 ?
                3 :
                3;
        }

        $('.flexslider-carousel').flexslider({
            animation: 'slide',
            animationLoop: false,
            controlsContainer: $('.custom-controls-container-carousel'),
            customDirectionNav: $('.custom-navigation-carousel a'),
            controlNav: false,
            itemWidth: 210,
            itemMargin: 30,
            minItems: getGridSize(), // use function to pull in initial value
            maxItems: getGridSize(), // use function to pull in initial value
        });


        // check grid size on resize event
        $window.resize(function() {
            var gridSize = getGridSize();

            flexslider.vars.minItems = gridSize;
            flexslider.vars.maxItems = gridSize;
        });
    });

    /* ======= SCROLL TOP ======= */
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 200) {
            // If page is scrolled more than 50px
            $('.scroll').fadeIn(200); // Fade in the arrow
        } else {
            $('.scroll').fadeOut(200); // Else fade out the arrow
        }
    });

    $('.scroll').click(function() {
        $('html, body').animate({
                scrollTop: 0,
            },
            800
        );
        return false;
    });

    /* ===== LIGHTCASE ======== */
    jQuery(document).ready(function($) {
        $('.post-inner-content figure > a').attr(
            'data-rel',
            'lightcase:myCollection'
        );
        $('.post-inner-content  a img')
            .parent()
            .attr('data-rel', 'lightcase:myCollection');
        $('.grid-gallery a').attr('data-rel', 'lightcase:myCollection');
        $('.page figure a').attr('data-rel', 'lightcase');
        $('.wp-block-gallery a').attr('data-rel', 'lightcase:blockGallery');

        /* checking image function */
        function isValidImageURL(str) {
            if (typeof str !== 'string') return false;
            return !!str.match(/\w+\.(jpg|jpeg|gif|png|tiff|bmp)$/gi);
        }
        /* Check if url is image, if not, delete data-rel for lightbox functionality*/
        $('a[data-rel^=lightcase]').each(function(index, el) {
            var $url = $(this).attr('href');
            if (!isValidImageURL($url)) {
                $(this).removeAttr('data-rel');
            }
        });

        $('a[data-rel^=lightcase]').lightcase({
            maxWidth: 1600,
            maxHeight: 1600,
            shrinkFactor: 0.85,
            onInit: {
                custom: function() {
                    if ($(this).next('.wp-caption-text').length) {
                        var caption = $(this).next('.wp-caption-text').html();

                        if (caption.length) {
                            $(this).attr('data-lc-caption', caption);
                        }
                        // lightcase.resize();
                    }
                },
            },
        });

        $('.post-inner-content embed').wrap("<div class='iframe-wrap'></div>");
    });

    /* ======= MAIN NAVIGATION ========= */
    // if ($(window).width() <= 1300) {
    var $menu = $('#main-nav').mmenu({
        extensions: [
            'border-full',
            'fx-menu-slide',
            'pageshadow',
            'effect-listitems-slide',
            'position-left',
            'pagedim-black',
        ],
        navbars: [{
            position: 'bottom',
            content: [
                '<a href="https://www.facebook.com/Gradnja/" target="_blank"><i class="fa fa-facebook"></i></a>',
                '<a href="https://twitter.com/gradnjars" target="_blank"><i class="fa fa-twitter"></i></a>',
                '<a href="https://www.instagram.com/gradnja/" target="_blank"><i class="fa fa-instagram"></i></a>',
                '<a href="https://www.youtube.com/gradnjatv" target="_blank"><i class="fa fa-youtube-play"></i></a>',
            ],
        }, ],

        wrappers: ['wordpress'],
        navbar: {
            title: 'Gradnja',
        },
    }, {
        clone: true,
        offCanvas: {
            pageSelector: '.wrapper',
            clone: true,
        },
    });

    var $icon = $('#my-icon');
    var API = $menu.data('mmenu');

    $icon.on('click', function() {
        API.open();
    });

    API.bind('open:finish', function() {
        setTimeout(function() {
            $icon.addClass('is-active');
        });
    });
    API.bind('close:finish', function() {
        setTimeout(function() {
            $icon.removeClass('is-active');
        });
    });
    // }

    /* ======= Fixed sticky header when scrolling ======== */
    if ($(window).width() <= 1300) {
        var promo = $('.top-promo-header').height();
        var total_height = promo + 54;
    } else {
        var header = $('.header-wrap').height();
        var promo = $('.top-promo-header').height();
        var total_height = promo + header;
    }

    $(window).scroll(function() {
        if ($(this).scrollTop() >= total_height) {
            $('.progress-container').addClass('fixed-progress');
        } else {
            $('.progress-container').removeClass('fixed-progress');
        }
    });

    var progress_height =
        $('.inner-post-content').height() +
        $('.header-post-image').height() +
        300;

    /* ======== SCROLL INDICATOR ======== */
    $(window).on('scroll', function() {
        if ($(this).scrollTop() >= total_height) {
            var docHeight = progress_height,
                winHeight = $(window).height();

            var viewport = docHeight - winHeight,
                positionY = $(window).scrollTop();
            var indicator = (positionY / viewport) * 100;

            $('.progress-bar').css('width', indicator + '%');
        } else {
            $('.progress-bar').css('width', 0 + '%');
        }
    });

    /* ======== INTELIGENT STICKY SIDEBARS - THEIA STICKY SIDEBAR ======== */
    if ($(window).width() >= 992) {
        $('.sidebar').theiaStickySidebar({
            additionalMarginTop: 100,
        });
        $('.vertical-social-icon').theiaStickySidebar({
            additionalMarginTop: 100,
        });
    }
    if ($(window).width() >= 768) {
        $('.vertical-social-icon').theiaStickySidebar({
            additionalMarginTop: 100,
        });
    }

    $('.style-select').select2();
    $('.select2').select2();

    //  Map height
    var topHeader = $('.push-content').height();
    var Promo = $('.promo-header').height();
    var sabaiform = $('.sabai-directory-search').height();
    var sabainav = $('.sabai-navigation').height();

    var totalHeight2 = topHeader + Promo + sabaiform + sabainav;

    $('.page-template-adresar .sabai-directory-map-container .sabai-directory-map.sabai-googlemaps-map').css('height', 'calc(100vh + ' + totalHeight2 + 'px)');
    $('.page-template-adresar .sabai-directory-map-container').css(
        'height',
        'calc(100vh + ' + totalHeight2 + 'px)'
    );

    $(document).ajaxStop(function() {
        $('.sabai-directory-map-container .sabai-directory-map.sabai-googlemaps-map').css('height', 'calc(100vh + ' + totalHeight2 + 'px)');
        $('.sabai-directory-map-container').css(
            'height',
            'calc(100vh + ' + totalHeight2 + 'px)'
        );
    });

    $(window).ready(function() {
        /* ====== MASONRY ======= */
        if ($(window).width() > 594) {
            const $grid_loop = $('.grid').masonry({
                itemSelector: '.grid-item',
                // use element for option
                columnWidth: '.grid-sizer',
                gutter: 30,
                percentPosition: true,
                horizontalOrder: true,
            });
            setTimeout(function() {
                $grid_loop.masonry('layout');
            }, 500);

            const $grid = $('.grid-uradi-sam ').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                gutter: 30,
                animate: true,
                percentPosition: true,
                horizontalOrder: true,
                transitionDuration: '0.2s',
            });
            setTimeout(function() {
                $grid.masonry('layout');
            }, 500);
        }

        $('.grid-uradi-sam .grid-item .btn-expand').on('click', function() {
            $(this).prev().children('li:nth-child(n+5)').slideToggle();

            var condition = $(this).attr('rev');
            if (condition == 'more') {
                $(this).html(
                    'Prikaži više <span class="iconify" data-icon="fe-arrow-right" data-inline="false"></span>'
                );

                $(this).attr('rev', 'less');
            } else {
                $(this).html(
                    'Prikaži manje <span class="iconify" data-icon="fe-arrow-up" data-inline="false"></span>'
                );
                $(this).attr('rev', 'more');
            }
            if ($(window).width() > 594) {
                setTimeout(function() {
                    $grid.masonry('layout');
                }, 500);
            }
        });
    });
    $(window).load(function() {
        //        $gridgallery = $('.grid-gallery').masonry({
        //          itemSelector: '.grid-gallery > p',
        //          columnWidth: '.grid-sizer',
        //          gutter: 30,
        //          animate: true,
        // //          percentPosition: true,
        //          horizontalOrder: true,
        //          transitionDuration: '0.2s'
        //       });
        // 	     setTimeout(function() {
        //          $gridgallery.masonry('layout');
        //      	 }, 1000);
    });

    $(document).ready(function() {
        $('img').load(function() {
            $('.grid-gallery').masonry({
                itemSelector: '.grid-gallery > p',
                columnWidth: '.grid-sizer',
                gutter: 30,
                animate: true,
                //          percentPosition: true,
                horizontalOrder: true,
                //          transitionDuration: '0.2s'
            });
        });

        $('.grid-gallery').masonry();
    });

    /*----------  Masonry gallery ----------*/
    $(document).ready(function() {

        $('.masonry-gallery').masonry({
            columnWidth: '.grid-sizer',
            itemSelector: '.post-content-gallery-item',
            percentPosition: true,
            gutter: 12,
            animate: true,
            horizontalOrder: true,
        });

        $('.masonry-gallery img').load(function() {
            $('.masonry-gallery').masonry({
                columnWidth: '.grid-sizer',
                itemSelector: '.post-content-gallery-item',
                percentPosition: true,
                gutter: 12,
                animate: true,
                horizontalOrder: true,
            });
        });


    });

    /* ====== VIDEO ASPECT RATIO ======== */
    $('p iframe').wrap('<div class="iframe-wrap"></div>');
    $('p img').parent().css('margin-bottom', '40px');

    $('.header-wrap #searchform .search-icon').click(function(e) {
        if (!$('#searchform button').hasClass('enabled')) {
            event.preventDefault();
            $('.header-wrap #searchform button').prop('disabled', true);
            $('.header-wrap .search-wrap').addClass('open');
            $('.header-wrap #searchform .search-field').focus();
            setTimeout(function() {
                $('.header-wrap #searchform button')
                    .prop('disabled', false)
                    .addClass('enabled');
            }, 500);
        }
    });
    $('.header-wrap #searchform .search-field').click(function(e) {
        e.stopPropagation();
    });
    $(document).click(function(e) {
        if ($('.header-wrap #searchform button').hasClass('enabled')) {
            $('.header-wrap #searchform button').removeClass('enabled');
            $('.header-wrap .search-wrap').removeClass('open');
        }
    });

    /* ========= SMOOT SCROLL ========= */
    $('.inner-post-content a[href*=\\#]:not([href=\\#])').click(function() {
        if (
            location.pathname.replace(/^\//, '') ==
            this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length ?
                target :
                $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                        scrollTop: target.offset().top,
                    },
                    1500
                );
                return false;
            }
        }
    });
    /* ========= END SMOOT SCROLL ========= */

    /* ====== MAP LIST ======= */
    $('.map-list ol >li').prepend(
        '<a class="js-custom-slueve-option"><span class="js-arrow-down"></span></a>'
    );

    $('.map-list ol >li').click(function() {
        $(this).find('span').toggleClass('js-arrow-down js-arrow-up');
    });

    $('.map-list ol ul').hide();
    $('.map-list ol >li ').on('click', function() {
        $(this).find('ul').first().slideToggle();
        return false;
    });

    $('#exitpopup-modal .newsletter-box .modal-close').on('click', function() {
        $('#exitpopup-modal').hide();
    });
}

$(function() {
    initMain();
});