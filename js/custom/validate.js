const patterns = {
    text: /^[A-Za-zŠĐČĆŽšđčćž0-9\s()\-\/,.+]+$/,
    number: /^\d{1,8}$/,
    street: /^\d{1,4}[abcdef]?$/,
    range: /^\d{1,7}(\s\-\s\d{1,7})?$/, // Match single values or ranges.
    tag: /^\d{4}\s?$/,
    surface: /^\d{1,7}(\s\-\s\d{1,7})?\s?(m2)$/,
    lat: /^(\+|-)?(?:90(?:(?:\.0+)?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]+)?))$/,
    lon: /^(\+|-)?(?:180(?:(?:\.0+)?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]+)?))$/,
    file: /\w+/,
};

const formats = {
    text: 'Popovica Lux, Gra-Vet Invest, NS Invest 021',
    number: '1, 17, 200, 5100',
    street: '10a-f, 10, 114, 2500a-f',
    range: '100, 1500 - 3000, 20000 - 400000',
    tag: '1980, 2022, 2025, 2050',
    surface: '50m2, 200 m2, 500 - 1250 m2',
    lat: '+90.0, 45, -90, 45.25225',
    lon: '-127.55642, 179.99999, +180, -180',
    fileLimit: 2048,
    file: 'File is too large, try uploading file less than ' +
        this.fileLimit +
        'MB',
};

/**
 * Checks for a character match.
 *
 * @param { RegExp } regex - Regex to match against.
 * @returns { boolean }
 */
function validate(regex) {
    return function(value) {
        return regex.test(value);
    };
}

/**
 * Negates the output of a function.
 *
 * @param { Function } fn - Function to negate.
 * @returns { boolean }
 */
function not(fn) {
    return function(...args) {
        return !fn(...args);
    };
}

const fields = document.querySelectorAll(
    '.addPageDiv input:not([type="submit"]):not(.novalidate), .addPageDiv textarea'
);

function createTutorialPopup(message) {
    const fieldMessage = document.createElement('span');
    const separator = ':';

    return function(type) {
        const description = `${message}${separator} <br> ${type}`;

        fieldMessage.classList.add('message');
        fieldMessage.innerHTML = description;

        return fieldMessage;
    };
}

function animateToField(field) {
    const whitespace = $(field).height() * 4; // give some room from the top of the window.

    $('html, body').animate({
        scrollTop: $(field).offset().top - whitespace,
    });

    $(field).focus();
}

function validateField(field) {
    const fieldType =
        field.getAttribute('data-type') || field.getAttribute('type');

    const pattern = !patterns[fieldType] ? /\w+/ : patterns[fieldType];

    const validateFieldValue = validate(pattern);
    let fieldValid = validateFieldValue(field.value);
    const { parentNode } = field;
    const message = createTutorialPopup('Polje nije validno. Dozvoljen format');
    const messageSelector = parentNode.querySelector('.message');
    const nonRequiredField = !field.required && (!field.value || field.value === '');

    // Skip validation if field is not required but value is empty.
    if (nonRequiredField) {
        fieldValid = true;
    }

    if (!fieldValid && !messageSelector) {
        field.classList.add('invalid');
        parentNode.append(message(formats[fieldType]));
    }

    if (fieldValid && messageSelector) {
        field.classList.remove('invalid');
        messageSelector.remove();
    }

    return fieldValid;
}

fields.forEach((field) => {
    field.addEventListener('input', (e) => {
        validateField(field);
    });
});

const form = document.querySelector('.page-dodaj-gradiliste #formid');

if (form) {
    form.addEventListener('submit', function(e) {
        e.preventDefault();

        const fields = [
            ...document.querySelectorAll(
                '.addPageDiv input:not(#tags):not(.novalidate):not([type="submit"]):not([type="button"])'
            ),
        ];

        const validatedFields = fields.map((field) => {
            const isValid = validateField(field);
            const firstInvalid = fields.filter((field) =>
                field.classList.contains('invalid')
            )[0];

            if (!isValid) {
                animateToField(firstInvalid);
            }

            return isValid;
        });

        const validFields = validatedFields.every((value) => value);

        if (validFields) {
            let formData = new FormData($(this).get(0));

            $.ajax({
                    method: 'post',
                    url: '/wp-content/themes/gradnja/dbcalls/add-to-database.php',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                })
                .done(function(data) {
                    console.log(JSON.parse(data).added);
                    if (!JSON.parse(data).added) {
                        alert(
                            'Desila se greška, podaci nisu prosledjeni u bazu podataka.'
                        );
                    } else {
                        $('.select2').val(null).trigger('change');
                        $('#investitor').val(null).trigger('change');
                        $('#investitor-select').val(null).trigger('change');
                        $('#projektant').val(null).trigger('change');
                        $('#projektant-select').val(null).trigger('change');
                        $('#izvodjac').val(null).trigger('change');
                        $('#izvodjac-select').val(null).trigger('change');

                        $('#select2-investitor-select-container').prop(
                            'title',
                            '-- Izaberi Investitora --'
                        );
                        $('#select2-investitor-select-container').text(
                            '-- Izaberi Investitora --'
                        );

                        $('#select2-projektant-select-container').prop(
                            'title',
                            '-- Izaberi Projektanta --'
                        );
                        $('#select2-projektant-select-container').text(
                            '-- Izaberi Projektanta --'
                        );

                        $('#select2-izvodjac-select-container').prop(
                            'title',
                            '-- Izaberi Izvodjača --'
                        );
                        $('#select2-izvodjac-select-container').text(
                            '-- Izaberi Izvodjača --'
                        );

                        $('#formid').trigger('reset');
                        $('#tags').children().remove();
                        alert('Podaci prosleđeni u bazu podataka');
                    }
                })
                .fail(function(req, e) {
                    console.log('error', req);
                    alert(
                        'Desila se greška, podaci nisu prosleđeni u bazu podataka'
                    );
                });
        }
    });
}