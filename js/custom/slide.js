$(document).ready(function () {
    const $slideInfo = $('.slide_info');
    const $mapInfo = $('.map__info');
    const $slideSearch = $('.slide_search');
    const $mapSearch = $('.map__search');

    $slideInfo.on('click', function () {
        const $this = $(this);

        if ($this.hasClass('active')) {
            $mapInfo.animate({ left: -400 });
        } else {
            $mapInfo.animate({ left: 0 });
        }

        $this.toggleClass('active');
    });

    $slideSearch.on('click', function () {
        const $this = $(this);

        if ($this.hasClass('active')) {
            $mapSearch.animate({ right: -400 });
        } else {
            $mapSearch.animate({ right: 0 });
        }

        $this.toggleClass('active');
    });
});
