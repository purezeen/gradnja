(function ($) {
    var mediaUploader;

    $('#upload-image').click(function (e) {
        e.preventDefault();
        // If the uploader object has already been created, reopen the dialog
        if (mediaUploader) {
            mediaUploader.open();
            return;
        }
        // Extend the wp.media object
        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image',
            },
            multiple: true,
        });

        // When a file is selected, grab the URL and set it as the text field's value
        mediaUploader.on('select', function () {
            const attachments = mediaUploader.state().get('selection').toJSON();
            const urls = attachments.map((attachment) => attachment.url);
            $('#image-url').text(urls.join('\n'));

            const images = $('#image-url').text().split('\n');

            const imageWrapper = $(
                `<div id="image-wrapper" class="image-wrapper"></div>`
            );

            $('#image-wrapper').remove();

            $('#btns').parent().append(imageWrapper);

            images.forEach((image) => {
                const imageComponent = $(
                    `<div class="image"><img src="${image}" width="150" height="150" /></div>`
                );
                if (!image || image === '\n') return;

                imageWrapper.append(imageComponent);
            });
        });
        // Open the uploader dialog
        mediaUploader.open();
    });
})(jQuery);
