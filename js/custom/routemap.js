var map;
var poly;

function initMap() {
    var mapOptions = {
        center: {
            lat: 44.187197,
            lng: 20.457273,
        },
        zoom: 7,
        zoomControl: true,
    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    initClearMapBtn(map);
    initCreatePolyline(map);

    var autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('autoc')
    );
    autocomplete.bindTo('bounds', map);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
    });
}

export function initClearMapBtn(currentMap = map) {
    const clearMapNode = document.getElementById('clear');

    if (clearMapNode) {
        // Clear button. Click to remove all polylines.
        clearMapNode.addEventListener('click', function (event) {
            event.preventDefault();

            poly.setMap(null);
            initCreatePolyline(currentMap);
            $('#putanje').text('');
            return false;
        });
    }
}

export function initCreatePolyline(map) {
    poly = new google.maps.Polyline({
        strokeColor: 'red',
        strokeOpacity: 1.0,
        strokeWeight: 4,
    });
    poly.setMap(map);
    // Add a listener for the click event
    map.addListener('click', addLatLng);
}

// Handles click events on a map, and adds a new point to the Polyline.
export function addLatLng(event) {
    const path = poly.getPath();

    /**
     * Tag functionality on main map depends on storing
     * the building id with drawn path.
     */

    $.ajax({
        method: 'get',
        url: '/wp-content/themes/gradnja/dbcalls/get-last-id.php',
    })
        .done(function (response) {
            let lastId = response;
            const requestFrom = window.location.pathname;

            if (requestFrom === '/izmeni-gradiliste/') {
                lastId = $('#id').val();
            }

            const pathObj = {
                [lastId]: path.Hd,
            };

            // Because path is an MVCArray, we can simply append a new coordinate
            // and it will automatically appear.
            path.push(event.latLng);
            $('#putanje').text(JSON.stringify(pathObj));
        })
        .fail(function (e) {
            console.error(e.message);
        });
}

$(function () {
    initMap();
});
