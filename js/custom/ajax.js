// Ajax.js 

jQuery(function($) {

    // On filter submit or pagination number click listener
    $('body').on('click', '.filter-submit, .pagination-ajax__item', function(e) {

        e.preventDefault();

        // check for current pagination number, and if exsists put it in the page variable
        if ($('#pagination-ajax').find('.current').data('number')) {
            var page = $('#pagination-ajax').find('.current').data('number');
        }

        // if clicked on some of pagination number, get pagination number(data-number) and put it in the page variable
        if ($(this).data('number')) {
            var page = $(this).attr('data-number');
        }

        // if clicked on next/prev in pagination number list, increase/decrease page variable
        if ($(this).data('next')) {
            page++;
        }
        if ($(this).data('prev')) {
            page--;
        }

        // if the location is selected, put it in the location variable
        if ($('#location option:selected').length) {
            var location = $('#location option:selected').val();
        }
        // if the category is selected, put it in the category variable
        if ($('#category option:selected').length) {
            var category = $('#category option:selected').val();
        }
        // if the sorting is selected, put the value in the sorting variable
        if ($('#sorting option:selected').length) {
            var sorting = $('#sorting option:selected').val();
        }

        $.ajax({
            url: ajaxUrl.ajaxurl,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'myfilter',
                location: location,
                category: category,
                sorting: sorting,
                page: page
            },
            beforeSend: function() {
                $(".loading").fadeIn('300'); // show loader until results are displayed/returned
            },
            success: function(data) {
                // console.log(data);
                // if there is at least one post, then insert posts and updated pagination
                if (data.posts.length > 0) {
                    $('#response').html(data.posts); // insert posts
                    $('#pagination-ajax').html(pagination(data.pages, data.current_page)); // insert pagination ( pagination(number, current_page) function used )
                } else {
                    // if there no posts, insert 'no-posts' message and remove pagination
                    var no_posts = "<h4 class='no-posts'>Žao nam je, od traženog sadržaja nema rezultata</h4>";
                    $('#response').html(no_posts);
                    $('#pagination-ajax').empty();
                }

            },
            complete: function() {
                $(".loading").fadeOut('300'); // hide loader when results are displayed/returned
            }
        });
        // filter apply or pagination number is clicked, when the result is returned, then scroll to top of jobs section
        $("html, body").animate({
            scrollTop: $('.jobs').offset().top
        }, 700);

        return false;
    });


    // Custom function 
    // used in ajax success to display updated ajax-pagination
    // takes the max number of pagination pages, and current_page
    function pagination(number, current_page) {
        // show pagination, if there is more than one pagination page
        if (number > 1) {
            var html = '<ul class="page-numbers">';
            if (current_page > 1) {
                html += '<li><a class="pagination-ajax__item prev page-numbers" href="#" data-prev="prev">prev</a></li>';
            }
            var i = 1;
            while (i <= number) {

                html += '<li><a class="pagination-ajax__item page-numbers';
                if (i == current_page) {
                    html += ' current';
                }
                html += '" href="#" data-number="' + i + '">' + i + '</a></li>';
                i++;
            }
            if (current_page < number && number > 1) {
                html += '<li><a class="pagination-ajax__item page-numbers next" href="#" data-next="next">next</a></li>';
            }
            html += '</ul>';
        } else {
            html = "";
        }
        return html;
    }



});