let map,
    allMarkers = [],
    allPaths = [],
    infowindow,
    searchBox,
    autocomplete,
    roadPath,
    tags = [];

function initMap() {
    let mapParams = {
        center: {
            lat: 44.187197,
            lng: 20.457273,
        },
        mapTypeId: 'hybrid',
        zoom: 7,
        zoomControl: true,
        landmark: 2,
        zoomControlOiptions: {
            position: google.maps.ControlPosition.RIGHT_TOP,
        },
        mapTypeControl: false,
        fullscreenControl: false,
        fullscreenControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP,
        },
    };
    map = new google.maps.Map(document.getElementById('map'), mapParams);
    let infoWindow = new google.maps.InfoWindow();
    let width =
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;
    let markersURL = templateUrl + '/dbcalls/markersToXML.php';
    if (width < 500) {
        map.zoomControl = false;
        map.mapTypeControl = false;
        map.zoom = 6.5;
    }
    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map);

    centerControlDiv.index = 0;
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(centerControlDiv);

    var logoControlDiv = document.createElement('DIV');
    logoControlDiv.classList.add('gradnja-logo-control');
    var logoControl = new MyLogoControl(logoControlDiv);
    logoControlDiv.index = 0; // used for ordering
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(logoControlDiv);

    // Change this depending on the name of your PHP or XML file
    downloadUrl(markersURL, (data) => {
        markersCallback(data, map, infoWindow);
    });

    $('.filter-link').each((i, link) =>
        $(link).on('click', () => {
            filterMarkers($(link).data('type'));
        })
    );

    renderTags();

    // enable autocomplete and search box
    (function () {
        const input = document.getElementById('map-search');
        searchBox = new google.maps.places.SearchBox(input);
        const options = {
            componentRestrictions: { country: 'rs' },
            fields: ['name', 'geometry'],
            strictBounds: false,
            types: ['street_address'],
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', () => {
            const place = autocomplete.getPlace();

            if (!place.geometry || !place.geometry.location) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent =
                place.formatted_address;
            infowindow.open(map, marker);
        });

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', () => {
            searchBox.setBounds(map.getBounds());
        });

        let markers = [];

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', () => {
            const places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach((marker) => {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();

            places.forEach((place) => {
                if (!place.geometry || !place.geometry.location) {
                    console.log('Returned place contains no geometry');
                    return;
                }

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    })();

    $('#map-search-select').on('change', function () {
        if ($(this).val() === 'lokacija') {
            $('.pac-container').css('visibility', 'visible');
        } else {
            $('.pac-container').css('visibility', 'hidden');
        }
    });

    if ($('#map-search-select').val() !== 'lokacija') {
        $('.pac-container').css('visibility', 'hidden');
    }

    const isLoggedIn = document.body.classList.contains('logged-in');
    if (isLoggedIn) {
        let marker;

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {
            if (marker == null) {
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                });
            } else {
                infoWindow.close();
                marker.setPosition(location);
            }

            const lat = marker
                .getPosition()
                .lat()
                .toString()
                .match(/\d{1,3}[,|.]\d{1,6}/);
            const lng = marker
                .getPosition()
                .lng()
                .toString()
                .match(/\d{1,3}[,|.]\d{1,6}/);

            const infowindowOpts = {
                content: `
                    <span>Latitude: ${lat}</span>
                    <br/>
                    <span>Longitude: ${lng}</span>`,
            };
            infoWindow = new google.maps.InfoWindow(infowindowOpts);

            google.maps.event.addListener(map, 'click', function () {
                infoWindow.close();
            });

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open(map, marker);
            });

            infoWindow.open(map, marker);
        }
    }
}

function downloadUrl(url, callback) {
    let request = window.ActiveXObject
        ? new ActiveXObject('Microsoft.XMLHTTP')
        : new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };

    request.open('GET', url, true);
    request.send(null);
}

function doNothing() {}

// Used to render markers data from database to map
function markersCallback(data, map, infoWindow) {
    let customLabel = {
        Blue: '0288D1',
        Yellow: 'FBC02D',
        Green: '7CB342',
        Red: 'E65100',
        'Ugostiteljski objekat': '1577-food-fork-knife_4x.png',
        'Hotel / prenoćište': '1602-hotel-bed_4x.png',
        'Tržni centar /šoping centar': '1684-shopping-bag_4x.png',
        'Garaža / parking': '1644-parking_4x.png',
        'Ski centar / gondola': '1688-ski-downhill_4x.png',
        Stadion: '1698-stadium-arena_4x.png',
        'Gerontološki centar': '1583-gated-community_4x.png',
        Džamija: '1673-religious-islamic_4x.png',
        'Stambena zgrada / kompleks': '1546-city-buildings_4x.png',
        'Poslovni objekat': '1547-city-buildings-pointed_4x.png',
        'Fabrika / proizvodni pogon': '1565-factory_4x.png',
        'Univerzitet  / institut': '1726-university_4x.png',
        Bolnica: '1807-hospital-h_4x.png',
        'Crkva / verski objekat': '1670-religious-christian_4x.png',
        'Škola / obrazovna ustanova': '1682-school-crossing_4x.png',
        'Klinički centar / klinika': '1624-medical_4x.png',
        'Distributivni centar / sedište firme': '1722-truck_4x.png',
        'Bazen / sportski kompleks': '1701-swimming_4x.png',
        'Auto-perionica': '1538-car_4x.png',
        'Prečistač otpadnih voda': '1703-tap-flowing_4x.png',
        'Benzinska pumpa': '1581-fuel-gasoline_4x.png',
        Park: '1886-tree-deciduous_4x.png',
        'Pravosudni objekat': '1552-courthouse-gavel_4x.png',
        Aerodrom: '1504-airport-plane_4x.png',
        'Autobuska stanica / autobuska radionica': '1532-bus_4x.png',
        'Jasle / obdanište': '1742-baby-nursery_4x.png',
        Pijaca: '1578-food-groceries_4x.png',
        Biblioteka: '1664-reading-library_4x.png',
        'Kulturna stanica / istorijska zgrada': '1598-historic-building_4x.png',
        'Trg / fontana': '1580-fountain_4x.png',
        'Železnička stanica / voz': '1717-train-steam_4x.png',
        Most: '1528-bridge_4x.png',
        Tunel: '1724-tunnel_4x.png',
        'Policijska stanica': '1655-police-badge_4x.png',
        Put: '1853-road-work-construction_4x.png',
    };
    let xml = data.responseXML;
    let markers = xml.documentElement.getElementsByTagName('marker');
    Array.prototype.forEach.call(markers, (markerElem) => {
        renderMarker(markerElem, map, infoWindow, customLabel);
    });

    allPaths.forEach((path) => {
        path.setMap(map);
    });
}

function renderMarker(markerElem, map, infoWindow, customLabel) {
    let id = markerElem.getAttribute('id');
    let name = markerElem.getAttribute('objekat');
    let type = markerElem.getAttribute('type');
    let buildingType = markerElem.getAttribute('type_of_building');
    let investitor = markerElem.getAttribute('investitor');
    let pocetakGradnje = markerElem.getAttribute('pocetak_gradnje');
    let zavrsetakGradnje = markerElem.getAttribute('zavrsetak_gradnje');
    let izvodjac = markerElem.getAttribute('izvodjac');
    let adresa = '';
    if (markerElem.getAttribute('broj') != '0')
        adresa =
            markerElem.getAttribute('grad') +
            ' ' +
            markerElem.getAttribute('ulica') +
            ' ' +
            markerElem.getAttribute('broj');
    else
        adresa =
            markerElem.getAttribute('grad') +
            ' ' +
            markerElem.getAttribute('ulica');
    let projektant = markerElem.getAttribute('projektant');
    let opis = markerElem.getAttribute('opis');
    let cena = markerElem.getAttribute('cena');
    let broj_stanova = markerElem.getAttribute('broj_stanova');
    let spratnost = markerElem.getAttribute('spratnost');
    let linkovi = markerElem.getAttribute('linkovi').split('\n');
    let slike = markerElem.getAttribute('slike').split('\n');
    let povrsinaObjekta = markerElem.getAttribute('povrsina_objekta');
    let tag = markerElem.getAttribute('tag');
    let putanje = markerElem.getAttribute('putanje');
    let point = new google.maps.LatLng(
        parseFloat(markerElem.getAttribute('lat')),
        parseFloat(markerElem.getAttribute('lon'))
    );
    let infowincontent = document.createElement('div');
    let strong = document.createElement('strong');
    strong.textContent = name;
    infowincontent.appendChild(strong);
    infowincontent.appendChild(document.createElement('br'));
    // 1546-city-buildings_4x.png
    //Highlight at the end represent the color of the marker which is extracted from customLabel object value which is obtained via type of marker from database
    const defaultIconURL =
        'https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-container_4x.png,icons/onion/' +
        customLabel[buildingType] +
        '&highlight=';
    let markerOptions = {
        map: map,
        position: point,
        icon: {
            url: defaultIconURL + customLabel[type],
        },
        id,
        name,
        type: type,
        buildingType: buildingType,
        buildingType,
        investitor,
        pocetakGradnje,
        zavrsetakGradnje,
        izvodjac,
        adresa,
        projektant,
        cena,
        broj_stanova,
        opis,
        spratnost,
        linkovi,
        slike,
        povrsinaObjekta,
        tag,
        putanje,
    };

    let marker = new google.maps.Marker(markerOptions);
    allMarkers.push(marker);
    marker.addListener('click', function () {
        infoWindow.setContent(infowincontent);
        infoWindow.open(map, marker);
        onMarkerClick(marker);
        const $mapInfo = $('.map__info');

        if ($mapInfo.hasClass('active') == false) {
            $mapInfo.animate({
                left: 0,
            });
            $mapInfo.toggleClass('active');
        }
    });

    if (roadPath) {
        roadPath.setMap(null);
    }

    const paths = marker.putanje.replace(/\\/g, '');

    if (paths) {
        const latLng = JSON.parse(paths);
        const lineColor = customLabel[marker.type];

        roadPath = new google.maps.Polyline({
            path: latLng[marker.id],
            geodesic: true,
            strokeColor: `#${lineColor}`,
            strokeOpacity: 1.0,
            strokeWeight: 6,
        });

        roadPath.building_id = marker.id;
        roadPath.addListener('click', function () {
            infoWindow.setContent(infowincontent);
            infoWindow.open(map, marker);
            onMarkerClick(marker);

            const $mapInfo = $('.map__info');
            if ($mapInfo.hasClass('active') == false) {
                $mapInfo.animate({
                    left: 0,
                });
                $mapInfo.toggleClass('active');
            }
        });

        allPaths.push(roadPath);
    }
}

function onMarkerClick(marker) {
    var data = {
        action: 'is_user_logged_in',
    };

    let $projectDetails;

    jQuery.post(ajaxurl, data, function (response) {
        if (response == 'yes') {
            $('#project-name').text(marker.name);
            $projectDetails = $('#project-details');
            $projectDetails.html('');
            let html = '';
            html +=
                '<p><strong>Tip zgrade:</strong> ' +
                marker.buildingType +
                '</p>';
            if (marker.spratnost && marker.spratnost !== '')
                html +=
                    '<p><strong>Spratnost:</strong> ' +
                    marker.spratnost +
                    '</p>';
            html += '<p><strong>Adresa:</strong> ' + marker.adresa + '</p>';
            if (marker.investitor && marker.investitor !== '')
                html +=
                    '<p><strong>Investitor:</strong> ' +
                    marker.investitor +
                    '</p>';
            if (marker.projektant && marker.projektant !== '')
                html +=
                    '<p><strong>Projektant:</strong> ' +
                    marker.projektant +
                    '</p>';
            if (marker.izvodjac && marker.izvodjac !== '')
                html +=
                    '<p><strong>Izvodjač:</strong> ' + marker.izvodjac + '</p>';
            if (marker.pocetakGradnje && marker.pocetakGradnje != '0000-0-0')
                html +=
                    '<p><strong>Početak radova:</strong> ' +
                    marker.pocetakGradnje +
                    '</p>';
            if (
                marker.zavrsetakGradnje &&
                marker.zavrsetakGradnje != '0000-0-0'
            )
                html +=
                    '<p><strong>Kraj radova:</strong> ' +
                    marker.zavrsetakGradnje +
                    '</p>';
            if (marker.povrsinaObjekta && marker.povrsinaObjekta !== '')
                html +=
                    '<p><strong>Površina u m2:</strong> ' +
                    marker.povrsinaObjekta +
                    '</p>';
            if (marker.broj_stanova && marker.broj_stanova !== '0')
                html +=
                    '<p><strong>Broj stanova:</strong> ' +
                    marker.broj_stanova +
                    '</p>';
            if (marker.cena && marker.cena !== '0')
                html +=
                    '<p><strong>Cena po m2:</strong> ' + marker.cena + '</p>';

            if (marker.opis && marker.opis !== '')
                html += '<p><strong>Opis:</strong> ' + marker.opis + '</p>';
            if (marker.linkovi && marker.linkovi[0] !== '') {
                let links = '';
                for (let i = 0; i < marker.linkovi.length; i++) {
                    if (marker.linkovi[i] === '') break;
                    links +=
                        "<br/><a class='project-links' href='" +
                        marker.linkovi[i] +
                        "'>Link do posta " +
                        (i + 1) +
                        '</a>';
                }
                html += '<p><strong>Linkovi:</strong> ' + links + '</p>';
            }
            $projectDetails.html(html);
        } else {
            $('#project-name').text(marker.name);
            $projectDetails = $('#project-details');
            $projectDetails.html('');
            let html = '';
            html += '<p><strong>Adresa:</strong> ' + marker.adresa + '</p>';
            html +=
                "<p><strong>Početak radova:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Kraj radova:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Površina u m2:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Investitor:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Projektant:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Izvođač:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Tip zgrade:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            html +=
                "<p><strong>Spratnost:</strong> Za više informacija <a href='/my-account'>registrujte se</a></p>";
            $projectDetails.html(html);
        }
        if (marker.slike) {
            let index = 0;

            const imageGallery =
                marker.slike[0] === ''
                    ? [
                          'https://www.gradnja.rs/wp-content/uploads/2021/01/gradnja-logo-bez-slogana-beli.png',
                      ]
                    : [...marker.slike];

            const createImage = (image, i) => {
                const url = image.replace('-150x150', '');
                const imageComponent = $(`
                <a class="image" href="${url}" data-lightbox="image-1" data-title="My caption">
					<img id="zgrada-${i}" src="${url}" alt="zgrada">
                 </a>
                `);

                return imageComponent;
            };

            const imageComponents = imageGallery
                .map((image, i) => {
                    return createImage(image, i);
                })
                .filter((image) => !!$(image).attr('href')) // filter out empty url values
                .map((image, i) => {
                    if (!i) {
                        $(image).addClass('active');
                    }

                    return image;
                });

            $('.project__gallery').children().remove(); // remove old marker images in order to add new selected marker images
            $('.project__gallery').append(...imageComponents);

            if (imageComponents.length > 1) {
                $('#direction-button').show();
                $('#back-button').show();
            } else {
                $('#direction-button').hide();
                $('#back-button').hide();
            }

            $('#back-button').on('click', function (e) {
                e.preventDefault();
                index--;

                const lastElement = imageComponents.length - 1;

                if (index < 0) {
                    index = lastElement;
                }

                imageComponents.forEach((element) =>
                    $(element).removeClass('active')
                );
                const activeElement = imageComponents[index];
                activeElement.addClass('active');
            });

            $('#direction-button').on('click', function (e) {
                e.preventDefault();
                index++;

                if (index > imageComponents.length - 1) {
                    index = 0;
                }

                imageComponents.forEach((element) =>
                    $(element).removeClass('active')
                );
                const activeElement = imageComponents[index];
                activeElement.addClass('active');
            });
        }
    });
}

export function filterFinishedAfterYearPassed(marker) {
    const { zavrsetakGradnje } = marker;
    const currentYear = new Date().getFullYear();

    if (zavrsetakGradnje === '0-0-0000') {
        return marker.setVisible(false);
    }
}

export function filterMarkersByYear(year) {
    if (tags.includes(year)) {
        tags = tags.filter((tag) => tag !== year);
    } else {
        tags = [...tags, year];
    }

    const markers = [...allMarkers];

    markers.forEach((marker) => {
        const markerTags = marker.tag.split(' ');
        const markerId = marker.id;

        if (!tags.length) {
            marker.setVisible(true);
            allPaths.forEach((path) => path.setVisible(true));
            return;
        }

        for (let tag of markerTags) {
            if (tags.includes(tag)) {
                marker.setVisible(true);

                break;
            }

            marker.setVisible(false);
        }
    });

    filterPaths(markers);
}

function filterPaths(markers) {
    allPaths.forEach((path) => {
        const visibleMarkers = markers
            .filter((marker) => marker.getVisible())
            .map((marker) => marker.id);
        const pathId = path.building_id;

        const markerVisible = visibleMarkers.includes(pathId);

        if (markerVisible) {
            path.setVisible(true);
            return;
        }

        path.setVisible(false);
    });
}

function filterMarkers(type) {
    const activeTags = $('.label-tag.active');

    activeTags.each((i, tag) => {
        $(tag).trigger('click');
    });

    allMarkers.forEach((marker) => {
        marker.setVisible(true);
        if (marker.type !== type && type !== 'All') {
            marker.setVisible(false);
        }
    });

    const markers = [...allMarkers];
    filterPaths(markers);
}

function filterMarkersByName(data) {
    let names = [];
    if (data !== 'No data') {
        data = JSON.parse(data);
        names = data.map((obj) => obj.objekat);
    }
    allMarkers.forEach((marker) => {
        marker.setVisible(true);
        if (!names.includes(marker.name)) marker.setVisible(false);
    });
}

function panToMarker(objekat) {
    let marker = {};
    allMarkers.forEach((mar) => {
        if (mar.name === objekat) {
            marker = mar;
            return;
        }
    });
    if (marker !== {}) {
        map.setZoom(20);
        let width =
            window.innerWidth ||
            document.documentElement.clientWidth ||
            document.body.clientWidth;
        if (width < 500)
            map.panTo(
                new google.maps.LatLng(
                    marker.getPosition().lat(),
                    marker.getPosition().lng() + 0.0001
                )
            );
        else map.panTo(marker.position);
    }
}

function MyLogoControl(controlDiv) {
    controlDiv.style.padding = '5px';
    var logo = document.createElement('IMG');
    logo.classList.add('gradnja-logo');
    logo.src = templateUrl + '/images/gradnja_logo.png';
    logo.style.cursor = 'pointer';
    controlDiv.appendChild(logo);

    google.maps.event.addDomListener(logo, 'click', function () {
        window.location = 'http://www.gradnja.rs';
    });
}

function CenterControl(controlDiv, map) {
    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.id = 'controlUI';
    controlUI.style.backgroundColor = '#2D7BA8';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginTop = '10px';
    controlUI.style.marginRight = '10px';
    controlUI.style.textAlign = 'center';
    controlUI.style.width = '40px';
    controlUI.style.color = 'white';

    controlUI.title = 'Click to search map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'white';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML =
        '<span class="iconify" data-icon="feather:search"></span>';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function () {
        const $mapSearch = $('.map__search');
        if ($mapSearch.hasClass('active')) {
            $mapSearch.animate({
                right: -400,
            });
            controlUI.style.marginRight = '10px';
        } else {
            $mapSearch.animate({
                right: 0,
            });
            controlUI.style.marginRight = '250px';
        }

        $mapSearch.toggleClass('active');
    });
}

function createTag(text) {
    return $(`
        <span class="label-tag" id="tag-${text}">
            <span class="label-tag__text">${text}</span>
        </span>
    `);
}

/**
 * Flattens the array by one depth.
 * @param { Array } - $arr - Array to flatten.
 */
function flatten(arr) {
    return arr.reduce((acc, cur) => {
        return acc.concat(cur);
    }, []);
}

/**
 * Renders unique tags on map UI.
 */
function renderTags() {
    $.ajax({
        method: 'get',
        url: '/wp-content/themes/gradnja/dbcalls/get-tags.php',
    })
        .done(function (data) {
            const tags = Array.from(JSON.parse(data)).map((objekat) => {
                if (objekat.tag.length > 1) {
                    return objekat.tag.split(' ');
                }

                return `${objekat.tag}`;
            });

            const flattenTags = flatten(tags);

            const uniqueTags = flattenTags
                .sort()
                .filter((tag) => tag !== '')
                .filter((value, index, self) => self.indexOf(value) === index)
                .map((tag) => createTag(tag))
                .map((tag) =>
                    tag.on('click', () => {
                        tag.toggleClass('active');
                        filterMarkersByYear(
                            tag.find('.label-tag__text').text()
                        );
                    })
                );

            $('.label-tag-wrapper').append(uniqueTags);
        })
        .fail(function (req, e) {
            alert(
                'Desila se greška pri povlačenju tagova. Molimo pokušajte ponovo'
            );
        })
        .always(function () {});
}

$(document).ready(function () {
    $('#exit-panel').on('click', function () {
        const $mapInfo = $('.map__info');

        if ($mapInfo.hasClass('active')) {
            $mapInfo.animate({
                left: -400,
            });
        } else {
            $mapInfo.animate({
                left: 0,
            });
        }

        $mapInfo.toggleClass('active');
    });

    $('#exit-panel2').on('click', function () {
        const $searchInfo = $('.map__search');
        if ($searchInfo.hasClass('active')) {
            $searchInfo.animate({
                right: -400,
            });
            $('#controlUI').css('marginRight', '10px');
        } else {
            $searchInfo.animate({
                right: 0,
            });
            $('#controlUI').css('marginRight', '250px');
        }

        $searchInfo.toggleClass('active');
    });

    $.ajax({
        method: 'get',
        url: '/wp-content/themes/gradnja/dbcalls/search.php',
        data: $('#map-search-form').serialize(),
    })
        .done(function (data) {
            $('.search__info').html(rawToHTML(data));
        })
        .fail(function (req, e) {
            alert(
                'Desila se greška pri pretraživanju. Molimo pokušajte ponovo'
            );
        })
        .always(function () {});
});

$(document).on('click', 'a.markerAnchor', function () {
    panToMarker($(this).html());
    const $searchInfo = $('.map__search');
    if ($searchInfo.hasClass('active')) {
        $searchInfo.animate({
            right: -400,
        });
        $('#controlUI').css('marginRight', '10px');
    } else {
        $searchInfo.animaite({
            right: 0,
        });
        $('#controlUI').css('marginRight', '250px');
    }

    $searchInfo.toggleClass('active');
});
var changeTimer = false;

$('#map-search').on('focus', function () {
    $('.label-tag.active').trigger('click');
});

$('#map-search').on('keydown', function (e) {
    if (e.keyCode === 10 || e.keyCode === 13) {
        e.preventDefault();
    }
    if (changeTimer !== false) clearTimeout(changeTimer);
    changeTimer = setTimeout(function () {
        /* your ajax here */
        $.ajax({
            method: 'get',
            url: '/wp-content/themes/gradnja/dbcalls/search.php',
            data: $('#map-search-form').serialize(),
        })
            .done(function (data) {
                $('.search__info').html(rawToHTML(data));
                filterMarkersByName(data);
            })
            .fail(function (req, e) {
                alert(
                    'Desila se greška pri pretraživanju. Molimo pokušajte ponovo'
                );
            })
            .always(function () {});
        changeTimer = false;
    }, 300);
});

$('#map-search-select').on('change', function (e) {
    if (changeTimer !== false) clearTimeout(changeTimer);
    changeTimer = setTimeout(function () {
        /* your ajax here */
        $.ajax({
            method: 'get',
            url: '/wp-content/themes/gradnja/dbcalls/search.php',
            data: $('#map-search-form').serialize(),
        })
            .done(function (data) {
                $('.search__info').html(rawToHTML(data));
                filterMarkersByName(data);
            })
            .fail(function (req, e) {
                alert(
                    'Desila se greška pri pretraživanju. Molimo pokušajte ponovo'
                );
            })
            .always(function () {});
        changeTimer = false;
    }, 300);
});

//TO DO: Napraviti da ova funckija radi mnogo bolje kada bude u mapi
function rawToHTML(data) {
    let html = '';
    if (data !== 'No data') {
        data = JSON.parse(data);
        for (let i = 0; i < data.length; i++) {
            html +=
                "<h1><a class='markerAnchor' src='#'>" +
                data[i].objekat +
                '</a></h1>';
            if (data[i].broj != '0')
                html +=
                    '<p>' +
                    data[i].grad +
                    ' ' +
                    data[i].ulica +
                    ' ' +
                    data[i].broj +
                    '</p>';
            else html += '<p>' + data[i].grad + ' ' + data[i].ulica + '</p>';
        }
    } else html = 'Nema rezultata';
    return html;
}

$(function () {
    initMap();
});

window.addEventListener('load', function () {
    setTimeout(() => {
        $('#tag-2022').trigger('click');
    }, 500);
});
