(function ($) {
    /* ========= ANIMATION FORM PLACEHOLDER ========= */
    $('div.wpcf7 input').focusin(function () {
        console.log('focusin');
        $(this).parent().next().css('bottom', '30px');
        $(this).parent().next().css('font-size', '12px');
    });

    $('div.wpcf7 span input').focusout(function () {
        if ($(this).val().length === 0) {
            $(this).parent().next().css('bottom', '0px');
            $(this).parent().next().css('font-size', '15px');
        } else {
            $(this).parent().next().css('bottom', '30px');
            $(this).parent().next().css('font-size', '12px');
        }
    });
})(jQuery);
