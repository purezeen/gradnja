<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

/**
 * Global variables.
 */
require get_template_directory() . '/inc/global.php';

/**
 * Helpers functions
 */
require get_template_directory() . '/inc/helper.php';

/**
 * Theme support.
 */
require get_template_directory() . '/inc/support.php';

/**
 * Script and styles enqueues
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Theme widgets.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * ACF Settings.
 */
require get_template_directory() . '/inc/acf-settings.php';

/**
 * Custom post types.
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Ajax filter function
 */
//require get_template_directory() . '/inc/ajax.php';

//* ======== EXCERPT ======== *//
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

/**
 * Estimates the reading time for a given piece of $content.
 *
 * @param string $content Content to calculate read time for.
 * @param int $wpm Estimated words per minute of reader.
 *
 * @returns int $time Esimated reading time.
 */
function prefix_estimated_reading_time( $content = '', $wpm = 300 ) {
	$clean_content = strip_shortcodes( $content );
	$clean_content = strip_tags( $clean_content );
	$word_count    = str_word_count( $clean_content );
	$time          = ceil( $word_count / $wpm );
	return $time;
}

/**
 * Show how many coments is in specific post.
 */
function wpb_comment_count() {
	$comments_count = wp_count_comments();
	$message        = 'There are <strong>' . $comments_count->approved . '</strong> comments posted by our users.';

	return $message;
}

add_shortcode( 'wpb_total_comments', 'wpb_comment_count' );

add_action( 'init', 'gradnja_pagination' ); // Add our HTML5 Pagination
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function gradnja_pagination() {
	if ( get_option( 'permalink_structure' ) ) {
		$format = 'page/%#%/';
	} else {
		$format = '&paged=%#%';
	}
	global $wp_query;
	$big = 999999999;
	echo paginate_links(
		array(
			'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'    => $format,
			'current'   => max( 1, get_query_var( 'paged' ) ),
			'total'     => $wp_query->max_num_pages,
			'mid_size'  => 2,
			'type'      => 'list',
			'prev_text' => esc_html__( 'Prethodno', 'gulp_wordpress' ),
			'next_text' => esc_html__( 'Sledeće', 'gulp_wordpress' ),
		)
	);
}


function my_pre_get_posts( $query ) {

	// do not modify queries in the admin
	if ( is_admin() ) {

		return $query;

	}

	// only modify queries for 'event' post type
	if ( isset( $query->query_vars['post_type'] ) && $query->query_vars['post_type'] == 'product' ) {

		// allow the url to alter the query
		if ( isset( $_GET['category'] ) ) {

			$query->set( 'cat', $_GET['category'] );

		}
	}

	// return
	return $query;

}

add_action( 'pre_get_posts', 'my_pre_get_posts' );


/**
 * Breadcrumb
 */

function get_breadcrumb() {
	echo 'Home';
	if ( is_category() || is_single() ) {

		/* The stuff below happens when it’s a category or a post*/

		echo ' &nbsp; / &nbsp;';
		echo '<strong>';
		the_category( ' • ' );
		echo '</strong>';
		if ( is_single() ) {
			echo ' &nbsp; / &nbsp;';
			echo '<strong>';
			the_title();
			echo '</strong>';
		}
	} elseif ( is_page() ) {

		/* The stuff below happens when it’s a page */

		echo ' &nbsp; / &nbsp; ';
		echo '<strong>';
		echo the_title();
		echo '</strong>';
	} elseif ( is_search() ) {

		/* The stuff below happens when it’s a search */

		echo '  /  Rezultati tražene reči… ';
		echo '<strong>';
		echo '"';
		echo the_search_query();
		echo '"';
		echo '</strong>';
	}
}

/**
 * Widget API: Gradnja_Commets class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Core class used to implement a Recent Comments widget.
 *
 * @since 2.8.0
 *
 * @see WP_Widget
 */
class Gradnja_Commets extends WP_Widget {

	/**
	 * Sets up a new Recent Comments widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'widget_recent_comments',
			'description'                 => __( 'Your site&#8217;s most recent comments.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'recent-comments', __( 'Najnoviji komentari' ), $widget_ops );
		$this->alt_option_name = 'widget_recent_comments';

		if ( is_active_widget( false, false, $this->id_base ) || is_customize_preview() ) {
			add_action( 'wp_head', array( $this, 'recent_comments_style' ) );
		}
	}

	/**
	 * Outputs the default styles for the Recent Comments widget.
	 *
	 * @since 2.8.0
	 */
	public function recent_comments_style() {
		/**
		 * Filters the Recent Comments default widget styles.
		 *
		 * @since 3.1.0
		 *
		 * @param bool   $active  Whether the widget is active. Default true.
		 * @param string $id_base The widget ID.
		 */
		if ( ! current_theme_supports( 'widgets' ) // Temp hack #14876
			|| ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) ) {
			return;
		}
		?>
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<?php
	}

	/**
	 * Outputs the content for the current Recent Comments widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Recent Comments widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$output = '';

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number ) {
			$number = 5;
		}

		/**
		 * Filters the arguments for the Recent Comments widget.
		 *
		 * @since 3.4.0
		 * @since 4.9.0 Added the `$instance` parameter.
		 *
		 * @see WP_Comment_Query::query() for information on accepted arguments.
		 *
		 * @param array $comment_args An array of arguments used to retrieve the recent comments.
		 * @param array $instance     Array of settings for the current widget.
		 */
		$comments = get_comments(
			apply_filters(
				'widget_comments_args',
				array(
					'number'      => $number,
					'status'      => 'approve',
					'post_status' => 'publish',
				),
				$instance
			)
		);

		$output .= $args['before_widget'];
		$output .= '<div class="widget-posts sidebar-block">';
		if ( $title ) {
			$output .= '<h3 class="title-line title-sidebar">' . $title . '<h3>';
		}

		$output .= '<ul id="recentcomments" class="unstyle-list">';
		$output .= '<li class="recentcomments">';
		if ( is_array( $comments ) && $comments ) {
			// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
			$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
			_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

			foreach ( (array) $comments as $comment ) {
				$output .= '<span href="#" class="widget-post clearfix">';
				/* translators: comments widget: 1: comment author, 2: post link */
				$output .= sprintf(
					_x( '%1$s %2$s', 'widgets' ),
					'<div>',
					'<div class="meta-data align-center"><span class="devider align-center"><span class="iconify" data-icon="fa-comment-o" data-inline="false"></span>' . get_comment_author( $comment ) . '</span><span>' . get_comment_date( 'd.m.Y.', $comment->comment_ID ) . '</span></div><a href="' . esc_url( get_comment_link( $comment ) ) . '"><h6 class="widget-post-title">' . get_the_title( $comment->comment_post_ID ) . '</h6></a>'
				);
				$output .= '</div></span>';
			}
		}
		$output .= '</li>';
		$output .= '</ul>';
		$output .= '</div>';
		$output .= $args['after_widget'];
		echo $output;
	}

	/**
	 * Handles updating settings for the current Recent Comments widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );
		return $instance;
	}

	/**
	 * Outputs the settings form for the Recent Comments widget.
	 *
	 * @since 2.8.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? $instance['title'] : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of comments to show:' ); ?></label>
		<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>
		<?php
	}

	/**
	 * Flushes the Recent Comments widget cache.
	 *
	 * @since 2.8.0
	 *
	 * @deprecated 4.4.0 Fragment caching was removed in favor of split queries.
	 */
	public function flush_widget_cache() {
		_deprecated_function( __METHOD__, '4.4.0' );
	}
}
add_action( 'widgets_init', 'register_my_widget' );
function register_my_widget() {
	 register_widget( 'Gradnja_Commets' );
}

function isMobileDevice() {
	return preg_match( '/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i', $_SERVER['HTTP_USER_AGENT'] );
}


	//Exclude pages from WordPress Search
if ( ! is_admin() ) {
	function wpb_search_filter( $query ) {
		if ( $query->is_search ) {
			$query->set( 'post_type', 'post' );
		}
		return $query;
	}
	add_filter( 'pre_get_posts', 'wpb_search_filter' );
}



/**
 * Post Gallery Layout
 */
add_filter( 'post_gallery', 'gillion_post_gallery', 10, 2 );
function gillion_post_gallery( $string, $attr ) {

	$columns = ( isset( $attr['columns'] ) && $attr['columns'] >= 1 && $attr['columns'] <= 4 ) ? $attr['columns'] : '3';
	// $size = $attr['size'];
	$size       = 'large';
	$gallery_id = 'gallery-' . gillion_rand( 6 );

	$data_justify = '';

	$output = '<div class="post-content-gallery masonry-gallery gallery-columns-' . esc_attr( $columns ) . '">';
	$output .= '<div class="grid-sizer"></div>';
	$posts  = get_posts(
		array(
			'include'   => $attr['ids'],
			'post_type' => 'attachment',
			'orderby'   => $attr['orderby'],
		)
	);

	foreach ( $posts as $post ) :
		$post_link = '';
		if ( isset( $post->ID ) && is_array( wp_get_attachment_image_src( $post->ID, 'full' ) ) ) :
			$post_link_var = wp_get_attachment_image_src( $post->ID, 'full' );
			if ( isset( $post_link_var[0] ) ) :
				$post_link = $post_link_var[0];
				endif;
			endif;

		$post_image = '';
		if ( isset( $post->ID ) && is_array( wp_get_attachment_image_src( $post->ID, $size ) ) ) :
			$post_image_var = wp_get_attachment_image_src( $post->ID, $size );
			if ( isset( $post_image_var[0] ) ) :
				$post_image = $post_image_var[0];
				endif;
	
	              if ( isset( $post_image_var[1] ) ) :
					$post_image_width = $post_image_var[1];
					endif;
					if ( isset( $post_image_var[2] ) ) :
						$post_image_height = $post_image_var[2];
						endif;
	
	
	
			endif;
		// $output .= '
        //     <figure class="post-content-gallery-item">
        //         <a href="' . esc_url( $post_link ) . '" data-rel="lightcase:' . $gallery_id . '">
        //             <img src="' . esc_url( $post_image ) . '" alt="' . $post->post_excerpt . '" />
        //             <div class="post-content-gallery-item-caption">' . $post->post_excerpt . '</div>
        //         </a>
        //     </figure>';

		$output .= '
                <figure class="post-content-gallery-item">
                    <a href="'.esc_url( $post_link ).'" data-rel="lightcase:'.$gallery_id.'">
                        <img src="'.esc_url( $post_image ).'" alt="'.$post->post_excerpt.'" width="'.$post_image_width.'" height="'.$post_image_height.'"/>';

			$caption=$post->post_excerpt;
			if($caption):
				$output .= '<div class="post-content-gallery-item-caption">'.$post->post_excerpt.'</div>';
			endif;

		    $output .= '</a>
                      </figure>';
		endforeach;

	$output .= '</div>';
	return $output;
}


/**
 * Create Custom Random Function
 */
function gillion_rand( $length = 10 ) {
	return substr( str_shuffle( str_repeat( $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil( $length / strlen( $x ) ) ) ), 1, $length );
}

add_filter( 'pre_get_posts', 'query_post_type' );
function query_post_type( $query ) {
	if ( is_category() && $query->is_main_query() ) {
		$post_type = get_query_var( 'post_type' );
		if ( $post_type ) {
			$post_type = $post_type;
		} else {
			$post_type = array( 'nav_menu_item', 'post', 'uradi-sam' ); // don't forget nav_menu_item to allow menus to work!
		}
		$query->set( 'post_type', $post_type );
		return $query;
	}
}

/**
 * Remove the slug from published post permalinks. Only affect our CPT though.
 */
function wpex_remove_cpt_slug( $post_link, $post, $leavename ) {
	if ( 'uradi-sam' != $post->post_type || 'publish' != $post->post_status ) {
		return $post_link;
	}
	$post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
	return $post_link;
}
add_filter( 'post_type_link', 'wpex_remove_cpt_slug', 10, 3 );


function ajax_check_user_logged_in() {
	echo is_user_logged_in() ? 'yes' : 'no';
	die();
}
add_action( 'wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in' );
add_action( 'wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in' );

add_action( 'wp_head', 'wp97797_define_ajaxurl' );

function wp97797_define_ajaxurl() {
	?>
<script>
	ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
</script>
	<?php
}

add_role(
	'placeni_korisnik',
	'Plaćeni korisnik',
	array(
		'read' => true,  // true allows this capability
	)
);

	add_action( 'admin_bar_menu', 'add_toolbar_items', 100 );
function add_toolbar_items( $admin_bar ) {
	$admin_bar->add_menu(
		array(
			'id'    => 'stranice-gradilista',
			'title' => 'Upravljanje gradilištima',
			'href'  => '#',
			'meta'  => array(
				'title' => __( 'Upravljanje gradilištima' ),
			),
		)
	);
	$admin_bar->add_menu(
		array(
			'id'     => 'dodaj',
			'parent' => 'stranice-gradilista',
			'title'  => 'Dodaj Gradilište',
			'href'   => '/dodaj-gradiliste',
			'meta'   => array(
				'title'  => __( 'Dodaj Gradilište' ),
				'target' => '',
				'class'  => 'my_menu_item_class',
			),
		)
	);
	$admin_bar->add_menu(
		array(
			'id'     => 'report',
			'parent' => 'stranice-gradilista',
			'title'  => 'Napravi Report',
			'href'   => '/napravi-report',
			'meta'   => array(
				'title'  => __( 'Napravi Report' ),
				'target' => '',
				'class'  => 'my_menu_item_class',
			),
		)
	);
	$admin_bar->add_menu(
		array(
			'id'     => 'update',
			'parent' => 'stranice-gradilista',
			'title'  => 'Izmeni gradilište',
			'href'   => '/izmeni-gradiliste',
			'meta'   => array(
				'title'  => __( 'Izmeni gradilište' ),
				'target' => '',
				'class'  => 'my_menu_item_class',
			),
		)
	);
}

	// show admin bar only for admins
if ( ! current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}
	// show admin bar only for admins and editors
if ( ! current_user_can( 'edit_posts' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}


// Filter search for promo post widget
add_filter( 'acf/fields/post_object/query/key=field_5d9b1ab64644f', 'my_acf_fields_post_object_query', 10, 3 );
function my_acf_fields_post_object_query( $args, $field, $post_id ) {

	// Show 40 posts per AJAX call.
	$args['posts_per_page'] = 40;

	$args['orderby'] = 'relevance';
	$args['order']   = 'ASC';

	return $args;
}

// Better search filter for Add Media functionality in Posts
// If search key exist, use different filter args
add_filter( 'ajax_query_attachments_args', 'better_filter_attachments', 10, 1 );

function better_filter_attachments( $query = array() ) {
	if ( isset( $query['s'] ) ) {
		$query['orderby'] = 'relevance';
		$query['order']   = 'ASC';
	}
	return $query;
}

// deactivate new block editor
function phi_theme_support() {
	remove_theme_support( 'widgets-block-editor' );
}
add_action( 'after_setup_theme', 'phi_theme_support' );


// ACF relationship field search functionality fixed
function acf_post_object_custom_query( $args, $field, $post_id ) 
{
    $args['orderby'] = null;
    return $args;
}

add_filter( 'acf/fields/relationship/query', 'acf_post_object_custom_query', 10, 3 );






