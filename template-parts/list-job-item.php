<?php $expired = false;
if(get_field('job_expires')){ 
    if(is_job_expired('job_expires')){ 
        $expired=true;
 }}?>


<article id="post-<?php the_ID(); ?>" <?php post_class(array('job-item')); ?>>
    <div class="job-item__wrap job-item__flex">

        <div class="card-image__wrap">
            <!-- card img job -->
            <div class="card-image job-item__img <?php echo ($expired)?'card-image__overlay':''; ?>">
                <!-- post thumbnail -->
                <?php if ( has_post_thumbnail() ) : // Check if thumbnail exists. ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php the_post_thumbnail(); ?>
                </a>
                <?php endif; ?>
                <!-- /post thumbnail -->
                <a href="<?php the_permalink(); ?>" class="card-overlay">
                    <div class="card-overlay-content">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
            </div>
            <!-- end card-image -->
        </div>

        <div class="card-content job-item__content">
            <span class="post-categories"><?php the_category( ', ' ); ?></span>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <h2 class="card-title"><?php the_title(); ?></h2>
            </a>
            <p class="card-desc">
                <?php //the_excerpt();
                       $excerpt = get_the_excerpt(); 
                       $excerpt = substr( $excerpt, 0, 130 ); // Only display first 260 characters of excerpt
                       $result = substr( $excerpt, 0, strrpos( $excerpt, ' ' ) );
                       $result .= '...';
                       echo $result;
               ?>
            </p>
            <div class="meta-data">
                <?php if(get_field('job_location')){ ?>
                <div class="align-center devider"><span class="iconify" data-icon="bytesize:location"></span>
                    <?php //echo get_the_date(); ?> <?php the_field('job_location'); ?>
                </div>
                <?php } ?>
                <?php if(get_field('job_expires')){ 
                        if(is_job_expired('job_expires')){ ?>


                <span class="job-item__expired align-center"><span class="iconify" data-icon="bytesize:clock"
                        data-inline="false"></span>
                    <span><?php echo 'Istekao'; ?></span>
                </span>

                <?php  }else{  ?>

                <span class="align-center"><span class="iconify" data-icon="bytesize:clock" data-inline="false"></span>
                    <?php the_field('job_expires');  ?>
                </span>

                <?php } ?>

                <?php } ?>

            </div>
            <!-- end meta-data -->
        </div>
        <!-- end card-content -->

    </div>
    <!-- end card-container -->
    <span><?php edit_post_link() ?></span>
</article>