<?php get_header(); ?>

<main class="container">
    <div class="row clearfix">
        <div class="col-12 col-lg-9 clearfix main-content">

            <?php
            $featured_jobs = get_field('featured_jobs', 'option');
            if( $featured_jobs ): ?>
            <div class="featured-jobs">

                <?php if( get_field('featured_jobs_title', 'option')): ?>
                <h1 class="featured-jobs__title"><?php the_field('featured_jobs_title', 'option'); ?></h1>
                <?php endif; ?>

                <div class="row">
                    <?php foreach( $featured_jobs as $post ): 
                        // Setup this post for WP functions (variable must be named $post).
                        setup_postdata($post); ?>
                    <div class="col-sm-4">
                        <div class="featured-job">

                            <!-- article -->
                            <article id="post-<?php the_ID(); ?>" <?php post_class(array('')); ?>>

                                <?php $expired = false;
                                if(get_field('job_expires')){ 
                                    if(is_job_expired('job_expires')){ 
                                        $expired=true;
                                 }}?>  
                                <div class="job-item__wrap">

                                    <div class="card-image job-item__img <?php if (has_category('video')) { echo 'video-icon';} ?> <?php echo ($expired)?'card-image__overlay':''; ?>">

                                        <?php 
                                           if (has_category('video')) : ?>
                                        <a class="video-icon-wrap">
                                            <span class="icon-title">
                                                <span>Play</span>
                                            </span>
                                            <img class="video-icon-img"
                                                src="<?php echo get_template_directory_uri(); ?>/images/play.svg"
                                                alt="play">
                                        </a>
                                        <?php endif; ?>

                                        <!-- post thumbnail -->
                                        <?php if ( has_post_thumbnail() ) : // Check if thumbnail exists. ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <?php the_post_thumbnail(); ?>
                                        </a>
                                        <?php endif; ?>
                                        <!-- /post thumbnail -->
                                        <a href="<?php the_permalink(); ?>" class="card-overlay">
                                            <div class="card-overlay-content">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- end card-image -->

                                    <div class="card-content">
                                        
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <h2 class="card-title"><?php the_title(); ?></h2>
                                        </a>
                                       
                                        <div class="meta-data">
                                            <?php if(get_field('job_location')){ ?>
                                            <div class="align-center devider"><span class="iconify"
                                                    data-icon="bytesize:location"></span>
                                                <?php the_field('job_location'); ?>
                                            </div>
                                            <?php } ?>

                                            <?php if(get_field('job_expires')){ 
                                                if(is_job_expired('job_expires')){ ?>


                                            <span class="job-item__expired align-center"><span class="iconify"
                                                    data-icon="bytesize:clock" data-inline="false"></span>
                                                <span><?php echo 'Istekao'; ?></span>
                                            </span>

                                            <?php  }else{  ?>

                                            <span class="align-center"><span class="iconify" data-icon="bytesize:clock"
                                                    data-inline="false"></span>
                                                <?php the_field('job_expires');  ?>
                                            </span>

                                            <?php } ?>

                                            <?php } ?>

                                        </div>
                                        <!-- end meta-data -->
                                    </div>
                                    <!-- end card-content -->

                                </div>
                                <!-- end card-container -->
                                <span><?php edit_post_link() ?></span>
                            </article>
                            <!-- end article -->
                        </div>
                    </div>

                    <?php endforeach; ?>
                </div>

            </div>
            <?php // Reset the global post object so that the rest of the page works correctly.
            wp_reset_postdata(); ?>
            <?php endif; ?>



            <section class="jobs">
                <!-- <div class="jobs__header align-center justify-content-between"> -->
                <div class="jobs__header">
                    <h2 class="jobs__title">
                        <?php $term = get_queried_object();
                              $alljobs_title = get_field('all_jobs_title', $term);
                               echo  $alljobs_title ?>
                    </h2>
                </div>

                <div class="response-wrap">
                    <!-- loader for filter -->
                    <!-- <div class="loading">
                        <i class="loading__icon"></i>
                    </div> -->

                    <!-- here we will insert our filter results -->
                    <div id="response">
                        <?php get_template_part( 'loop', 'posao' ); ?>
                    </div>

                    <?php get_template_part( 'pagination' ); ?>
                </div>

            </section>
            <!-- end grid -->


        </div>
        <!-- end main content-->

        <div class="col-12 col-lg-3 sidebar clearfix ">
            <div class="theiaStickySidebar">
                <?php get_sidebar();?>
            </div>
        </div>
        <!--  end sidebar  -->

    </div>
    <!-- end row  -->
</main>
<!-- end container -->



<?php get_footer(); ?>