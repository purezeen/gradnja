<?php 
/**
 *  Ajax filter function
 * 
 */


add_action('wp_ajax_myfilter', 'filter_ajax');
add_action('wp_ajax_nopriv_myfilter', 'filter_ajax');

function filter_ajax(){

	header("Content-Type: application/json"); 
	
	// create meta_query array for acf fields
	$meta_query = array('relation' => 'AND');  // AND means that all conditions of meta_query should be true

	// location filter
	// ***********************************
    if(!empty($_POST['location'])){
        $location = sanitize_text_field( $_POST['location'] );
        $meta_query[] = array(
            'key' => 'job_location',
            'value' => $location,
            'compare' => '='
        );
    }

	// expired jobs
	// *************************************
	// if( isset( $_POST['expired'] ) && $_POST['expired'] == 'on' ){
	// if( isset( $_POST['expired'] )){
	if(!empty($_POST['sorting']) && $_POST['sorting'] == 'expired'){
			$meta_query[] = array(
				'key' => 'job_expires',
				'value' => date("Y-m-d"),
				'compare' => '<',
				'type'    => 'DATE'
			);	
	}

	// expire soon
	// *************************************
	// if(isset($_POST['expire_soon'])){
	if(!empty($_POST['sorting']) && $_POST['sorting']  == 'expire_soon'){
		$meta_query[] = array(
			'key' => 'job_expires',
			'value' => date("Y-m-d"),
			'compare' => '>=',
			'type'    => 'DATE'
		);
					
	}

	// create tax_query array for taxonomy fields
    $tax_query = array('relation' => 'AND'); 

	// category filter
	// ********************************************
	if(!empty($_POST['category'])){
        $category = sanitize_text_field( $_POST['category'] );
		$default_cat_id=get_category_id('posao');
		$picked_cat_id=get_category_id($category); 
        $tax_query[] = array(			
			'relation'		=> 'AND',
            array(
                'taxonomy' => 'category',
				'terms' => 	$default_cat_id,
				'include_children' => false
            ),
            array(
                'taxonomy' => 'category',
				'terms' => 	$picked_cat_id,
				'include_children' => false
            )
        );		
    }else{		

		$tax_query[] = array(
            'taxonomy' => 'category',
            'terms' => get_category_id('posao')
		
        );
	}

	// default args for filter
	// *************************************
	$args = array(
        'post_type' => 'post',
		'post_status' => 'publish' ,
		'orderby' => 'date',
		'order'   => 'DESC',
        'meta_query' => $meta_query, 
        'tax_query' => $tax_query,   
        'posts_per_page' => 10,    
		'paged' => 1             
    );

	// expire soon
	// *************************************
	if(!empty($_POST['sorting']) && $_POST['sorting']  == 'expire_soon'){
		$args['orderby']='meta_value_num';
		$args['meta_key']='job_expires';
		$args['order']='ASC';			
	}

	// expired jobs
	// *************************************
	if(!empty($_POST['sorting']) && $_POST['sorting'] == 'expired'){
		$args['orderby']='meta_value_num';
		$args['meta_key']='job_expires';
		$args['order']='DESC';	
	}

	// set the default current page
	// *************************************
    $current_page = 1;

	// page
	// *************************************
    if(!empty($_POST['page'])){   
     	$current_page = $_POST['page'];
	    $args['paged'] = $current_page;  
    } 

	$query = new WP_Query( $args );

	// max number of pages, for pagination
	$max_pages = $query->max_num_pages; 

	// created an empty array where we put all filterd posts
	$posts=array(); 

	if( $query->have_posts() ) :
	
		while( $query->have_posts() ): $query->the_post();
	
		     $posts[] = load_template_part('template-parts/list-job', 'item');

		endwhile; 

		wp_reset_postdata();
		
	endif;

	$results = [ 'posts' => $posts, 'current_page' => intval($current_page) , 'pages' => $max_pages ];  

    echo json_encode( $results ); 
	
	die();
}


// output template buffering (very bad solution and error sensitive)
function load_template_part($template_name, $part_name=null) {

    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;

}