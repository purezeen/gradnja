<?php
/**
 * ACF settings
 */
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page(
		array(
			'page_title' => 'Options',
			'menu_title' => 'Options',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title'  => 'Banners',
			'menu_title'  => 'Baneri',
			'parent_slug' => 'theme-general-settings',
		)
	);

	acf_add_options_sub_page(
		array(
			'page_title'  => 'Snippet',
			'menu_title'  => 'Snippet',
			'parent_slug' => 'theme-general-settings',
		)
	);
}

add_action(
	'after_setup_theme',
	function() {
		add_theme_support( 'responsive-embeds' );
	}
);
