<?php
/**
 * Gradnja theme useful functions.
 *
 * @package gradnja
 * @since 0.0.1
 */

/**
 * Checks if file has been changed.
 *
 * @param string $file_path - file path relative to theme dir.
 * @return int
 */
function get_file_version( $file_path ) {
	return filemtime( get_stylesheet_directory() . $file_path );
}

/**
 * Convert a multi-dimensional array into a single-dimensional array.
 *
 * @author Sean Cannon, LitmusBox.com | seanc@litmusbox.com
 * @param  array $array The multi-dimensional array.
 * @return array
 */
function array_flatten( $array ) {
	if ( ! is_array( $array ) ) {
		return false;
	}
	$result = array();
	foreach ( $array as $key => $value ) {
		if ( is_array( $value ) ) {
			$result = array_merge( $result, array_flatten( $value ) );
		} else {
			$result = array_merge( $result, array( $key => $value ) );
		}
	}
	return $result;
}

/**
 * Convert obj to an array
 *
 * @param  object $obj object to convert.
 * @return array
 */
function obj_to_arr( $obj ) {
	return json_decode( wp_json_encode( $obj ), true );
}


/**
 * Simple Templating function
 *
 * checking if the job has expired
 * $date_field_name is Acf date field name 
 */
function is_job_expired($date_field_name) {
	// supply 3rd argument of false to get unformatted date
	$data_expires = get_field($date_field_name, false, false);
	// use the same date format for the current date
	$currentDateTime = date('Ymd');

	if($data_expires < $currentDateTime) {
		return true;
	}else{
		return false;
	}
}

/**
 * Simple Templating function
 *
 *  console log php code in console - for debuging
 */
function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
}

/**
 * Simple Templating function
 *
 *  get category id by category name
 */
function get_category_id($cat_name){
	$term = get_term_by('name', $cat_name, 'category');
	return $term->term_id;
}