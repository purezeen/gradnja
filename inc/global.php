<?php
/**
 * Global variables.
 */
define( 'THEME_VERSION', wp_get_theme()->Version );

function gradnja_db_shortcut() {
	global $wpdb;

	$wpdb->map_markers = $wpdb->prefix . 'maps_markers';
}
add_action( 'init', 'gradnja_db_shortcut', 0 );
