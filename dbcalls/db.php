<?php
// @phpcs:ignoreFile

$env = 'gradnja.local' === $_SERVER['HTTP_HOST'] ? 'development' : 'production';

if ( 'development' === $env ) {
	// Development creds
	$db       = 'db';
	$username = 'wordpress';
	$password = 'wordpress';
	$database = 'gradnja_wp';
} else {
	// Production creds
	$username = 'gradnja_wp';
	$password = 'J1b6H7d4';
	$database = 'gradnja_wp';
	$db       = 'localhost';
}
