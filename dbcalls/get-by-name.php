<?php
//phpcs:ignoreFile
require 'db.php';

$text_parameter = $_GET['changeSelect'];
$connection     = mysqli_connect( $db, $username, $password );
if ( ! $connection ) {
	die( 'Not connected : ' . mysqli_error( $connection ) );
}

$db_selected = mysqli_select_db( $connection, $database );
if ( ! $db_selected ) {
	die( 'Can\'t use db : ' . mysqli_error( $connection ) );
}
if ( $text_parameter !== '' ) {

	$query = "SELECT * FROM wp_maps_markers WHERE objekat = '" . $text_parameter . "'";

	$get_data = mysqli_query( $connection, $query );
	if ( ! $get_data ) {
		die( 'Couldnt get ' . mysqli_error( $connection ) );
	}
	if ( $get_data->num_rows === 0 ) {
		echo 'No data';
		return;
	}
	while ( $row = $get_data->fetch_array( MYSQLI_ASSOC ) ) {
		$myArray[] = $row;
	}
	echo json_encode( $myArray, JSON_NUMERIC_CHECK );
	return;
}

echo 'No data';
mysqli_close( $connection );
