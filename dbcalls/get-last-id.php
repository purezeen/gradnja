<?php
require 'db.php';
require '../../../../wp-load.php';


global $wpdb;

$last_id_query   = "SELECT id FROM $wpdb->map_markers ORDER BY id DESC LIMIT 1";

// Increment ID in order to get key for next storing 
$last_id_result   = obj_to_arr( $wpdb->get_results( $last_id_query ) )[0]['id'] + 1; //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared

if($last_id_result) {
    echo $last_id_result;
    return;
}

echo 'Could not retrieve the last id.';