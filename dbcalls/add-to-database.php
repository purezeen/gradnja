<?php
require 'db.php';
require_once '../../../../wp-load.php';

global $wpdb;

$nonce     = wp_verify_nonce( $_POST['gradnja_nonce'], 'dodaj-gradiliste' );
$obj_type  = $_POST['type'];
$pocetak   = $_POST['pocetak_gradnje'];
$zavrsetak = $_POST['zavrsetak_gradnje'];
$obj_data  = $_POST;

// Unset nonce and tags from db.
// Tags are only used as a setting mechanism in the frontend and,
// nonce is not needed in the database.
// Putanja is checkbox not needed in db.
unset( $obj_data['gradnja_nonce'] );
unset( $obj_data['tags'] );
unset( $obj_data['putanja'] );
unset( $obj_data['id'] );
unset( $obj_data['last_id'] );

if ( 'U izgradnji' === $obj_type ) {
	$obj_type = 'Blue';
} elseif ( 'U planu' === $obj_type ) {
	$obj_type = 'Yellow';
} elseif ( 'Obustavljeno' === $obj_type ) {
	$obj_type = 'Red';
} else {
	$obj_type = 'Green';
}

if ( '' === $pocetak ) {
	$pocetak = '0000-0-0';
}

if ( '' === $zavrsetak ) {
	$zavrsetak = '0000-0-0';
}

$obj_data['type']              = $obj_type;
$obj_data['pocetak_gradnje']   = $pocetak;
$obj_data['zavrsetak_gradnje'] = $zavrsetak;

if ( $nonce ) {
	$added = $wpdb->insert( $wpdb->map_markers, $obj_data );
}

echo wp_json_encode( array( 'added' => boolval( $added ) ) );
