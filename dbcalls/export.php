<?php
//@phpcs:ignoreFile
require '../../../../wp-load.php';
require 'db.php';

global $wpdb;

if ( isset( $_POST['export'] ) ) {
	$interval = $_POST['Interval'];
	$is_by_year = explode('-', $interval)[0] === 'year';

	header( 'Content-Type: text/csv; charset=utf-8' );
	header( 'Content-Disposition: attachment; filename=data.csv' );
	$output = fopen( 'php://output', 'w' );
	fputs($output, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
	fputcsv( $output, array( 'Objekat', 'Investitor', 'Pocetak gradnje', 'Kraj gradnje', 'Izvodjac', 'Grad', 'Ulica', 'Broj', 'Projektant', 'Spratnost', 'Opis', 'Tip', 'Status' ) );

	if($is_by_year) {
		$interval = explode('year-', $interval)[1];
		$query = "SELECT objekat, investitor, pocetak_gradnje, zavrsetak_gradnje, izvodjac, grad, ulica, broj, projektant, spratnost, opis, type_of_building, type FROM $wpdb->map_markers WHERE tag LIKE " . $interval . " ORDER BY " . $_POST["Order"] . " DESC";
	} else {
		$query  = "SELECT objekat, investitor, pocetak_gradnje, zavrsetak_gradnje, izvodjac, grad, ulica, broj, projektant, spratnost, opis, type_of_building, type FROM $wpdb->map_markers WHERE createdOn >= DATE_SUB(CURDATE(), INTERVAL " . $interval . " DAY) ORDER BY " . $_POST["Order"] . " DESC";
	}

	$results = obj_to_arr( $wpdb->get_results( $query ) );

	foreach ($results as $key => $building) {
	
		$status     = $building['type'];
		$date_start = $building['pocetak_gradnje'];
		$date_end   = $building['zavrsetak_gradnje'];
		$description = $building['opis'];
	
		if($description) {
			$building['opis'] = preg_replace( '/-/', '', $description );
			$building['opis'] = trim( $building['opis'], ' ' );
		}
	
		if($date_start === '0000-0-0') {
			$building['pocetak_gradnje'] = '';
		}

		if($date_end === '0000-0-0') {
			$building['zavrsetak_gradnje'] = '';
		}

		if($status === 'Blue') {
			$building['type'] = 'U Izgradnji';
		} else if($status === 'Red') {
			$building['type'] = 'Obustavljeno';
		} else if($status === 'Green') {
			$building['type'] = 'Završeno';
		} else if($status === 'Yellow') {
			$building['type'] = 'U Planu';
		}

		fputcsv( $output, $building );
	}
	
	fclose( $output );
}
