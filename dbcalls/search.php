<?php
require 'db.php';
require '../../../../wp-load.php';

global $wpdb;

$text_parameter   = $_GET['map-search'];
$select_parameter = $_GET['map-search-select'];

$connection = mysqli_connect( $db, $username, $password );
if ( ! $connection ) {
	die( 'Not connected : ' . mysqli_error( $connection ) );
}

$db_selected = mysqli_select_db( $connection, $database );
if ( ! $db_selected ) {
	die( 'Can\'t use db : ' . mysqli_error( $connection ) );
}

if ( '' !== $text_parameter && 'lokacija' !== $select_parameter ) {

	$query = 'SELECT * FROM wp_maps_markers WHERE LOWER(' . $select_parameter . ") LIKE LOWER('%" . $text_parameter . "%') ORDER BY createdOn DESC";

	$get_data = mysqli_query( $connection, $query );
	if ( ! $get_data ) {
		die( 'Couldnt get ' . mysqli_error( $connection ) );
	}
	if ( $get_data->num_rows === 0 ) {
		echo 'No data';
		return;
	}
	while ( $row = $get_data->fetch_array( MYSQLI_ASSOC ) ) {
		$myArray[] = $row;
	}
	echo json_encode( $myArray );
	return;
} else {
	$query    = 'SELECT * FROM wp_maps_markers ORDER BY createdOn DESC';
	$get_data = mysqli_query( $connection, $query );
	if ( ! $get_data ) {
		die( 'Couldnt get ' . mysqli_error( $connection ) );
	}
	if ( $get_data->num_rows === 0 ) {
		echo 'No data';
		return;
	}
	while ( $row = $get_data->fetch_array( MYSQLI_ASSOC ) ) {
		$myArray[] = $row;
	}
	echo json_encode( $myArray );
	return;
}

echo 'No data';
mysqli_close( $connection );
