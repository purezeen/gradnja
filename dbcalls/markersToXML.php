<?php
// @phpcs:ignoreFile
require 'db.php';

// Start XML file, create parent node
$doc = new DOMDocument( '1.0' );

$node    = $doc->createElement( 'markers' );
$parnode = $doc->appendChild( $node );

// Opens a connection to a MySQL server
$connection = mysqli_connect( $db, $username, $password );
if ( ! $connection ) {
	die( 'Not connected : ' . mysqli_error() );
}

// Set the active MySQL database
$db_selected = mysqli_select_db( $connection, $database );
if ( ! $db_selected ) {
	die( 'Can\'t use db : ' . mysqli_error() );
}

// Select all the rows in the markers table
$query  = 'SELECT * FROM wp_maps_markers WHERE 1 ORDER BY createdOn DESC';
$result = mysqli_query( $connection, $query );
if ( ! $result ) {
	die( 'Invalid query: ' . mysqli_error() );
}
header( 'Content-type: text/xml' );

// Iterate through the rows, adding XML nodes for each
while ( $row = @mysqli_fetch_assoc( $result ) ) {
	// Add to XML document node
	$node    = $doc->createElement( 'marker' );
	$newnode = $parnode->appendChild( $node );

	$newnode->setAttribute( 'id', $row['id'] );
	$newnode->setAttribute( 'objekat', $row['objekat'] );
	$newnode->setAttribute( 'lat', $row['lat'] );
	$newnode->setAttribute( 'lon', $row['lon'] );
	$newnode->setAttribute( 'cena', $row['cena'] );
	$newnode->setAttribute( 'broj_stanova', $row['broj_stanova'] );
	$newnode->setAttribute( 'type', $row['type'] );
	$newnode->setAttribute( 'izvodjac', $row['izvodjac'] );
	$newnode->setAttribute( 'projektant', $row['projektant'] );
	$newnode->setAttribute( 'pocetak_gradnje', $row['pocetak_gradnje'] );
	$newnode->setAttribute( 'zavrsetak_gradnje', $row['zavrsetak_gradnje'] );
	$newnode->setAttribute( 'opis', $row['opis'] );
	$newnode->setAttribute( 'spratnost', $row['spratnost'] );
	$newnode->setAttribute( 'investitor', $row['investitor'] );
	$newnode->setAttribute( 'type_of_building', $row['type_of_building'] );
	$newnode->setAttribute( 'grad', $row['grad'] );
	$newnode->setAttribute( 'ulica', $row['ulica'] );
	$newnode->setAttribute( 'broj', $row['broj'] );
	$newnode->setAttribute( 'linkovi', $row['linkovi'] );
	$newnode->setAttribute( 'slike', $row['slike'] );
	$newnode->setAttribute( 'povrsina_objekta', $row['povrsina_objekta'] );
	$newnode->setAttribute( 'tag', $row['tag'] );
	$newnode->setAttribute( 'putanje', $row['putanje'] );
}

$xmlfile = $doc->saveXML();
echo $xmlfile;
mysqli_close( $connection );
