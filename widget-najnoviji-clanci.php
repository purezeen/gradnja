<!-- sidebar latest post -->
<div class="latest-post sidebar-block">
  <h3 class="title-line title-sidebar"><?php the_field( 'widget_latest_posts_title', $acfw ); ?></h3>

  	<div class="custom-navigation navigation">
      <a href="#" class="flex-prev icon-arrow-left">
        <span class="iconify" data-icon="mdi-light:arrow-left-circle" data-inline="false"></span>
      </a>
      <div class="custom-controls-container"></div>
      <a href="#" class="flex-next icon-arrow-right">
        <span class="iconify" data-icon="mdi-light:arrow-right-circle" data-inline="false"></span>
      </a>
    </div>
      
  <div class="flexslider-latest-post widget-posts">
    <ul class="slides clearfix">
      <?php 
        $counter = 0;
        $widget_post_number = get_field( 'widget_latest_posts_number', $acfw );
      ?>
      
      <?php 
        $args = array(
          'post_type' => array( 'post', 'uradi-sam'),
          'posts_per_page' => $widget_post_number,
          'status' => 'publish',
          'ignore_sticky_posts' => 1
        );
        $query_latest_posts = new WP_Query( $args );
        while ($query_latest_posts->have_posts()) : $query_latest_posts->the_post();
       ?>
       <?php if( $query_latest_posts->current_post % 3 == 0 ) : ?>
        <li>
        <?php endif; ?>
        <a href="<?php the_permalink(); ?>" class="widget-post clearfix">
          <div class="img-wrap">
            <?php the_post_thumbnail( 'thumbnail'); ?>
          </div>
          <div class="widget-post-content">
            <h6 class="widget-post-title"><?php the_title() ?></h6>
            <div class="meta-data">
              <?php echo get_the_date( 'd.m.Y.' ); ?>
            </div>
          </div>
        </a>
<?php if( $query_latest_posts->current_post % 3 == 2 ) : ?>
           </li>
      <!-- row -->
      <?php endif; ?>  
      <?php $counter++ ; 
        endwhile; ?>       
      <?php wp_reset_postdata(); ?>
    </ul>
  </div>
</div>
<!-- end sidebar latest post -->
