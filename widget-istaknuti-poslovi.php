<!-- sidebar featured jobs -->
<?php global $post; ?>

<?php 
    $widget_poslovi = get_field('featured_jobs', 'option');
    if( $widget_poslovi ): ?>

<div class="latest-post sidebar-block">

    <?php if( get_field('featured_jobs_title', 'option')): ?>
    <h3 class="title-line title-sidebar"><?php the_field('featured_jobs_title', 'option'); ?></h3>
    <?php endif; ?>

    <div class="custom-navigation-jobs navigation">
        <a href="#" class="flex-prev icon-arrow-left">
            <span class="iconify" data-icon="mdi-light:arrow-left-circle" data-inline="false"></span>
        </a>
        <div class="custom-controls-container-jobs"></div>
        <a href="#" class="flex-next icon-arrow-right">
            <span class="iconify" data-icon="mdi-light:arrow-right-circle" data-inline="false"></span>
        </a>
    </div>


    <div class="flexslider-jobs widget-posts">
        <ul class="slides clearfix">

            <?php foreach(  $widget_poslovi as $post ): 
              // Setup this post for WP functions (variable must be named $post).
              setup_postdata($post); ?>

            <li>

                <a href="<?php the_permalink(); ?>" class="widget-post clearfix">

                    <?php $expired = false;
                        if(get_field('job_expires')){ 
                            if(is_job_expired('job_expires')){ 
                                $expired=true;
                    }}?>

                    <div class="img-wrap <?php echo ($expired)?'card-image__overlay':''; ?>">
                        <?php the_post_thumbnail( 'thumbnail'); ?>
                    </div>
                    <div class="widget-post-content">
                        <h6 class="widget-post-title"><?php the_title(); ?></h6>
                        <div class="meta-data">
                            <?php //echo get_the_date( 'd.m.Y.' ); ?>

                            <?php if(get_field('job_location')){ ?>
                            <div class="align-center devider"><span class="iconify"
                                    data-icon="bytesize:location"></span>
                                <?php //echo get_the_date(); ?> <?php the_field('job_location'); ?>
                            </div>
                            <?php } ?>
                            <?php if(get_field('job_expires')){ 
                                   if(is_job_expired('job_expires')){ ?>

                            <span class="job-item__expired align-center"><span class="iconify"
                                    data-icon="bytesize:clock" data-inline="false"></span>
                                <span><?php echo 'Istekao'; ?></span>
                            </span>

                            <?php  }else{  ?>

                            <span class="align-center"><span class="iconify" data-icon="bytesize:clock"
                                    data-inline="false"></span>
                                <?php the_field('job_expires');  ?>
                            </span>

                            <?php } ?>

                            <?php } ?>

                        </div>
                    </div>
                </a>

            </li>
            <?php endforeach; ?>

        </ul>
    </div>

</div>
<?php wp_reset_postdata(); ?>

<?php endif; ?>
<!-- end sidebar featured jobs -->