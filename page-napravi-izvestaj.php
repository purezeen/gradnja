<?php  
include 'dbcalls/db.php';
$connect = mysqli_connect('localhost', $username, $password, $database);   
 $query ='SELECT * FROM wp_maps_markers WHERE createdOn >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)';  
 $result = mysqli_query($connect, $query);  
    get_header();

    if(current_user_can('administrator') || current_user_can('editor') || current_user_can('placeni_korisnik'))
{
?>

<body>
     <div class="container">
          <div class="row">
               <div class="col-2">
               
     <br /><br />
     <div class="container" style="width:900px;">
          <h2 align="center">Kreiranje tabele o gradilištima dodatim u prethodnom periodu</h2>
          <h3 align="center">Podaci o gradilištima</h3>
          <br />
          <form method="post" id="export-form"
               action="<?php echo get_template_directory_uri() ?>/dbcalls/export.php" align="center">
               <button type="submit" style="font-size:20px" name="export" value="CSV Export"
                    class="btn btn-success"><span class="iconify" data-icon="simple-icons:microsoftexcel"
                         data-inline="false"></span> CSV Export </button>
               <p>Sortiraj po: </p>
               <select form="export-form" style="width:250px" name="Order" id="export-order">
                    <option value="investitor">Investitor</option>
                    <option value="pocetak_gradnje">Pocetak gradnje</option>
                    <option value="zavrsetak_gradnje">Zavrsetak gradnje</option>
                    <option value="izvodjac">Izvodjac</option>
                    <option value="objekat">Ime objekta</option>
                    <option value="Projektant">Projektant</option>
               </select>
               <p>Period: </p>
               <select form="export-form" style="width:250px" name="Interval" id="export-interval">
                    <option value="1">Prethodni dan</option>
                    <option value="7">Prethodnih 7 dana</option>
                    <option value="15">Prethodnih 15 dana</option>
                    <option value="30">Prethodnih 30 dana</option>
                    <option value="365">Prethodnih godinu dana</option>
               </select>
          </form>
          <br />
          <div style="margin-bottom:50px" class="table-responsive" id="employee_table">
               <table class="table table-bordered">
                    <tr>
                         <th width="10%">Objekat</th>
                         <th width="10%">Investitor</th>
                         <th width="5%">Pocetak gradnje</th>
                         <th width="5%">Kraj gradnje</th>
                         <th width="10%">Izvodjac</th>
                         <th width="10%">Adresa</th>
                         <th width="10%">Projektant</th>
                         <th width="10%">Spratnost</th>
                         <th width="20%">Opis</th>
                    </tr>
                    <?php  
                     while($row = mysqli_fetch_array($result))  
                     {  
                     ?>
                    <tr>
                         <td><?php echo $row["objekat"]; ?></td>
                         <td><?php echo $row["investitor"]; ?></td>
                         <td><?php echo $row["pocetak_gradnje"]; ?></td>
                         <td><?php echo $row["zavrsetak_gradnje"]; ?></td>
                         <td><?php echo $row["izvodjac"]; ?></td>
                         <td><?php echo $row["grad"] . " " . $row["ulica"] . " " . $row["broj"]; ?></td>
                         <td><?php echo $row["projektant"]; ?></td>
                         <td><?php echo $row["spratnost"]; ?></td>
                         <td><?php echo $row["opis"]; ?></td>
                    </tr>
                    <?php       
                     }  
                     ?>
               </table>
          </div>
     </div>
               </div>
          </div>

          <div class="row">
      <div class="col-6 col-lg-9 main-content map-list">
      </div>
      <aside class="col-6 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar(); ?>
         </div>
      </aside>
   </div>
     </div>

     <?php
}
else{
    echo '<script>window.location.href = "404.php";</script>';
}
    get_footer();
?>