<?php if ( have_rows( 'bottom_sidebar_widgets_gtag', 'option' ) ) : ?>
	<?php while ( have_rows( 'bottom_sidebar_widgets_gtag', 'option' ) ) : the_row(); ?>
		<div class="sidebar-block advertising">
			<?php the_sub_field( 'widget_script_and_html' );?>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'sidebar_commercials', 'options') ) : ?>
	<?php while ( have_rows( 'sidebar_commercials', 'options') ) : the_row(); ?>
		<div class="sidebar-block advertising">
			<a href="<?php the_sub_field( 'widget_commercial_url', 'options'); ?>" target="blank">
			<?php $widget_commercial_image = get_sub_field( 'widget_commercial_image', 'options'); ?>
			<?php if ( $widget_commercial_image ) { ?>
				<img class="alignnone  size-full" src="<?php echo $widget_commercial_image['url']; ?>" alt="<?php echo $widget_commercial_image['alt']; ?>"/>
			<?php } ?>
			</a>
		</div>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>