<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
        'post_type' => 'post',
        'cat' => 28653,
        'post_status' => 'publish',
        'orderby' => 'date',
        'order'   => 'DESC',
        'paged' => $paged
      );

    
$wp_query = new WP_Query( $args );

if ( $wp_query->have_posts() ) : ?>
<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

        <?php get_template_part('template-parts/list-job', 'item'); ?>

<?php 
   if(!isMobileDevice()){
    // do nothing
   } else {

    $rows = get_field('top_sidebar_widgets', 'option' ); // get all the rows
    // $rand_row = $rows[ array_rand( $rows ) ]; // get a random row
    global $wp_query;
    $row_count = count(get_field('top_sidebar_widgets', 'option')); // Count
    if( $wp_query->current_post  === 2 ) { 
       if ($row_count >= 1) {
        debug_to_console("2");
          $first_baner = $rows[0];
          $baner_script_first = $first_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
         <article class="card-container home-card inline-baner grid-item">
               <?php echo $baner_script_first; ?>
         </article>
<?php
       }
    }
    if( $wp_query->current_post  === 5 ) { 
       if ($row_count >= 2) {
        debug_to_console("5");
          $second_baner = $rows[1];
          $baner_script_second = $second_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
          <article class="card-container home-card inline-baner grid-item">
               <?php echo $baner_script_second; ?>
         </article>
<?php
       }
    }
    if( $wp_query->current_post  === 8 ) { 
       if ($row_count >= 3) {
        debug_to_console("8");
          $third_baner = $rows[2];
          $baner_script_third = $third_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
<article class="card-container home-card inline-baner grid-item">
    <?php echo $baner_script_third; ?>
</article>
<?php
         }
      }
   }
?>
<?php endwhile; ?>

<?php  wp_reset_postdata();?>

<?php else : ?>

<!-- article -->
<article class="grid-item">
    <h2 class="title"><?php esc_html_e( 'Žao nam je, od traženog sadržaja nema rezultata', 'gulp_wordpress' ); ?></h2>
</article>
<!-- /article -->

<?php endif; ?>