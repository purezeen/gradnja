<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	
<article  id="post-<?php the_ID(); ?>" <?php post_class(array('grid-item')); ?>>
   <div class="card-container">

      <div class="card-image <?php if (has_category('video')) { echo 'video-icon';} ?>">

     <?php 
        if (has_category('video')) : ?>
          <a class="video-icon-wrap">
            <span class="icon-title">
              <span>Play</span>
            </span>
            <img class="video-icon-img" src="<?php echo get_template_directory_uri(); ?>/images/play.svg" alt="play">
          </a>
      <?php endif; ?>
       
				<!-- post thumbnail -->
				<?php if ( has_post_thumbnail() ) : // Check if thumbnail exists. ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php the_post_thumbnail(); ?>
					</a>
				<?php endif; ?>
				<!-- /post thumbnail -->
         <a href="<?php the_permalink(); ?>" class="card-overlay">
          <div class="card-overlay-content">
            <span></span> 
            <span></span> 
            <span></span>
          </div>
        </a>
      </div>
      <!-- end card-image -->

      <div class="card-content">
        <span class="post-categories"><?php the_category( ', ' ); ?></span>
         <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h2 class="card-title"><?php the_title(); ?></h2></a>
         <?php the_excerpt(); ?>
         <div class="meta-data">
            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?>,</a>
            <a class="devider">&nbsp;<?php echo get_the_date( 'd.m.Y.' ); ?></a>
            <a href="<?php the_permalink() ?>/#commets" class="devider align-center">
              <span class="iconify" data-icon="fa-comment-o" data-inline="false"></span>
              <?php
                $comments_count = wp_count_comments( $post->ID );
                echo $comments_count->approved;
              ?>
            </a>
            <span class="align-center">
              <span class="iconify" data-icon="bytesize:clock" data-inline="false"></span>
              <?php echo prefix_estimated_reading_time( get_the_content() ); ?> min 
              <span class="remove">&nbsp;za čitanje</span>
            </span>
         </div>
         <!-- end meta-data -->
      </div>
      <!-- end card-content -->
      
   </div>
   <!-- end card-container -->
   <span><?php edit_post_link() ?></span>
</article>
<!-- end article -->

<?php 
   if(!isMobileDevice()){
    // do nothing
   } else {

    $rows = get_field('top_sidebar_widgets', 'option' ); // get all the rows
    // $rand_row = $rows[ array_rand( $rows ) ]; // get a random row
    global $wp_query;
    $row_count = count(get_field('top_sidebar_widgets', 'option')); // Count
    if( $wp_query->current_post  === 2 ) { 
       if ($row_count >= 1) {
          $first_baner = $rows[0];
          $baner_script_first = $first_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
          <article class="card-container home-card inline-baner grid-item">
            <?php echo $baner_script_first; ?>
          </article>
          <?php
       }
    }
    if( $wp_query->current_post  === 5 ) { 
       if ($row_count >= 2) {
          $second_baner = $rows[1];
          $baner_script_second = $second_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
          <article class="card-container home-card inline-baner grid-item">
            <?php echo $baner_script_second; ?>
          </article>
          <?php
       }
    }
    if( $wp_query->current_post  === 8 ) { 
       if ($row_count >= 3) {
          $third_baner = $rows[2];
          $baner_script_third = $third_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
          <article class="card-container home-card inline-baner grid-item">
            <?php echo $baner_script_third; ?>
          </article>
          <?php
       }
    }

   }
  
  ?>
<?php endwhile; ?>

<?php else : ?>

	<!-- article -->
	<article class="grid-item">
		<h2 class="title"><?php esc_html_e( 'Žao nam je, od traženog sadržaja nema rezultata', 'gulp_wordpress' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
