<?php /* Template Name: Home page */ get_header(); ?>

<section class="container relative">
    <?php 
// the query
$slider_args = array(
  'post_type' => 'post',
  'status' => 'publish',
  'category_name' => 'izdvojeno',
  'posts_per_page' => 4,
  'orderby' => 'post_date',
  'order' => 'DESC'
);
$slider_query = new WP_Query( $slider_args ); ?>
    <!-- Blog slider -->

    <!-- pagination here -->

    <div class="blog-slider clearfix">
        <ul class="slides clearfix">
            <!-- the loop -->
            <?php while ( $slider_query->have_posts() ) : $slider_query->the_post(); ?>
            <li>
                <?php 
                 /* grab the url for the full size featured image */
                  $slider_query_featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                 ?>
                <div class="blog-slider-item cover radius"
                    style="background-image: linear-gradient(to bottom, rgba(50,50,50,0) 0%, rgba(16,15,15,0.91) 100%, rgba(16,15,15,0.63) 93%), url(<?php echo esc_url($slider_query_featured_img_url); ?>)">

                    <div class="blog-slider-content">
                        <span class="post-categories"><?php the_category( ', ' ); ?></span>
                        <a href="<?php the_permalink(); ?>" class="color-white">
                            <h1><?php the_title() ?></h1>
                        </a>
                        <div class="meta-data align-center">
                            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"
                                class="align-center meta-author">
                                <span class="post-author-img">
                                    <?php echo get_avatar( get_the_author_meta( 'ID' )); ?>
                                </span>
                                <?php the_author(); ?>, <a class="devider">&nbsp;<?php echo get_the_date( 'd.m.Y.' ); ?>
                                </a>

                                <a href="<?php the_permalink() ?>/#commets" class="devider align-center meta-comment">
                                    <span class="iconify" data-icon="fa-comment-o" data-inline="false"></span>
                                    <?php 
                                        $comments_count = wp_count_comments( $post->ID );
                                        echo $comments_count->approved;
                                    ?>
                                </a>
                                <span class="align-center meta-time">
                                    <span class="iconify" data-icon="bytesize:clock" data-inline="false"></span>
                                    <?php echo prefix_estimated_reading_time( get_the_content() ); ?> min
                                    <span class="remove">
                                        &nbsp; za čitanje
                                    </span>
                                </span>
                        </div>
                        <!-- end meta-data -->
                    </div>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
        <!-- end of the loop -->
        <?php 
    wp_reset_postdata();
    wp_reset_query();
    $slider_thumb_query = new WP_Query( $slider_args );
 ?>

        <!-- Blog slider mini list -->
        <div class="blog-slider-mini-list widget-posts clearfix">
            <?php while ( $slider_thumb_query->have_posts() ) : $slider_thumb_query->the_post(); ?>
            <a href="<?php the_permalink(); ?>" class="widget-post clearfix">
                <div class="img-wrap">
                    <?php $slider_query_featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?>
                    <img src="<?php echo esc_url($slider_query_featured_img_url); ?>" alt="post1">
                </div>
                <div class="widget-post-content">

                    <h5 class="widget-post-title color-white"><?php the_title(); ?></h5>
                </div>
            </a>
            <?php endwhile; ?>
        </div>
        <?php wp_reset_postdata();
             wp_reset_query(); ?>
        <!-- End Blog slider mini list -->
    </div>
    <div class="custom-navigation-blog">
        <a href="#" class="flex-next"><span class="iconify" data-icon="mdi-light:arrow-left-circle"
                data-inline="false"></span></a>
        <div class="custom-controls-container-blog"></div>
        <a href="#" class="flex-prev"><span class="iconify" data-icon="mdi-light:arrow-right-circle"
                data-inline="false"></span></a>
    </div>
    <!-- end Blog slider -->
</section>

<main class="container">
    <div class="row clearfix">

        <?php 
     if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
     elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
     else { $paged = 1; }
    // the query
    $post_args = array(
      'post_type' => 'post',
      'status' => 'publish',
      'posts_per_page' => 12,
      'orderby' => 'post_date',
      'order' => 'DESC',
      'paged' => $paged,
    );
    $the_query = new WP_Query( $post_args ); ?>

        <div class="col-12 col-lg-9 clearfix main-content">
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


            <?php if(has_category('posao')){ ?>

            <div class="job-item__home">
                <?php get_template_part('template-parts/list-job', 'item'); ?>

                <div class="social-icon-line">
                    <h3 class="title-line title-sidebar"><a href="<?php the_permalink(); ?>">Otvori članak</a></h3>
                    <div class="social-icon">
                        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>/&text=<?php the_title(); ?>"
                            class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:twitter"
                                data-inline="false"></span></a>
                        <a href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="border-icon"
                            target="_blank"><span class="iconify" data-icon="mdi:facebook"
                                data-inline="false"></span></a>
                        <a href="https://pinterest.com/pin/create/bookmarklet/?&url=<?php the_permalink(); ?>/&description=<?php the_title(); ?>/&"
                            class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:pinterest"
                                data-inline="false"></span></a>
                    </div>
                </div>
            </div>

            <?php } else {?>


            <article class="card-container home-card <?php if (has_category('video')) { echo 'video-icon';} ?>">

                <span class="post-categories"><?php the_category( ', ' ); ?></span>
                <a href="<?php the_permalink(); ?>">
                    <h2><?php the_title(); ?></h2>
                </a>
                <div class="meta-data">
                    <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?>,
                    </a>
                    <a class="devider">&nbsp;<?php echo get_the_date( 'd.m.Y.' ); ?></a>
                    <a href="<?php the_permalink() ?>/#commets" class="devider align-center">
                        <span class="iconify" data-icon="fa-comment-o" data-inline="false"></span>
                        <?php 
                $comments_count = wp_count_comments( $post->ID );
                echo $comments_count->approved;
            ?>
                    </a>
                    <span class="align-center">
                        <span class="iconify" data-icon="bytesize:clock" data-inline="false"></span>
                        <?php echo prefix_estimated_reading_time( get_the_content() ); ?> min
                        <span class="remove">&nbsp;za čitanje</span>
                    </span>
                </div>
                <!-- end meta-data -->

                <div class="card-image">
                    <?php the_post_thumbnail( 'full', array( 'sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 1024px, (max-width: 1024px) 100vw, 1024px' ) ); ?>
                    <a href="<?php the_permalink(); ?>" class="card-overlay">
                        <div class="card-overlay-content"><span></span> <span></span> <span></span></div>
                    </a>
                </div>
                <!-- end card-image -->

                <?php the_excerpt(); ?>

                <div class="social-icon-line">
                    <h3 class="title-line title-sidebar"><a href="<?php the_permalink(); ?>">Otvori članak</a></h3>
                    <div class="social-icon">
                        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>/&text=<?php the_title(); ?>"
                            class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:twitter"
                                data-inline="false"></span></a>
                        <a href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="border-icon"
                            target="_blank"><span class="iconify" data-icon="mdi:facebook"
                                data-inline="false"></span></a>
                        <a href="https://pinterest.com/pin/create/bookmarklet/?&url=<?php the_permalink(); ?>/&description=<?php the_title(); ?>/&"
                            class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:pinterest"
                                data-inline="false"></span></a>
                    </div>
                </div>
            </article>
            <!-- end card-container -->

            <?php } ?>

            <?php 
   if(!isMobileDevice()){
    // do nothing
   } else {
    $rows = get_field('top_sidebar_widgets', 'option' ); // get all the rows
    // $rand_row = $rows[ array_rand( $rows ) ]; // get a random row

    $row_count = count(get_field('top_sidebar_widgets', 'option')); // Count
    if( $the_query->current_post  === 2 ) { 
       if ($row_count >= 1) {
          $first_baner = $rows[0];
          $baner_script_first = $first_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
            <article class="card-container home-card inline-baner">
                <?php echo $baner_script_first; ?>
            </article>
            <?php
       }
    }
    if( $the_query->current_post  === 5 ) { 
       if ($row_count >= 2) {
          $second_baner = $rows[1];
          $baner_script_second = $second_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
            <article class="card-container home-card inline-baner">
                <?php echo $baner_script_second; ?>
            </article>
            <?php
       }
    }
    if( $the_query->current_post  === 8 ) { 
       if ($row_count >= 3) {
          $third_baner = $rows[2];
          $baner_script_third = $third_baner['widget_script_and_html' ]; // get the sub field value 
          ?>
            <article class="card-container home-card inline-baner">
                <?php echo $baner_script_third; ?>
            </article>
            <?php
       }
    }

   }
  
  ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata();
      wp_reset_query(); ?>
            <?php 

      if( $the_query->max_num_pages > 1 ) {
        if( get_option('permalink_structure') ) {
          $format = 'page/%#%/';
        } else {
          $format = '&paged=%#%';
        }
        $big = 999999999;
        echo paginate_links( array(
            'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'    => $format,
            'current' => max( 1, $paged ),
            'total'   => $the_query->max_num_pages,
            'mid_size'  => 2,
            'type'      => 'list',
            'prev_text' => esc_html__( 'Prethodno', 'gulp_wordpress' ),
            'next_text' => esc_html__( 'Sledeće', 'gulp_wordpress' ),
        ) );
      }
    ?>
        </div>
        <!-- end main content-->

        <aside class="col-12 col-lg-3 sidebar clearfix ">
            <div class="theiaStickySidebar">
                <?php get_sidebar(); ?>
            </div>
        </aside>
        <!-- end sidebar -->

    </div>
    <!-- end row -->
</main>
<!-- end container -->


<?php get_footer(); ?>