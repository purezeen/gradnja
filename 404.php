<?php
/**
 * 404 page
 */

$default_title = esc_html__( 'Oops, This Page Could Not Be Found!', 'gillion' );
$default_text = esc_html__( 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'gillion' );
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main container" role="main">

			<section class="error-404 not-found row">
				<div class="col-12 col-md-6 mb-4">	
					<div class="circle-404">
						<h2>404</h2>
					</div>
					<div class="shadow-404"></div>
				</div>

				<div class="col-12 col-md-6 col-lg-5">	
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Stranica ne postoji', 'gulp-wordpress' ); ?></h1>
					</header><!-- .page-header -->

					<div class="page-content">
						<h6><?php esc_html_e( 'UPS! Stranica koju ste tražili ne postoji. Koristite pretragu ili idite na naslovnu stranicu.', 'gulp-wordpress' ); ?></h6>

						<h3 class="title-line">Pretražite sajt:</h3>
						<?php
							get_search_form();

							// the_widget( 'WP_Widget_Recent_Posts' );

							// Only show the widget if site has multiple categories.
							if ( gulp_wordpress_categorized_blog() ) :
						?>

						<!-- <div class="widget widget_categories">
							<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'gulp-wordpress' ); ?></h2>
							<ul>
							<?php
								wp_list_categories( array(
									'orderby'    => 'count',
									'order'      => 'DESC',
									'show_count' => 1,
									'title_li'   => '',
									'number'     => 10,
								) );
							?>
							</ul>
						</div> -->

						<?php
							endif;
						?>

					</div><!-- .page-content -->
				</div>
			
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
