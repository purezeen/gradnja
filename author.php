
<?php get_header(); ?>

<main class="container">
   <div class="row clearfix">
      <div class="col-12 col-lg-9 clearfix main-content">
      	 <div class="category-title">
			    	<?php /* Show information about author */ ?>
						<?php $author_id = get_the_author_meta( 'ID' ); ?>
						<div class="post-author grey-background radius" style="margin-bottom: 30px;">
							<div class="circle-img">
								<?php echo get_avatar( $author_id, '185' ); ?>
							</div>
							<div class="content">
								<a class="author__name" href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>"><h6><?php the_author(); ?></h6></a>
								<p class="author__desc"><?php the_author_meta( 'description' ); ?></p>
								<!-- <div class="sh-post-author-icons"> -->
									<?php
										// $userinfo = get_userdata( $author_id );
										// if( $userinfo->user_url ) :
										// 	echo '<a href="'.esc_url( $userinfo->user_url ).'" target="_blank"><i class="fa fa-globe"></i></a>';
										// endif;

										// $usermeta = get_user_meta( $author_id );
										// $meta_fields = array( 'facebook', 'twitter', 'google-plus', 'instagram', 'linkedin', 'pinterest', 'tumblr', 'youtube' );
										// foreach( $meta_fields as $meta) :

										// 	$this_meta = ( isset( $usermeta[$meta][0] ) && $usermeta[$meta][0] ) ? $usermeta[$meta][0] : '';
										// 	if( $this_meta ) :
										// 		echo '<a href="'.esc_url( $this_meta ).'" target="_blank"><i class="fa fa-'.esc_attr( $meta ).'"></i></a>';
										// 	endif;

										// endforeach;
									?>
								<!-- </div> -->
							</div>
						</div>
						<!-- end post-author -->
			    </div>
         <div class="grid">

            <div class="grid-sizer"></div>
            <?php get_template_part( 'loop' ); ?>
        </div>
        <!-- end grid -->
        <?php get_template_part( 'pagination' ); ?>
      </div>
      <!-- end main content-->

       <div class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar();?>
         </div>
      </div> 
    <!--  end sidebar  -->

   </div>
    <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>