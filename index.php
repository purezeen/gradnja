<?php get_header(); ?>

<main class="container">
   <div class="row clearfix">
    <div class="col-12">
     <h1><?php esc_html_e( '', 'gulp_wordpress' ); single_post_title(); ?></h1>
    </div>
      <div class="col-12 col-lg-9 clearfix">
      		<?php
						// Start the Loop.
					while ( have_posts() ) :

						the_content();
						
						endwhile;
					?>
      </div>
      <!-- end main content-->

       <div class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar();?>
         </div>
      </div> 
    <!--  end sidebar  -->

   </div>
    <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>