<?php 
   if(!isMobileDevice()){
 ?>
	<?php if ( have_rows( 'top_sidebar_widgets', 'option' ) ) : ?>
		<?php while ( have_rows( 'top_sidebar_widgets', 'option' ) ) : the_row(); ?>
			<div class="sidebar-block advertising advertising-top">
				<?php the_sub_field( 'widget_script_and_html' );?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
<?php } ?>