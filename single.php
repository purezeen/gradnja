<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<main class="container">
    <div class="row clearfix">

        <div class="col-12 col-lg-9 clearfix  main-content">
            <?php while ( have_posts() ) : the_post(); ?>
            <?php if (has_category('video') && get_field( 'video_url' ) ) { ?>
            <?php $url = get_field( 'video_url' ); 
					preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
					$youtube_id = $match[1];
				 ?>
            <div class="card-image header-post-image header-post-video">
                <iframe width="420" height="345"
                    src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>?autoplay=1&mute=1"></iframe>
            </div>

            <?php } else { ?>
            <?php $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>

            <?php $expired = false;
              if(in_category( 'posao')){
                    if(get_field('job_expires')){ 
                        if(is_job_expired('job_expires')){ 
                            $expired=true;
                }}}?>
            <a href="<?php echo $thumbnail_url; ?>" data-rel="lightcase:myCollection" data-rel="lightcase"
                class="card-image header-post-image <?php echo ($expired)?'card-image__overlay':''; ?>">
                <!-- post thumbnail -->
                <?php if ( has_post_thumbnail() ) : // Check if thumbnail exists. ?>

                <?php the_post_thumbnail('full'); ?>

                <?php endif; ?>
                <div class="post-image-caption"><?php the_post_thumbnail_caption() ?></div>
            </a>
            <?php } ?>


            <article class="inner-post-content">

                <div class="post-content">

                    <div class="post-header">
                        <span class="post-categories"><?php the_category( ', ' ); ?></span>
                        <h1 class="mt-3 mb-3"><?php the_title(); ?></h1>
                        <div class="meta-data mb-5 align-center">
                            <?php if(in_category( 'posao')): ?>

                            <?php if(get_field('job_location')){ ?>
                            <div class="align-center devider"><span class="iconify"
                                    data-icon="bytesize:location"></span>
                                <?php the_field('job_location'); ?>
                            </div>
                            <?php } ?>
                            <?php if(get_field('job_expires')){ 
                                   if(is_job_expired('job_expires')){ ?>

                            <span class="job-item__expired align-center"><span class="iconify"
                                    data-icon="bytesize:clock" data-inline="false"></span>
                                <span><?php echo 'Istekao'; ?></span>
                            </span>

                            <?php  }else{  ?>

                            <span class="align-center"><span class="iconify" data-icon="bytesize:clock"
                                    data-inline="false"></span>
                                <?php the_field('job_expires');  ?>
                            </span>

                            <?php } ?>

                            <?php } ?>

                            <?php else: ?>
                            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"
                                class="align-center"><span
                                    class="post-author-img "><?php echo get_avatar( get_the_author_meta( 'ID' )); ?>
                                </span><?php the_author(); ?>,</a></a><a
                                class="devider">&nbsp;<?php echo get_the_date( 'd.m.Y.' ); ?></a>
                            <a href="<?php the_permalink() ?>/#commets" class="devider align-center meta-comment"><span
                                    class="iconify" data-icon="fa-comment-o" data-inline="false"></span>

                                <?php $comments_count = wp_count_comments( $post->ID ); echo $comments_count->approved; ?>
                            </a>
                            <span class="align-center"><span class="iconify" data-icon="bytesize:clock"
                                    data-inline="false"></span>
                                <?php echo prefix_estimated_reading_time( get_the_content() ); ?>
                                min &nbsp;<span class="remove"> za
                                    čitanje</span></span>
                            <?php endif; ?>
                        </div>
                        <!-- end meta-data -->
                    </div>

                    <div class="post-inner-content">
                        <?php 
                                function isMobileDevice2() {
                                    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
                                }
                                if (function_exists('isMobileDevice2')) {
                                //echo 'y';
                                } else {
                                //echo 'n';
                                }
                                //echo isMobileDevice2();
									if( isMobileDevice2() === 1  || isMobileDevice2() === false) {
										
										$rows = get_field('top_sidebar_widgets', 'option' ); // get all the rows
										// $rand_row = $rows[ array_rand( $rows ) ]; // get a random row

										$row_count = count(get_field('top_sidebar_widgets', 'option')); // Count

										if ($row_count >= 1) {
											$first_baner = $rows[0];
						          $baner_script_first = $first_baner['widget_script_and_html' ]; // get the sub field value

											$banerAfter[3] = $baner_script_first; //display after the third paragraph
										}
										if ($row_count >= 2) {
											$second_baner = $rows[1];
	        						$baner_script_second = $second_baner['widget_script_and_html' ]; // get the sub field value

											$banerAfter[6] = $baner_script_second; //display after the third paragraph
										}
										if ($row_count >= 3) {
											$third_baner = $rows[2];
	          					$baner_script_third = $third_baner['widget_script_and_html' ]; // get the sub field value 

											$banerAfter[9] = $baner_script_third; //display after the third paragraph
										}
										// print_r($second_baner);
										$content = apply_filters( 'the_content', get_the_content() );
										$content = explode("</p>", $content);
										$count = count($content);
										for ($i = 0; $i < $count; $i++ ) {
										    if ( array_key_exists($i, $banerAfter) ) {
										        echo $banerAfter[$i];
										    }
										    echo $content[$i] . "</p>";
										}
							   } else {
							   	
							   		the_content();
									}
								?>

                    </div>

                    <!-- Faktografija section -->
                    <?php if( have_rows('faktografija_lista') ): ?>
                    <section class="fakto">
                        <h5 class="fakto__title">
                            <?php echo get_field('faktografija_naslov') ? the_field('faktografija_naslov')  : 'Faktografija' ?>
                        </h5>
                        <div class="fakto__list">
                            <?php 
                            while( have_rows('faktografija_lista') ) : the_row(); 

                            $faktoLabel = get_sub_field('stavka_label');
                            $faktoDesc = get_sub_field('stavka_opis');
                            ?>

                            <div class="fakto__item">
                                <span class="fakto__label"><?php echo $faktoLabel; ?>:</span>
                                <?php echo $faktoDesc; ?>
                            </div>

                            <?php wp_reset_postdata();  ?>
                            <?php endwhile; ?>

                        </div>

                    </section>
                    <?php endif; ?>
                    <!-- END Faktografija section -->

                    <!-- selected posts / izabrali smo za vas -->
                    <?php if( have_rows('izabrani_linkovi') ): ?>
                    <section class="selected-posts widget-posts">

                        <h4 class="selected-posts__title">
                            <?php echo get_field('selected_post_title') ? the_field('selected_post_title')  : 'Izabrali smo za vas...' ?>
                        </h4>

                        <div class="selected-posts__list">

                            <?php 
                                   while( have_rows('izabrani_linkovi') ) : the_row();
                                  
                                   $selected_link = get_sub_field('izabrani_link');
                                   $selected_url=$selected_link['url'];
                                   $selected_title = $selected_link['title'];
                                   
                                   $postId=url_to_postid( $selected_url );

                               ?>

                            <a href="<?php echo $selected_url; ?>" class="selected-post widget-post">

                                <?php if($postId && has_post_thumbnail( $postId)){ ?>
                                <div class="img-wrap selected-post__img">
                                    <?php echo get_the_post_thumbnail($postId, 'thumbnail'); ?>
                                </div>
                                <?php }?>

                                <div class="selected-post__content">

                                    <h5 class="selected-post__title"><?php echo $selected_title; ?></h5>

                                    <div class="meta-data">
                                        <span
                                            class="post-publish-time">&nbsp;<?php echo get_the_date( 'd.m.Y.',  $postId ); ?></span>
                                    </div>
                                </div>

                            </a>

                            <?php wp_reset_postdata();  ?>
                            <?php endwhile; ?>
                        </div>

                    </section>
                    <?php endif; ?>
                    <!-- end selected posts / izabrali smo za vas -->

                </div>

                <!-- social icon -->
                <div class="vertical-social-icon">
                    <div class="theiaStickySidebar">
                        <div class="social-icon-line">
                            <div class="social-icon">
                                <a href="https://twitter.com/share?url=<?php the_permalink(); ?>/&text=<?php the_title(); ?>"
                                    class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:twitter"
                                        data-inline="false"></span></a>
                                <a href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"
                                    class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:facebook"
                                        data-inline="false"></span></a>
                                <a href="https://pinterest.com/pin/create/bookmarklet/?&url=<?php the_permalink(); ?>/&description=<?php the_title(); ?>/&"
                                    class="border-icon" target="_blank"><span class="iconify" data-icon="mdi:pinterest"
                                        data-inline="false"></span></a>
                            </div>
                        </div>
                    </div>
                </div>



            </article>
            <!-- end inner post content -->

            <?php endwhile; // End of the loop. ?>





            <!-- /3777314/intext 	-->
            <div id='div-gpt-ad-1603355251586-0'>
                <script>
                googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-1603355251586-0');
                });
                </script>
            </div>








            <div class="col-12 col-lg-12 clearfix">

                <?php /* Show Tags */ ?>
                <?php if( count( wp_get_post_tags( get_the_ID() ) ) > 0 ) : ?>
                <div class="post-tags">

                    <span id="adsense">

                        <h4>Pogledajte još članaka sa istim ključnim rečima:</h4>
                        <?php foreach( get_the_tags( get_the_ID() ) as $tag ) :
										$term_link = get_tag_link( $tag->term_id );
									?>
                        <a href="<?php echo esc_url( $term_link ); ?>" class="post-tags-item">
                            #<?php echo esc_attr( $tag->name ); ?>
                        </a>
                        <?php endforeach; ?>

                    </span>


                    <style>
                    @media screen and (min-width: 800px) {
                        #adsense {
                            float: right;
                            width: 46%;
                        }

                    }
                    </style>
                    <span id="adsense">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Pored related 336x280 -->
                        <ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px"
                            data-ad-client="ca-pub-9601128549259195" data-ad-slot="7749095237"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </span>

                </div>
                <?php endif; ?>

            </div>
            <?php 
				$prev_post = get_next_post();
				$next_post = get_previous_post();
				$prev_class = '';
				$next_class = ''; 
			?>
            <div class="post-switch container mt-8">
                <div class="row">
                    <?php if( isset( $prev_post->ID) ) : ?>
                    <div class="col-12 col-md-6">
                        <div class="post-switch-item radius cover"
                            style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url( $prev_post->ID,'medium' ) ); ?>)">
                            <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" class="arrow">
                                <span class="iconify" data-icon="mdi-light:arrow-left-circle"
                                    data-inline="false"></span>
                            </a>
                            <div class="text-left">
                                <span class="post-categories"><?php the_category( ', ', '', $prev_post->ID ); ?></span>
                                <a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
                                    <h2 class="widget-title"><?php echo wp_kses_post( $prev_post->post_title ); ?></h2>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( isset( $next_post->ID) ) : ?>
                    <div class="col-12 col-md-6">
                        <div class="post-switch-item radius cover"
                            style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url( $next_post->ID,'medium' ) ); ?>)">
                            <div class="text-right">
                                <span class="post-categories"><?php the_category( ', ', '', $next_post->ID ); ?></span>
                                <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
                                    <h2 class="widget-title"><?php echo wp_kses_post( $next_post->post_title ); ?></h2>
                                </a>
                            </div>
                            <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="arrow">
                                <span class="iconify" data-icon="mdi-light:arrow-right-circle"
                                    data-inline="false"></span>
                            </a>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php /* Show information about author */ ?>
            <?php $author_id = get_the_author_meta( 'ID' ); ?>
            <?php if($author_id != 2192) : ?>
            <div class="post-author grey-background mt-8 radius">
                <div class="circle-img">
                    <?php echo get_avatar( $author_id, '185' ); ?>
                </div>
                <div class="content">
                    <a class="author__name" href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>">
                        <h6><?php the_author(); ?></h6>
                    </a>
                    <p class="author__desc"><?php the_author_meta( 'description' ); ?></p>
                    <!-- <div class="sh-post-author-icons"> -->
                    <?php
							// $userinfo = get_userdata( $author_id );
							// if( $userinfo->user_url ) :
							// 	echo '<a href="'.esc_url( $userinfo->user_url ).'" target="_blank"><i class="fa fa-globe"></i></a>';
							// endif;

							// $usermeta = get_user_meta( $author_id );
							// $meta_fields = array( 'facebook', 'twitter', 'google-plus', 'instagram', 'linkedin', 'pinterest', 'tumblr', 'youtube' );
							// foreach( $meta_fields as $meta) :

							// 	$this_meta = ( isset( $usermeta[$meta][0] ) && $usermeta[$meta][0] ) ? $usermeta[$meta][0] : '';
							// 	if( $this_meta ) :
							// 		echo '<a href="'.esc_url( $this_meta ).'" target="_blank"><i class="fa fa-'.esc_attr( $meta ).'"></i></a>';
							// 	endif;

							// endforeach;
						?>
                    <!-- </div> -->

                    <a class="author__link" href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>">Pogledaj
                        sve tekstove <small>(<?php echo count_user_posts( $author_id )?>)</small></a>

                </div>
            </div>
            <?php endif;  ?>
            <!-- end post-author -->

            <!-- similar post -->
            <div class="similar-post relative mt-8">

                <h3 class="title-line title-sidebar"><?php esc_html_e( 'Srodni tekstovi', 'gulp-wordpress' ); ?></h3>

                <div class="navigation custom-navigation-carousel">
                    <a href="#" class="flex-prev icon-arrow-left">
                        <span class="iconify" data-icon="mdi-light:arrow-left-circle" data-inline="false"></span>
                    </a>
                    <div class="custom-controls-container-carousel"></div>
                    <a href="#" class="flex-next icon-arrow-right">
                        <span class="iconify" data-icon="mdi-light:arrow-right-circle" data-inline="false"></span>
                    </a>
                </div>
                <?php
					$categories = array();
					foreach( get_the_category( get_the_ID() ) as $category ) :
						$categories[] = $category->term_id;
					endforeach;

					$args = array(
						'post__not_in' => array( get_the_ID() ),
						'posts_per_page' => 6,
						'ignore_sticky_posts' => 1,
						'tax_query' => array(
							array(
								'taxonomy' => 'category',
								'field'    => 'term_id',
								'terms'    => $categories,
								'operator' => 'IN',
							)
						),
						'orderby' => 'rand'
					);
					$query = new WP_Query( $args );

					if( $query->post_count < 3 ) :
						$args = array(
							'post__not_in' => array( get_the_ID() ),
							'posts_per_page' => 6,
							'ignore_sticky_posts' => 1,
							'orderby' => 'rand'
						);
						$query = new WP_Query( $args );
					endif;

					if( $query->have_posts() ) :?>
                <div class="flexslider-carousel carousel">
                    <ul class="slides">
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <li>
                            <div class="card-container">
                                <?php
                                  if (has_post_thumbnail()) {
                                    $backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                  }
								?>
                                <div class="card-image cover <?php if (has_category('video')) {
                                    echo 'video-icon';
                                } ?>" style="background-image:url(<?php echo $backgroundImg[0]?>)">

                                    <?php 
										if (has_category('video')) : ?>
                                    <a class="video-icon-wrap">
                                        <span class="icon-title">
                                            <span>Play</span>
                                        </span>
                                        <img class="video-icon-img"
                                            src="<?php echo get_template_directory_uri(); ?>/images/play.svg"
                                            alt="play">
                                    </a>
                                    <?php endif; ?>

                                    <a href="<?php the_permalink(); ?>" class="card-overlay">
                                        <div class="card-overlay-content">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </a>
                                </div>
                                <!-- end card-image -->

                                <div class="card-content">
                                    <span class="post-categories"><?php the_category( ', ' ); ?></span>
                                    <a href="<?php the_permalink(); ?>">
                                        <h2 class="card-title"><?php the_title(); ?></h2>
                                    </a>

                                    <div class="meta-data">
                                        <span><?php echo get_the_date( 'd.m.Y.' ); ?></span>
                                    </div>
                                    <!-- end meta-data -->
                                </div>
                                <!-- end card-content -->

                            </div>
                            <!-- end card-container -->
                        </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <?php endif; wp_reset_postdata(); ?>
            </div>
            <!-- end similar post -->

            <div class="cta-newsletter">
                <div class="cta-content">
                    <h6>PRIJAVI SE NA NEWSLETTER</h6>
                    <p>Najnovije vesti s našeg portala svakog petka u vašem sandučetu.</p>
                </div>
                <div class="cta-btn">

                    <div id="mc_embed_signup" class="search-wrap newsletter">
                        <form
                            action="https://gradnja.us10.list-manage.com/subscribe/post?u=2ef2edad01a5f07846c336407&amp;id=ec50ce55a2"
                            method="post" id="mc-embedded-subscribe-form " name="mc-embedded-subscribe-form"
                            class="validate searchform" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">

                                <input type="email" value="" name="EMAIL" class="email search-field" id="mce-EMAIL"
                                    placeholder="E-mail adresa" required>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                        name="b_f90c3b6b7a255ca3b7157f51f_bb4f0ec2a9" tabindex="-1" value=""></div>
                                <label class="search-icon">
                                    <div class="clear"><input type="submit" value="Prijavi se" name="subscribe"
                                            id="mc-embedded-subscribe" class="button search-button"></div>
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
		while ( have_posts() ) : the_post();


				if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

        </div>

        <!-- end main content-->

        <div class="col-12 col-lg-3 sidebar clearfix">

            <div class="theiaStickySidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
        <!-- end sidebar -->

    </div>
    <!-- end row -->
    <!-- end container -->

    <?php
get_footer();