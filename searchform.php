<div class="search-wrap">
   <form id="searchform" class="searchform" method="get" action="<?php echo home_url('/'); ?>">
      <input type="text" class="search-field" name="s" placeholder="Upišite traženu reč..." value="<?php the_search_query(); ?>">
      <!-- <input type="hidden" name="post_type" value="post"> -->
  		<!-- <input type="hidden" name="post_type" value="uradi-sam"> -->
      <div class="search-icon"><button class="search-button" type="submit" value=""></button><span class="iconify" data-icon="ei:search" data-inline="false"></span></div>
   </form>
</div>

