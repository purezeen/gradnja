<?php get_header(); ?>
<div class="breadcrumb-titlebar">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class=""><?php esc_html_e( '', 'gulp_wordpress' ); single_cat_title(); ?></h1>
                <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
            </div>
        </div>
    </div>
</div>


<main class="container">
    <div class="row clearfix">
        <div class="col-12 col-lg-9 clearfix main-content">

            <?php
                 $args = array(
                  'posts_per_page' => 4, 
                  'cat' => '57'
                  );
                 
               $lastPosts = new WP_Query( $args );
                 
               // Check that we have query results.
               if ( $lastPosts->have_posts() ) {
                 
                   // Start looping over the query results.
                   while ( $lastPosts->have_posts() ) {
                 
                       $lastPosts->the_post();
               
                        $media = get_media_embedded_in_content(
                          apply_filters( 'the_content', get_the_content() ), 'iframe'
                        );
                        
                       if(!empty($media)){
                        preg_match('/src="([^"]+)"/', $media[0], $match);
                        $fullSrc = $match[1];
               
                        // add extra params to iframe src
                        $params = array(
                           'autoplay'=> 1,
                           'mute' => 1
                        );
                        
                        $newSrc = add_query_arg($params, $fullSrc);
                      
                        ?>
                           <div class="card-image header-post-image header-post-video">
                               <iframe width="420" height="345" src="<?php echo $newSrc; ?>"></iframe>
                           </div>
                           <?php  
                        break;
                     }
                   }
               }
               wp_reset_postdata();
            ?>


            <div class="grid">
                <div class="grid-sizer"></div>
                <?php get_template_part( 'loop' ); ?>
            </div>
            <!-- end grid -->
            <?php get_template_part( 'pagination' ); ?>
        </div>

        <!-- end main content-->

        <div class="col-12 col-lg-3 sidebar clearfix ">
            <div class="theiaStickySidebar">
                <?php get_sidebar();?>
            </div>
        </div>
        <!--  end sidebar  -->

    </div>
    <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>