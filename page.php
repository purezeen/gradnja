<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php the_title(); ?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>
<main class="container">
   <div class="row clearfix">

      <div class="col-12 col-lg-9 clearfix main-content main-content--link">

					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					?>
        <!-- end grid -->
      </div>
      <!-- end main content-->

       <div class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar();?>
         </div>
      </div> 
    <!--  end sidebar  -->

   </div>
    <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>