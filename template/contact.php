<?php /* Template Name: Kontakt */ get_header(); ?>
<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php the_title(); ?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<main class="container">
   <div class="row">
      <div class="col-12 col-lg-9 main-content">
      <div class="contact-header">
            <div>
               <img src="<?php echo get_template_directory_uri() ?>/images/gradnja_logo.png" alt="Sonja Bristina" class="radius">
            </div>
            <div class="left-border">
               <p>  Adresa: Bulevar Oslobođenja 30a,<br>
                  21000 Novi Sad, Srbija<br> Email: kontakt@gradnja.rs</p>
            </div>
         </div>
      <?php
      while ( have_posts() ) : the_post();
         the_content();
      endwhile;
      ?>
      
      </div>
      <div class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar(); ?>
         </div>
      </div>
      <!-- end sidebar -->

   </div>
   <!-- end row -->
</main>
<!-- end container -->

<?php get_footer(); ?>