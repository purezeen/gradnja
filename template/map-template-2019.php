<?php /* Template Name: Mapa 2019 */ get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="breadcrumb-title">
               <h1 class=""><?php the_title(); ?></h1>
               <p class="mt-0 mb-0">Saznajte šta se zida u vašem komšiluku!</p>
            </div>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>


<div class="container">
   <div class="row">
      <div class="col-12 map-legend">
         <div>
            <p class="blue"><span class="circle"></span> u izgradnji</p>
            <p class="yellow"><span class="circle"></span> u planu</p>
            <p class="green"><span class="circle"></span> završeno</p>
            <p class="red"><span class="circle"></span> obustavljeno</p>
         </div>
      </div>
      <div class="col-12 map-iframe">
         <iframe style="border: 0; position: absolute; width: 100%; height:100%; top:0; left:0;" src="https://www.google.com/maps/d/embed?mid=15cqH1EkUjspyXmtfkEYwuWtEjDxCZhyi"
            width="1200" height="780" frameborder="0"><span data-mce-type="bookmark"
               style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;"
               class="mce_SELRES_start">﻿</span><span data-mce-type="bookmark"
               style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;"
               class="mce_SELRES_start">﻿</span><span data-mce-type="bookmark"
               style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;"
               class="mce_SELRES_start">﻿</span><span data-mce-type="bookmark"
               style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;"
               class="mce_SELRES_start">﻿</span></iframe>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
    
     
      <div class="col-12 col-lg-9 main-content main-content--link map-list">

         <?php
         while ( have_posts() ) : the_post();
            the_content();
         endwhile;
         ?>

      </div>
      <aside class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar(); ?>
         </div>
      </aside>
   </div>
</div>

<?php get_footer(); ?>