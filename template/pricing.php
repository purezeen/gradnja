<?php /* Template Name: Pricing */ get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php the_title(); ?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<main class="container">
   <div class="row clearfix">

      <div class="col-12 col-lg-9 clearfix main-content address-price">
         <p>Ukoliko želite da upišete vašu firmu u <strong><a href="/adresar"
                  target="_blank" class="color-blue"> Adresar</a></strong> u okviru portala Gradnja.rs, kako bi vas
            potencijalni klijenti što lakše pronašli, na raspolaganju su vam sledeće opcije:</p> <br><br><br>
         <div class="card-price-wrap">
            <a href="/add-directory-listing/" class="card-price">
               <div class="card-header">
                  <h3>FREE</h3>
               </div>
               <div class="card-body">
                  <p>Sa osnovnim, besplatnim, paketom dobijate sledeće mogućnosti:</p>
                  <ul class="unstyle-list">
                     <li>Upis kontakt podataka u jednu kategoriju,</li>
                     <li>Logo firme,</li>
                     <li>Pozicioniranje vaše firme na mapi,</li>
                     <li>Direktan link do vašeg sajta,</li>
                     <li>Informativni tekst o vašoj firmi,</li>
                     <li>Kontakt s potencijalnim klijentima</li>
                  </ul>

               </div>
               <span class="btn btn-border">Dodaj
                  firmu</span>
            </a>
            <a href="#form" class="card-price">
               <div class="card-header">
                  <h3>STANDARD</h3>
                  <h6>2.000 dinara<br><span> mesečno*</span></h6>
               </div>
               <div class="card-body">
                  <p>Standard paket uključuje sve što i paket <strong>FREE</strong> plus sledeće:</p>
                  <ul class="unstyle-list">
                     <li>Status izdvojene firme u izabranoj kategoriji,</li>
                     <li>Pojavljivanje profila firme u srodnim tekstovima na portalu Gradnja.rs,</li>
                     <li>10 proizvoda i usluga.</li>
                  </ul>

               </div>
               <span class="btn">Naruči paket</span>
            </a>
            <a href="#form" class="card-price premium">
               <div class="card-header">
                  <h3>PREMIUM</h3>
                  <h6>3.500 dinara<br><span>mesečno*</span></h6>
               </div>
               <div class="card-body">
                  <p>Premium paket uključuje sve što i paketi FREE i STANDARD plus sledeće:</p>
                  <ul class="unstyle-list">
                     <li>Status izdvojene firme u tri (3) izabrane kategorije,</li>
                     <li>25 proizvoda i usluga sa mogućnošću ubacivanje PDF kataloga,</li>
                     <li>Popust od 50% na Standard banere na portalu Gradnja.rs</li>
                  </ul>

               </div>
               <span href="#" class="btn">Naruči paket</span>
            </a>
            <p class="info">* plaćanje se vrši na godišnjem nivou</p>
         </div>

         <div>
            <h5>5 benefita za izdvojene firme u Adresaru</h5>
            <p class="mb-5">Ukoliko želite da upišete vašu firmu u <a href="#"> Adresar </a>u okviru portala Gradnja.rs,
               kako bi vas potencijalni klijenti što lakše pronašli, na raspolaganju su vam sledeće opcije:</p>
            <ul class="unstyle-list address-benefit">
               <li><span class="count-number">1</span>Firma se pojavljuje prva u izabranim kategorijama u
                  Adresaru<br><br>
                  <a href="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-1.png"
                     data-rel="lightcase:myCollection" data-rel="lightcase"
                     data-lc-caption="Firma se pojavljuje prva u izabranim kategorijama u Adresaru"> <img
                        src="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-1.png" alt=""></a>
               </li>
               <li><span class="count-number">2</span>Dodavanje proizvoda, usluga i kataloga (JPG, GIF, PDF…) u
                  Adresaru<br><br>
                  <a href="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-2.png"
                     data-rel="lightcase:myCollection" data-rel="lightcase"
                     data-lc-caption="Dodavanje proizvoda, usluga i kataloga (JPG, GIF, PDF…) u Adresaru"> <img
                        src="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-2.png" alt=""></a>
               </li>
               <li><span class="count-number">3</span>Pojavljivanje profila u sidebaru na svim stranicama na portalu
                  Gradnja.rs<br><br>
                  <a href="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-3.png"
                     data-rel="lightcase:myCollection" data-rel="lightcase"
                     data-lc-caption="Pojavljivanje profila u sidebaru na svim stranicama na portalu Gradnja.rs"> <img
                        src="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-3.png" alt=""></a>
               </li>
               <li><span class="count-number">4</span>Pojavljivanje profila na kraju srodnih tekstova na portalu
                  Gradnja.rs<br><br>
                  <a href="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-4.png"
                     data-rel="lightcase:myCollection" data-rel="lightcase"
                     data-lc-caption="Pojavljivanje profila na kraju srodnih tekstova na portalu Gradnja.rs"> <img
                        src="<?php echo get_template_directory_uri() ?>/images/adresar-benefiti-4.png" alt=""></a>
               </li>
            </ul>
         </div>

         <div class="application_forms">
            <br><br>
            <h5>Prijava za paket</h5>
            <p><strong>Nije zanemarljivo, zar ne? Ukoliko vam se sviđa naša ponuda, naručite jedan od naših
                  paketa!</strong></p>
            <div id="form"></div>
            <?php echo do_shortcode('[contact-form-7 id="91688" title="Prijava za paket"]' );?>

         </div>
      </div>
      <!-- end main content-->
      <div class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar();?>
         </div>
      </div>
      <!--  end sidebar  -->


   </div>
   <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>