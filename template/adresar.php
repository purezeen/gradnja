<?php

/* Template Name: Adresar */ get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php the_title(); ?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<main class="container">
   <div class="row">

      <div class="col-12">
         <?php
         while ( have_posts() ) : the_post();
            the_content();
         endwhile;
         ?>
      </div>

   </div>
    <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>