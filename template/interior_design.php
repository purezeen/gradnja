<?php /* Template Name: Interior design  */ get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php the_title(); ?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<main class="container">
   <div class="row clearfix">
      <div class="col-12 col-lg-9 clearfix main-content main-content--link">
         <?php
            while ( have_posts() ) : the_post();
               the_content();
            endwhile;
         ?>

       
         <div class="grid-gallery clearfix">

         <div class="grid-sizer"></div>


         <?php the_field( 'masonry_gallery' ); ?>

         </div>
         <!-- end grid -->

         <div class="cta clearfix">
            <div>
               <img src="<?php echo get_template_directory_uri() ?>/images/enterijer-sonja-brstina-logo.svg"
                  alt="Sonja Bristina">
               <p><strong>arhitekta Sonja Bristina</strong><br>
                  arhitekta i rendering * studio za unutrašnji dizajn gradnja.rs <br> + 381 62 899 55 64 *
                  sonja@gradnja.rs
               </p>
            </div>
            <a href="https://www.interiors.rs/portfolio/" class="btn" target="_blank">CELOKUPAN PORTFOLIO</a>
         </div>
      </div>
      <!-- end main content-->

      <div class="col-12 col-lg-3 sidebar clearfix">
         <div class="theiaStickySidebar">
            <?php get_sidebar();?>
         </div>
      </div>
      <!--  end sidebar  -->
   </div>
   <!-- end row  -->
</main>
<!-- end container -->
<?php get_footer(); ?>