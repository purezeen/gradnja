<?php /* Template Name: Uradi sam */ get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="breadcrumb-title">
               <h1 class=""><?php the_title(); ?></h1>
            </div>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<div class="container">
   <div class="row">
      <div class="col-12 col-lg-9 main-content main-content--link">
         <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
         <?php endwhile; endif; ?>
         <div class=" radius video iframe-wrap mb-5 clearfix">
            <?php $url = get_field( 'video_url' ); 
               preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
               $youtube_id = $match[1];
            ?>
            <iframe width="420" height="345" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>?autoplay=1&mute=1&sr"></iframe>
         </div>

         <div class="grid-uradi-sam clearfix">

            <div class="grid-sizer"></div>


            <?php 
               // ID of Uradi Sam category is 25640
               $parent = '25841';

               $catlist = get_categories(
                       array(
                       'child_of' => $parent,
                       'orderby' => 'id',
                       'order' => 'ASC',
                       'exclude' => $parent,
                       'hide_empty' => '0'
                       ) );
               // print_r($catlist);
               foreach ($catlist as $cat) {
                  ?>
            <article class="grid-item">
               <div class="card-container">

                  <div class="card-image">
                     <?php
                        // Load image of category 
                        $taxonomy_prefix = 'category';
                        $term_id = $cat->term_id;
                        $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;
                     ?>
                     <img src="<?php the_field( 'image', $term_id_prefixed ); ?>" />
                     <a href="<?php echo home_url(); ?>/category/<?php echo $cat->slug; ?>" class="card-overlay">
                        <div class="card-overlay-content"><span></span> <span></span> <span></span></div>
                     </a>
                  </div>
                  <!-- end card-image -->
                  <div class="card-content">
                     <h5><?php echo $cat->name; ?></h5>

                     <ul>
                     <?php 
                     $args = array( 'category' => $term_id, 'post_type' =>  'uradi-sam' ); 
                     $postslist = get_posts( $args );
                     foreach ($postslist as $post) :  setup_postdata($post); 
                     ?>  
                     <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li> 
                     <?php 
                     endforeach;
                     ?>
                     </ul>
                     <span class="btn-expand">Prikaži više <span class="iconify" data-icon="fe-arrow-right" data-inline="false"></span></span>
                  </div>
                  <!-- end card-content -->

               </div>
               <!-- end card-container -->
            </article>
                 <?php

               }
             ?>


         </div>
         <!-- end grid -->
         <div>
            <?php echo do_shortcode('[sabai-directory-slider slider_height=200 slider_border=1 featured_only=1]');?>
         </div>

         <?php
            while ( have_posts() ) : the_post();

                  if ( comments_open() || get_comments_number() ) :
                  comments_template();
               endif;

            endwhile; // End of the loop.
            ?>

      </div>
      <aside class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar(); ?>
         </div>
      </aside>
   </div>
</div>

<?php get_footer(); ?>