<?php /* Template Name: Mapa API */ get_header(); ?>
<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="breadcrumb-title">
               <h1 class=""><?php the_title(); ?></h1>
               <p class="mt-0 mb-0">Saznajte šta se zida u vašem komšiluku!</p>
            </div>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>


<div class="container">
   <div class="row">
      <div class="col-12 map-iframe">
         <div id="map"></div>
         <div class="map__info">
            <button id="exit-panel"><span class="iconify" data-icon="feather:x-square"></span></button>
            <div class="map__info--project">
               <div class="project__gallery">
                  <a id="lightbox-zgrada"
                     href="https://www.gradnja.rs/wp-content/uploads/2021/01/gradnja-logo-bez-slogana-beli.png"
                     data-lightbox="image-1" data-title="My caption">
                        <img id="zgrada"
                        src="https://www.gradnja.rs/wp-content/uploads/2021/01/gradnja-logo-bez-slogana-beli.png"
                        alt="zgrada">
                     </a>
               </div>
               <div class="project__info">
                  <div class="project__name">
                     <button class="back__button" id="back-button"><span class="iconify" data-icon="feather:chevron-left"></span> </button> <div class="name">
                              <h2 id="project-name"></h2>
                  </div>
                  <button class="direction__button" id="direction-button"><strong><span class="iconify" data-icon="feather:chevron-right"></span></strong></button>
               </div>
               <h3>Opis Projekta:</h3>
               <div class="project__description--padding">
                  <div id="project-details" class="project__description">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="map__search">
      <button id="exit-panel2"><span class="iconify" data-icon="feather:x-square"></span></button>
         <form id="map-search-form" action="" method="get">
            <div class="search__contianer">
               <div class="search__bar">
                  <input id="map-search" name="map-search" type="text" placeholder="Search..">
               </div>
               <div class="search__select">
                  <select id="map-search-select" name="map-search-select">
                     <option value="objekat" selected >Po imenu zgrade</option>
                     <option value="investitor">Po investitoru</option>
                     <option value="grad">Po gradu</option>
                     <option value="ulica">Po ulici</option>
                     <option value="type_of_building">Po tipu zgrade</option>
                     <option value="projektant">Po projektantu</option>
                  </select>
               </div>
               <div class="search__info"></div>
            </div>
         </form>
      </div>
   </div>
   <div class="col-12 map-legend">
      <div>
         <a class="blue filter-link" onclick="filterMarkers('Blue')"><span class="circle"></span> u izgradnji</a>
         <a class="yellow filter-link" onclick="filterMarkers('Yellow')"><span class="circle"></span> u planu</a>
         <a class="green filter-link" onclick="filterMarkers('Green')"><span class="circle"></span> završeno</a>
         <a class="red filter-link" onclick="filterMarkers('Red')"><span class="circle"></span> obustavljeno</a>
         <a class="black filter-link" onclick="filterMarkers('All')"><span class="circle"></span> sva gradilišta</a>
      </div>
   </div>
</div>
<div class="col-12 col-lg-9 main-content map-list">
   
</div>
</div>


<div class="container">
   <div class="row">
      <div class="col-12 col-lg-9 main-content main-content--link map-list">
         <h6><strong>Primetili ste novo gradilište? Javite nam!<strong></h6>
         <p style="font-size:16px">
            Naša građevinska mapa svakodnevno raste. Uz vašu pomoć, ona će biti još tačnija i ažurnija.
            Ukoliko ste primetili novo gradilište bilo gde u Srbiji, javite nam gde se ono nalazi!
            Pošaljite nam informacije koje o njemu imate, tačnu adresu, a posebno ćemo ceniti ako nam pošaljete i fotografije.
         </p>

         <?php
         while ( have_posts() ) : the_post();
            the_content();
         endwhile;
         ?>

      </div>
      <aside class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar(); ?>
         </div>
      </aside>
   </div>
</div>

<script type="text/javascript">
   $(document).ready(function () {
      $("#exit-panel").on("click", function () {
         const $mapInfo = $(".map__info");

         if ($mapInfo.hasClass("active")) {
            $mapInfo.animate({
               left: -400
            });
         } else {
            $mapInfo.animate({
               left: 0
            });
         }

         $mapInfo.toggleClass("active");
         
      });

      $("#exit-panel2").on("click", function () {
         const $searchInfo = $(".map__search");
         if ($searchInfo.hasClass("active")) {
            $searchInfo.animate({
               right: -400
            });
            $("#controlUI").css("marginRight", "10px")
         } 
         else {
            $searchInfo.animate({
               right: 0
            });
            $("#controlUI").css("marginRight", "250px")
         }

         $searchInfo.toggleClass("active");
      });

      $.ajax({
            method: "get",
            url: "<?php echo get_template_directory_uri() ?>/dbcalls/search.php",
            data: $('#map-search-form').serialize()
         }).done(function (data) {
            $(".search__info").html(rawToHTML(data));
         })
         .fail(function (req, e) {
            alert("Desila se greška pri pretraživanju. Molimo pokušajte ponovo");
         })
         .always(function () {});
   })
   $(document).on("click", "a.markerAnchor", function () {
      panToMarker($(this).html());
      const $searchInfo = $(".map__search");
         if ($searchInfo.hasClass("active")) {
            $searchInfo.animate({
               right: -400
            });
            $("#controlUI").css("marginRight", "10px")
         } 
         else {
            $searchInfo.animate({
               right: 0
            });
            $("#controlUI").css("marginRight", "250px")
         }

         $searchInfo.toggleClass("active");
   })
   var changeTimer = false;

   $("#map-search").on("keydown", function (e) {
      if(e.keyCode === 10 || e.keyCode === 13){
         e.preventDefault();
      }
      if (changeTimer !== false) clearTimeout(changeTimer);
      changeTimer = setTimeout(function () {
         /* your ajax here */
         $.ajax({
               method: "get",
               url: "<?php echo get_template_directory_uri() ?>/dbcalls/search.php",
               data: $('#map-search-form').serialize()
            }).done(function (data) {
               $(".search__info").html(rawToHTML(data));
               filterMarkersByName(data);
            })
            .fail(function (req, e) {
               alert("Desila se greška pri pretraživanju. Molimo pokušajte ponovo");
            })
            .always(function () {});
         changeTimer = false;
      }, 300);
   });

   $("#map-search-select").on("change", function (e) {
      
      if (changeTimer !== false) clearTimeout(changeTimer);
      changeTimer = setTimeout(function () {
         /* your ajax here */
         $.ajax({
               method: "get",
               url: "<?php echo get_template_directory_uri() ?>/dbcalls/search.php",
               data: $('#map-search-form').serialize()
            }).done(function (data) {
               $(".search__info").html(rawToHTML(data));
               filterMarkersByName(data);
            })
            .fail(function (req, e) {
               alert("Desila se greška pri pretraživanju. Molimo pokušajte ponovo");
            })
            .always(function () {});
         changeTimer = false;
      }, 300);
   });

   //TO DO: Napraviti da ova funckija radi mnogo bolje kada bude u mapi
   function rawToHTML(data) {
      let html = "";
      if (data !== "No data") {
         data = JSON.parse(data);
         for (let i = 0; i < data.length; i++) {
            html += "<h1><a class='markerAnchor' src='#'>" + data[i].objekat + "</a></h1>";
            if(data[i].broj == 0 || data[i].broj == "0")
               html += "<p>" + data[i].grad + " " + data[i].ulica + "</p>";
            else
               html += "<p>" + data[i].grad + " " + data[i].ulica + " " + data[i].broj + "</p>";
         }
      } else html = "Nema rezultata";
      return html;

   }
</script>

<?php get_footer(); ?>