  <?php

/* Template Name: Address categories */ get_header(); ?>

<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php the_title(); ?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<div class="container">
	<div class="row">

		<div class="col-12 col-lg-9 clearfix main-content">
			<?php
			while ( have_posts() ) : the_post();
				the_content();
			endwhile;
			?>
			<?php
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			if ( is_singular() ) :
				wp_enqueue_script( 'comment-reply' );
			endif;
			?>

		</div>

		<aside class="col-12 col-lg-3 sidebar clearfix ">
		<div class="theiaStickySidebar">
		<?php get_sidebar(); ?>
		</div>
	</aside>
	<!-- end sidebar -->
</div>

<?php
// get_sidebar();
get_footer();
