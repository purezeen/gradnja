<?php /* Template Name: Mapa API */ get_header(); ?>
<div class="breadcrumb-titlebar">
   <div class="container">
	  <div class="row">
		 <div class="col-12">
			<div class="breadcrumb-title">
			   <h1 class=""><?php the_title(); ?></h1>
			   <p class="mt-0 mb-0">Saznajte šta se zida u vašem komšiluku!</p>
			</div>
			<div class="breadcrumb"><?php get_breadcrumb(); ?></div>
		 </div>
	  </div>
   </div>
</div>

<div class="container">
   <div class="row">
	  <div class="col-12 map-iframe">
		 <div id="map"></div>
		 <div class="map__info">
			<button id="exit-panel"><span class="iconify" data-icon="feather:x-square"></span></button>
			<div class="map__info--project">
			   <div class="project__gallery">
				  
			   </div>
			   <div class="project__info">
				  <div class="project__name">
					 <button class="back__button" id="back-button"><span class="iconify" data-icon="feather:chevron-left"></span> </button> <div class="name">
							  <h2 id="project-name"></h2>
				  </div>
				  <button class="direction__button" id="direction-button"><strong><span class="iconify" data-icon="feather:chevron-right"></span></strong></button>
			   </div>
			   <h3>Opis Projekta:</h3>
			   <div class="project__description--padding">
				  <div id="project-details" class="project__description">
				  </div>
			   </div>
			</div>
		 </div>
	  </div>
	  <div class="map__search">
	  <button id="exit-panel2"><span class="iconify" data-icon="feather:x-square"></span></button>
		 <form id="map-search-form" action="" method="get">
			<div class="search__contianer">
			   <div class="search__bar">
				  <input id="map-search" name="map-search" type="text" placeholder="Search..">
			   </div>
			   <div class="search__select">
				  <select id="map-search-select" name="map-search-select">
					 <option value="objekat" selected >Po imenu zgrade</option>
					 <option value="lokacija">Po lokaciji</option>
					 <option value="investitor">Po investitoru</option>
					 <option value="grad">Po gradu</option>
					 <option value="type_of_building">Po tipu zgrade</option>
					 <option value="projektant">Po projektantu</option>
				  </select>
			   </div>
			   <div class="search__info"></div>
			</div>
		 </form>
	  </div>
   </div>
   <div class="col-12 map-legend">
	<section>
		<h3>Status:</h3>
		<a class="blue filter-link" data-type="Blue"><span class="circle"></span> u izgradnji</a>
		<a class="yellow filter-link" data-type="Yellow"><span class="circle"></span> u planu</a>
		<a class="green filter-link" data-type="Green"><span class="circle"></span> završeno</a>
		<a class="red filter-link" data-type="Red"><span class="circle"></span> obustavljeno</a>
		<a class="black filter-link" data-type="All"><span class="circle"></span> sva gradilišta</a>
	</section>

	<section id="tags" class="tags">
		<h3 class="tags__title">Izaberite godinu:</h3>
		<div class="label-tag-wrapper map-tags"></div>
	  </section>
   </div>
</div>
<div class="col-12 col-lg-9 main-content map-list">
   
</div>
</div>


<div class="container">
   <div class="row">
	  <div class="col-12 col-lg-9 main-content main-content--link map-list">
	  <h6><strong>Primetili ste novo gradilište? Javite nam!<strong></h6>
   <p style="font-size:16px">
   Naša građevinska mapa svakodnevno raste. Uz vašu pomoć, ona će biti još tačnija i ažurnija.
   Ukoliko ste primetili novo gradilište bilo gde u Srbiji, javite nam gde se ono nalazi!
   Pošaljite nam informacije koje o njemu imate, tačnu adresu, a posebno ćemo ceniti ako nam pošaljete i fotografije.
  </p>
  <?php
	while ( have_posts() ) :
		the_post();
		the_content();
		 endwhile;
	?>
	  </div>
	  <aside class="col-12 col-lg-3 sidebar clearfix ">
		 <div class="theiaStickySidebar">
			<?php get_sidebar(); ?>
		 </div>
	  </aside>
   </div>
</div>

<?php get_footer(); ?>
