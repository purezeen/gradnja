<?php /* Template Name: O nama */ get_header(); ?>

   <main class="container">
      <div class="row">
         <div class="col-12 col-lg-9 main-content main-content--link">
            <!-- Video  version1-->

         <?php
         while ( have_posts() ) : the_post();
            the_content();
         endwhile;
          ?>

         </div>

         <div class="col-12 col-lg-3 sidebar clearfix ">
            <div class="theiaStickySidebar">
               <?php get_sidebar(); ?>
            </div>
         </div>
         <!-- end sidebar -->

      </div>
      <!-- end row -->
   </main>
   <!-- end container -->

<?php get_footer(); ?>