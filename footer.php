<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>

	</div><!-- #content -->
	</div> 
	<!-- wrapper-site -->
	<!--<h1 class="scroll"><span class="iconify" data-icon="fe:arrow-up" data-inline="false"></span>
	</h1>-->
</div><!-- #page -->
	<footer class="site-footer section">
		<div class="container">
			<div class="row">
			
				<div class="col-12 col-xl-8">
					<ul class="unstyle-list">
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'secondary',
								'menu'            => 'secondary',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => '',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '%3$s',
								'depth'           => 0,
								'walker'          => ''
								) 
							); 
						?>		
					</ul> 
				</div>
				
				<div class="col-12 col-xl-4 copyright justify-content-right">
					<span>Copyright © 2010 - 2022 Gradnja.rs. Sva prava zadržana.</span>
				</div>
				
			</div>
		</div>
	</footer>


<!-- /3777314/Brending -->


<div id='div-gpt-ad-1524421926014-0' class="fixed-brending"  style='height:1px; width:1px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1538215930509-0'); });
</script>
</div>

<!-- <div id="GradnjaStickyFooter2"> -->
<!-- /3777314/GradnjaStickyFooter -->
	
<!-- <div id="div-gpt-ad-1560279422554-0" class="fixed-brending" style="margin:0 auto !important; text-align:center;">
	<script>
	googletag.cmd.push(function() { googletag.display('div-gpt-ad-1560279422554-0'); });
	</script>
</div> -->
<div id="div-gpt-ad-1617364950269-0" class="fixed-brending" style="margin:0 auto !important; text-align:center;">
	 <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1617364950269-0'); });
  </script>
</div>
<!--</div> -->
 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  
<?php wp_footer(); ?>

</body>
</html>