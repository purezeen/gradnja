
<?php get_header(); ?>


<div class="breadcrumb-titlebar">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <h1 class=""><?php
				the_archive_title();
				the_archive_description();
			?></h1>
            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
         </div>
      </div>
   </div>
</div>

<main class="container">
   <div class="row clearfix">
      <div class="col-12 col-lg-9 clearfix grid">
         <div class="grid">

            <div class="grid-sizer"></div>
            <?php get_template_part( 'loop' ); ?>
        </div>
        <!-- end grid -->
        <?php get_template_part( 'pagination' ); ?>
      </div>
      <!-- end main content-->

       <div class="col-12 col-lg-3 sidebar clearfix ">
         <div class="theiaStickySidebar">
            <?php get_sidebar();?>
         </div>
      </div> 
    <!--  end sidebar  -->

   </div>
    <!-- end row  -->
</main>
<!-- end container -->

<?php get_footer(); ?>