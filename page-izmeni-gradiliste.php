<?php
require 'wp-load.php';
require 'dbcalls/db.php';

global $wpdb;

$building_query  = 'SELECT DISTINCT objekat, id FROM wp_maps_markers WHERE objekat <> ""';
$investor_query  = "SELECT DISTINCT investitor FROM $wpdb->map_markers WHERE investitor <> ''";
$architect_query = "SELECT DISTINCT projektant FROM $wpdb->map_markers WHERE projektant <> ''";
$performer_query = "SELECT DISTINCT izvodjac FROM $wpdb->map_markers WHERE izvodjac <> ''";

$building_result  = obj_to_arr( $wpdb->get_results( $building_query ) ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
$investor_result  = obj_to_arr( $wpdb->get_results( $investor_query ) ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
$architect_result = obj_to_arr( $wpdb->get_results( $architect_query ) ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
$performer_result = obj_to_arr( $wpdb->get_results( $performer_query ) ); //phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared

$nonce_action = preg_replace( '/page-/', '', wp_basename( __FILE__, '.php' ) );
$nonce        = wp_create_nonce( $nonce_action );

get_header();

if ( current_user_can( 'administrator' ) || current_user_can( 'editor' ) ) : ?>

<main class="page-izmeni-gradiliste">
	<div class="container">
		<h1 class="header-gradiliste">Izmeni gradilište</h1>
		<form id="formid" action="" method="post">
			<input type="hidden" name="id" id="id">
			<input type="hidden" name="gradnja_nonce" value="<?php echo esc_attr( $nonce ); ?>"/>
			<select name="id" id="change-select">
				<option selected value=""> -- Izaberi gradilište -- </option>

				<?php

				foreach ( $building_result as $key => $value ) :
					$obj_name = $value['objekat'];
					$obj_id   = $value['id'];
					?>

				<option value="<?php echo esc_attr( $obj_id ); ?>" data-id="objekat-<?php echo esc_attr( $obj_id ); ?>">
					<?php echo esc_attr( $obj_name ); ?>
				</option>

				<?php endforeach; ?>
			</select>

			<div class="container-gradiliste">
				<div class="row-gradiliste">
					<div class="addPageDiv">
						<h6>Ime objekta</h6>
						<input type="text" name="objekat" id="objekat" placeholder="Ime objekta">
					</div>
					<div class="addPageDiv">
						<h6>Grad</h6>
						<input type="text" name="grad" id="grad" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Ulica</h6>
						<input type="text" name="ulica" id="ulica" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Broj</h6>
						<input type="number" name="broj" id="broj" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Pocetak gradnje</h6>
						<input type="text" name="pocetak_gradnje" id="pocetak" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Zavrsetak gradnje</h6>
						<input type="text" name="zavrsetak_gradnje" id="zavrsetak" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Opis</h6>
						<textarea form="formid" rows="6" name="opis" id="opis" style="resize: none; height:150px" placeholder="Opciono, opis projekta i/ili dodatne informacije"></textarea>
					</div>
					<div class="addPageDiv">
						<h6>Slike</h6>
						<textarea id="image-url" name="slike" class="novalidate hidden"></textarea>
						<div id="btns" class="btns">
							<input type="button" id="upload-image" class="btn novalidate" multiple="multiple" value="Izmeni slike">
							<input type="button" id="delete-image" class="btn btn--danger novalidate" value="Obriši slike">
						</div>
					</div>
					<div class="addPageDiv">
						<h5><label for="putanja">Putanja</label></h5>
						<input type="checkbox" name="putanja" id="putanja" class="novalidate"/>
						<div class="map-wrapper">
							<div id="map" class="map"></div>
							<div id="bar" class="bar">
								<p class="auto"><input type="text" id="autoc"/></p>
								<button class="btn" id="clear">Očisti mapu</button>
							</div>
							<textarea class="hidden novalidate" name="putanje" id="putanje" cols="30" rows="10"></textarea>
						</div>
					</div>
				</div>
				<div class="row-gradiliste">
					<div class="addPageDiv">
						<h6>Tip gradnje</h6>
						<select name="type" id="tip" form="formid">
							<option value="U izgradnji">U izgradnji</option>
							<option value="Zavrseno">Završeno</option>
							<option value="U planu">U planu</option>
							<option value="Obustavljeno">Obustavljeno</option>
						</select>
					</div>
					<div class="addPageDiv">
						<h6>Tip zgrade</h6>
						<select name="type_of_building" id="tipZgrade" form="formid">
							<option value="Ugostiteljski objekat">Ugostiteljski objekat</option>
							<option value="Hotel / prenoćište">Hotel / prenoćište</option>
							<option value="Tržni centar /šoping centar">Tržni centar /šoping centar</option>
							<option value="Garaža / parking">Garaža / parking</option>
							<option value="Ski centar / gondola">Ski centar / gondola</option>
							<option value="Stadion">Stadion</option>
							<option value="Gerontološki centar">Gerontološki centar</option>
							<option value="Džamija">Džamija</option>
							<option value="Stambena zgrada / kompleks">Stambena zgrada / kompleks</option>
							<option value="Poslovni objekat">Poslovni objekat</option>
							<option value="Fabrika / proizvodni pogon">Fabrika / proizvodni pogon</option>
							<option value="Univerzitet  / institut">Univerzitet  / institut</option>
							<option value="Bolnica">Bolnica</option>
							<option value="Crkva / verski objekat">Crkva / verski objekat</option>
							<option value="Škola / obrazovna ustanova">Škola / obrazovna ustanova</option>
							<option value="Klinički centar / klinika">Klinički centar / klinika</option>
							<option value="Distributivni centar / sedište firme">Distributivni centar / sedište firme</option>
							<option value="Bazen / sportski kompleks">Bazen / sportski kompleks</option>
							<option value="Auto-perionica">Auto-perionica</option>
							<option value="Prečistač otpadnih voda">Prečistač otpadnih voda</option>
							<option value="Benzinska pumpa">Benzinska pumpa</option>
							<option value="Park">Park</option>
							<option value="Pravosudni objekat">Pravosudni objekat</option>
							<option value="Aerodrom">Aerodrom</option>
							<option value="Autobuska stanica / autobuska radionica">Autobuska stanica / autobuska radionica</option>
							<option value="Jasle / obdanište">Jasle / obdanište</option>
							<option value="Pijaca">Pijaca</option>
							<option value="Biblioteka">Biblioteka</option>
							<option value="Kulturna stanica / istorijska zgrada">Kulturna stanica / istorijska zgrada</option>
							<option value="Trg / fontana">Trg / fontana</option>
							<option value="Železnička stanica / voz">Železnička stanica / voz</option>
							<option value="Most">Most</option>
							<option value="Tunel">Tunel</option>
							<option value="Policijska stanica">Policijska stanica</option>
							<option value="Put">Put</option>
						</select>
					</div>
					<div class="addPageDiv">
						<h6>Geografska širina</h6>
						<input type="text" name="lat" id="lat" placeholder="Latitude koordinata">
					</div>
					<div class="addPageDiv">
						<h6>Geografska dužina</h6>
						<input type="text" name="lon" id="lon" placeholder="Longitude koordinata">
					</div>
					<div class="addPageDiv">
						<h6>Cena</h6>
						<input type="text" name="cena" id="cena" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Broj Stanova</h6>
						<input type="text" name="broj_stanova" id="brojStanova" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Površina objekta</h6>
						<input type="text" name="povrsina_objekta" id="povrsina" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Tagovi</h6>
						<input type="text" name="tags" id="tags" placeholder="Opciono" autocomplete="off">
						<input type="hidden" name="tag" id="tag">
						<div class="label-tag-wrapper"></div>
					</div>
					<div class="addPageDiv">
						<h6>Spratnost</h6>
						<input type="text" name="spratnost" id="spratnost" placeholder="Opciono">
					</div>
					<div class="addPageDiv">
						<h6>Investitor</h6>
						<input type="hidden" name="investitor" id="investitor" placeholder="Opciono">
					</div>

					<select id="investitor-select" data-name="investitor"  class="field">
						<option selected value=""> -- Izaberi Investitora -- </option>

						<?php

						foreach ( $investor_result as $key => $value ) :
							$name = $value['investitor'];

							?>

						<option value="<?php echo esc_attr( $name ); ?>">
							<?php echo esc_attr( $name ); ?>
						</option>

						<?php endforeach; ?>
					</select>

					<div class="addPageDiv">
						<h6>Projektant</h6>
						<input type="hidden" name="projektant" id="projektant" placeholder="Opciono">
					</div>

					<select id="projektant-select" data-name="projektant" class="field">
						<option selected value=""> -- Izaberi Projektanta -- </option>

						<?php

						foreach ( $architect_result as $key => $value ) :
							$name = $value['projektant'];

							?>

						<option value="<?php echo esc_attr( $name ); ?>">
							<?php echo esc_attr( $name ); ?>
						</option>

						<?php endforeach; ?>
					</select>

					<div class="addPageDiv">
						<h6>Izvođač</h6>
						<input type="hidden" name="izvodjac" id="izvodjac" placeholder="Opciono">
					</div>

					<select id="izvodjac-select" data-name="izvodjac" class="field">
						<option selected value=""> -- Izaberi Izvodjaca -- </option>

						<?php

						foreach ( $performer_result as $key => $value ) :
							$name = $value['izvodjac'];

							?>

						<option value="<?php echo esc_attr( $name ); ?>">
							<?php echo esc_attr( $name ); ?>
						</option>

						<?php endforeach; ?>	
					</select>

					<div class="addPageDiv">
						<h6>Linkovi</h6>
						<textarea form="formid" rows="6" name="linkovi" id="linkovi" style="resize: none;" placeholder="Opciono, svaki link do posta u jednom redu"></textarea>
					</div>
				</div>
			</div>
			<div class="addPageDiv">
				<input type="submit" id="izmeni" name="izmeni" value="Izmeni"></input>
			</div>
		</form>
		<hr class="hr"/>
		<h1 class="header-gradiliste">Izbriši postojeće gradilište</h1>
		<div class="addPageDiv">
			<h7>Izaberite ime gradilišta koje želite da uklonite iz baze podataka</h7>
			<form id="delete-form" action="" method="post">
				<select name="deleteSelect" id="delete-select" class="hybrid-search-delete">
				<?php

				foreach ( $building_result as $key => $value ) :
					$obj_name = $value['objekat'];
					$obj_id   = $value['id'];
					?>

				<option value="<?php echo esc_attr( $obj_name ); ?>" data-id="objekat-<?php echo esc_attr( $obj_id ); ?>">
					<?php echo esc_attr( $obj_name ); ?>
				</option>

				<?php endforeach; ?>
				</select>
				<div class="addPageDiv">
					<input type="submit" id="obrisi" name="obrisi" value="Obriši iz baze"></input>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-6 col-lg-9 main-content map-list">
		</div>
		<aside class="col-6 col-lg-3 sidebar clearfix ">
			<div class="theiaStickySidebar">
			</div>
		</aside>
	</div>
</div>
	<?php
	else :
		echo '<script>window.location.href = "404.php";</script>';
endif;
	?>

<?php
	get_footer();
?>
