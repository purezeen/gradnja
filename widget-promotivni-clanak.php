<?php global $post; ?>
<?php $post_object = get_field( 'promo_post', $acfw ); ?>

<?php if ( $post_object ): ?>
	<?php $post = $post_object; ?>
	<?php setup_postdata( $post ); ?> 
		<div class="highlighted-post sidebar-block">
		  <h3 class="title-line title-sidebar mb-4">Odabrano</h3>
		  <?php $post_thumbnail = get_the_post_thumbnail_url(get_the_ID(),'large'); ?>
		  <div class="img-wrap-1-1 cover radius"
		    style="background-image: url(<?php echo $post_thumbnail; ?>);">
		    <div class="widget-content">
		      <h5 class="widget-post-title color-white"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
		      <div class="meta-data">
		        <span> <?php echo get_the_date( 'd.m.Y.' ); ?></span>
		      </div>
		    </div>
		  </div>
		</div>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>