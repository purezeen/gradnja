<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script type="text/javascript">
		var templateUrl = '<?php echo get_bloginfo( 'template_url' ); ?>';
	</script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css" integrity="sha512-Woz+DqWYJ51bpVk5Fv0yES/edIMXjj3Ynda+KWTIkGoynAMHrqTcDUQltbipuiaD5ymEo9520lyoVOo9jCQOCA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700|Open+Sans:300,400,700&display=swap&subset=latin-ext" rel="stylesheet">

	<?php
		$libraries = 'drawing,places';

		if( is_page( 'mapa' ) ) {
			$callback = 'initMap';
		}
	?>
	
	<script async type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCza3q1Gm2n6CDGZPN8YpxjCcykD35BJSc&libraries=<?php echo esc_attr( $libraries ); ?>">
	</script>


	<?php wp_head(); ?>

	<script>
		// Set cookie for responsive php - GTag 
		if (matchMedia && window.matchMedia('(min-device-width: 1000px)').matches) {
			document.cookie = 'desktop=1; path=/';
		}
	</script>
	
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd.push(function() {
    googletag.defineSlot('/3777314/mapa', [[750, 250], [300, 250]], 'div-gpt-ad-1661852681689-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>


<!-- Head inline scripts ACF -->
<?php if ( get_field( 'head_inline_scripts', 'option' ) ) : ?>
	<?php the_field( 'head_inline_scripts', 'option' ); ?>
<?php endif ?>

<!-- Head inline styles ACF -->
<?php if ( get_field( 'head_inline_styles', 'option' ) ) : ?>
	<style>
		<?php the_field( 'head_inline_styles', 'option' ); ?>
	</style>
<?php endif ?>

<!-- Head inline Google Tag Manager ACF -->
<?php if ( get_field( 'google_tag_manager_scripts', 'option' ) ) : ?>
		<?php the_field( 'google_tag_manager_scripts', 'option' ); ?>
<?php endif ?>

</head>
<body <?php body_class(); ?>>
	
	

<style>
#div-gpt-ad-1653473723714-0 > div >iframe{
position:fixed;
top:0px;
left:0px;
width:100%;
height:1000px;
}
</style>
<div class="menu-dark-line"></div>
<!-- /3777314/Brending dodat 25.maja 2022 -->
<div id='div-gpt-ad-1653473723714-0'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1653473723714-0'); });
  </script>
</div>
<!-- /3777314/Brending dodat 25.maja 2022 -->
	
	
<div id="page" class="site wrapper">
		
		
		
		
<!-- Above navigation GTag display  -->
<?php
if ( have_rows( 'above_navigation', 'option' ) ) :
	while ( have_rows( 'above_navigation', 'option' ) ) :
		the_row();
		if ( get_sub_field( 'enable' ) == 1 ) {
			echo '<div class="top-promo-header">';
			the_sub_field( 'above_navigation_code', 'option' );
			echo '</div>';
		}
	 endwhile;
endif;
?>

		<div class="header-wrap">

			<div class="top-header">
				<div class="container">
					<div class="row">
						<div class="col-8">
							<ul class="unstyle-list">
								<?php
								wp_nav_menu(
									array(
										'theme_location'  => 'top-header',
										'menu'            => 'top-header',
										'container'       => 'li',
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => '',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '%3$s',
										'depth'           => 0,
										'walker'          => '',
									)
								);
								?>
							</ul>
						</div>
						<?php if ( have_rows( 'links', 'option' ) ) : ?>
						<div class="col-4 header-social-icon">
							<?php
							while ( have_rows( 'links', 'option' ) ) :
								the_row();
								?>
							<a href="<?php the_sub_field( 'facebook' ); ?>" target="_blank" rel="nofollow"><span class="iconify" data-icon="dashicons:facebook-alt"
									data-inline="false"></span></a>
							<a href="<?php the_sub_field( 'twitter' ); ?>" target="_blank" rel="nofollow"><span class="iconify" data-icon="dashicons:twitter"
									data-inline="false"></span></a>
							<a class="instagram" href="<?php the_sub_field( 'instagram' ); ?>" target="_blank" rel="nofollow"><span class="iconify" data-icon="dashicons:instagram"
									data-inline="false"></span></a>
							<a href="<?php the_sub_field( 'youtube' ); ?>" target="_blank" rel="nofollow"><span class="iconify" data-icon="ps:youtube"
									data-inline="false"></span></a>
							<?php endwhile; ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<header id="masthead" class="site-header clearfix" role="banner">
				<div class="container">

				<div class="hamburger hamburger--arrow-r" id="my-icon"> <span class="hamburger-box"> <span
								class="hamburger-inner"></span> </span> </div>
					<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/gradnja_logo.png"
							alt="Gradnja logo" width="150" height="68"></a>

					<nav id="main-nav" class="main-navigation " role="navigation">

						<ul class="menu clearfix unstyle-list">

							<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'primary',
									'menu'            => 'primary',
									'container'       => '',
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => '',
								)
							);
							?>
							<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'top-header',
									'menu'            => 'top-header',
									'container'       => 'li',
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => '',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => '',
								)
							);
							?>

						</ul>

					</nav><!-- #site-navigation -->

					
					<div class="search-top">
						<?php
						get_search_form();
						?>
					</div>
				</div>
				<div class="progress-container">
					<div class="progress-bar" id="myBar"></div>
				</div>
			</header><!-- #masthead -->



		</div>
<div class="wrap">

		<div id="content" class="site-content">

			<?php if ( have_rows( 'belove_navigation', 'option' ) ) : ?>
				<?php
				while ( have_rows( 'belove_navigation', 'option' ) ) :
					the_row();
					?>
					<?php $belove_navigation_image = get_sub_field( 'image' ); ?>
					<?php if ( $belove_navigation_image ) : ?>
					<div class="container promo-header">
						<a href="<?php the_sub_field( 'url' ); ?>" target="_blank"><img src="<?php echo $belove_navigation_image['url']; ?>" alt="<?php echo $belove_navigation_image['alt']; ?>" /></a>
					</div>
					<?php endif ?>
				<?php endwhile; ?>
			<?php endif; ?>
